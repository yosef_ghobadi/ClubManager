package com.nikosoft.clubmanager;

import android.content.Context;

import androidx.test.InstrumentationRegistry;

import com.nikosoft.clubmanager.Data.DBRepository.CoachRepository;
import com.nikosoft.clubmanager.Utiles.Utility;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */

public class ExampleInstrumentedTest {

    Utility utility=new Utility();

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        //assertEquals("com.nikosoft.clubmanager", appContext.getPackageName());
        assertEquals("فارسی", utility.deviceLanguage());
    }

    @Test
    public void getCoachsFromDB() throws Exception{
        CoachRepository repository=new CoachRepository();
        assertEquals(null,repository.getAll());
    }
}
