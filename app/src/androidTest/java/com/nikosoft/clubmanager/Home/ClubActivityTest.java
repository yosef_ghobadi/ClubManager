package com.nikosoft.clubmanager.Home;


import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.DataInteraction;
import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import com.nikosoft.clubmanager.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(JUnit4.class)
public class ClubActivityTest {

    @Rule
    public ActivityTestRule<HomeActivity> mActivityTestRule = new ActivityTestRule<>(HomeActivity.class);

    @Test
    public void clubActivityTest() {
        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html

        try {
            Thread.sleep(7000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.txt_name),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.input_name),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText.perform(replaceText("فجر"), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.txt_monthly_cost),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.input_monthly_cost),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText2.perform(replaceText("5"), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.txt_monthly_cost), withText("۵"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.input_monthly_cost),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText3.perform(replaceText("۵0"));

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.txt_monthly_cost), withText("۵0"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.input_monthly_cost),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText4.perform(closeSoftKeyboard());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.txt_monthly_cost), withText("۵۰"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.input_monthly_cost),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText5.perform(replaceText("۵۰0"));

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.txt_monthly_cost), withText("۵۰0"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.input_monthly_cost),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText6.perform(closeSoftKeyboard());

        ViewInteraction appCompatEditText7 = onView(
                allOf(withId(R.id.txt_monthly_cost), withText("۵۰۰"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.input_monthly_cost),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText7.perform(replaceText("۵۰۰0"));

        ViewInteraction appCompatEditText8 = onView(
                allOf(withId(R.id.txt_monthly_cost), withText("۵۰۰0"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.input_monthly_cost),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText8.perform(closeSoftKeyboard());

        ViewInteraction appCompatEditText9 = onView(
                allOf(withId(R.id.txt_monthly_cost), withText("۵ ۰۰۰"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.input_monthly_cost),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText9.perform(replaceText("۵ ۰۰۰0"));

        ViewInteraction appCompatEditText10 = onView(
                allOf(withId(R.id.txt_monthly_cost), withText("۵ ۰۰۰0"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.input_monthly_cost),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText10.perform(closeSoftKeyboard());

        ViewInteraction appCompatSpinner = onView(
                allOf(withId(R.id.spinner_tuition_day),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.nestedScrollView),
                                        0),
                                4),
                        isDisplayed()));
        appCompatSpinner.perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(250);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        DataInteraction appCompatCheckedTextView = onData(anything())
                .inAdapterView(withClassName(is("androidx.appcompat.widget.DropDownListView")))
                .atPosition(29);
        appCompatCheckedTextView.perform(click());

        ViewInteraction appCompatTextView = onView(
                allOf(withId(R.id.club_txt_time), withText("انتخاب کنید"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        5),
                                2),
                        isDisplayed()));
        appCompatTextView.perform(click());

        ViewInteraction appCompatTextView2 = onView(
                allOf(withClassName(is("androidx.appcompat.widget.AppCompatTextView")), withText("۰۰"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.FrameLayout")),
                                        0),
                                2),
                        isDisplayed()));
        appCompatTextView2.perform(click());

        ViewInteraction tabView = onView(
                allOf(withContentDescription("ساعت پایان :"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.tabLayout),
                                        0),
                                1),
                        isDisplayed()));
        tabView.perform(click());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.btnPositiveDialog), withText("تایید"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.RelativeLayout")),
                                        2),
                                0),
                        isDisplayed()));
        appCompatButton.perform(click());

        ViewInteraction appCompatTextView3 = onView(
                allOf(withId(R.id.club_txt_fields), withText("انتخاب کنید"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.LinearLayout")),
                                        6),
                                2),
                        isDisplayed()));
        appCompatTextView3.perform(click());

        ViewInteraction appCompatButton2 = onView(
                allOf(withId(android.R.id.button1), withText("تایید"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.buttonPanel),
                                        0),
                                3)));
        appCompatButton2.perform(scrollTo(), click());

        ViewInteraction appCompatEditText11 = onView(
                allOf(withId(R.id.txt_description),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.input_description),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText11.perform(replaceText("باشگاه فجر"), closeSoftKeyboard());
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
