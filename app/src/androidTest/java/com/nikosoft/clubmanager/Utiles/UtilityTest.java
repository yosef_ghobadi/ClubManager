package com.nikosoft.clubmanager.Utiles;

import org.junit.Test;

import java.io.IOException;

import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;

import static org.junit.Assert.assertEquals;

public class UtilityTest {


    Utility utility=new Utility();
    @Test
    public void getJoinYears() {
        PersianDate persianDate = new PersianDate();
        PersianDateFormat pdformater = new PersianDateFormat();
        pdformater.format(persianDate);
        int JOIN_YEAR_RANGE = persianDate.getGrgYear();

        assertEquals("۱۳۹۸",utility.getJoinYears().get(0));
        //assertEquals(1398,JOIN_YEAR_RANGE);
    }

    @Test
    public void detectLanguege()
    {
         String Farsi = "فارسی";
        assertEquals(Farsi,utility.deviceLanguage());
    }

    @Test
    public void setPhoneNumber()
    {
        utility.savePref(utility.PHONE_NUMBER,"09211958964", utility.context);
    }

    @Test
    public void getPhoneNumber()
    {
        assertEquals("09211958964",utility.retrievePref(utility.PHONE_NUMBER,"",utility.context));
    }

    @Test
    public void getFirstRun()
    {
        assertEquals("2",utility.retrievePref(utility.FIRST_RUN,"-1",utility.context));
    }
    @Test
    public void setFirstRun()
    {
        utility.savePref(utility.FIRST_RUN,"-1",utility.context);
    }

    @Test
    public void getDate() throws IOException {
        assertEquals("",utility.getTodayTimeFromInternet().toString());
    }
}