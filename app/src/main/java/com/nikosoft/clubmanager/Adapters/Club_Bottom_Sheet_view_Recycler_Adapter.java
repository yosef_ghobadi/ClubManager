package com.nikosoft.clubmanager.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.nikosoft.clubmanager.Club.ClubActivity;
import com.nikosoft.clubmanager.ClubModify.ClubModifyActivity;
import com.nikosoft.clubmanager.Club_Fragment_Dialog.Fragment_dialog_modal;
import com.nikosoft.clubmanager.Data.DBRepository.ClubRepository;
import com.nikosoft.clubmanager.Data.Model.Club;
import com.nikosoft.clubmanager.R;

import java.util.List;

/**
 * Created by Yosef on 12/02/2019.
 */

public class Club_Bottom_Sheet_view_Recycler_Adapter extends RecyclerView.Adapter<Club_Bottom_Sheet_view_Recycler_Adapter.Dialog_ViewHolder> {

    Context context;
    List<Club> clubs;
    BottomSheetDialogFragment dialogFragment;
    ClubRepository repository = new ClubRepository();


    public Club_Bottom_Sheet_view_Recycler_Adapter(Context context, List<Club> clubs, BottomSheetDialogFragment dialogFragment) {
        this.context = context;
        this.clubs = clubs;
        this.dialogFragment = dialogFragment;

    }


    @NonNull
    @Override

    public Dialog_ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.bottom_sheet_view_recycler_row, parent, false);
        return new Dialog_ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Dialog_ViewHolder holder, int position) {
        holder.txt_club_name.setText(clubs.get(position).getName());
        holder.txt_club_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ClubActivity.class);
                intent.putExtra("id", clubs.get(position).getId());
                intent.putExtra("name", clubs.get(position).getName());
                context.startActivity(intent);
            }
        });

        holder.delete_club.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                AlertDialog dialog = new AlertDialog.Builder(context)
                        .setTitle(context.getString(R.string.delete_club))
                        .setMessage(clubs.get(position).getName() )
                        .setCancelable(true)
                        .setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogFragment.dismiss();
                                removeItem(position);
                                Fragment_dialog_modal modal = new Fragment_dialog_modal();
                                FragmentManager manager = ((AppCompatActivity) context).getSupportFragmentManager();
                                modal.show(manager, modal.getTag());
                            }
                        })
                        .setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        }).create();
                dialog.show();
            }
        });
        holder.edit_club.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id = clubs.get(position).getId();
                Intent intent = new Intent(context, ClubModifyActivity.class);
                intent.putExtra("id", id);
                context.startActivity(intent);
            }
        });

    }

    /**
     * @param position
     */
    private void removeItem(int position) {

        repository.delete(clubs.get(position).getId());
        /*coachList.clear();
        coachList=repository.getAll();*/
        notifyDataSetChanged();
        /*notifyItemRemoved(position);
        notifyItemRangeChanged(position,coachList.size());*/
        repository.close();
    }


    @Override
    public int getItemCount() {
        return (clubs!=null)? clubs.size():0;
    }

    public class Dialog_ViewHolder extends RecyclerView.ViewHolder {

        TextView txt_club_name;
        ImageView delete_club, edit_club;

        public Dialog_ViewHolder(View itemView) {
            super(itemView);
            txt_club_name = itemView.findViewById(R.id.txt_recycler_row_name);
            delete_club = itemView.findViewById(R.id.img_recycler_row_delete);
            edit_club = itemView.findViewById(R.id.img_recycler_row_edit);
        }
    }
}
