package com.nikosoft.clubmanager.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nikosoft.clubmanager.Data.Model.Coach;
import com.nikosoft.clubmanager.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Yosef on 12/02/2019.
 */

public class Club_Coaches_Recycler_Adapter extends RecyclerView.Adapter<Club_Coaches_Recycler_Adapter.ViewHolder> {

    Context context;
    List<Coach> coachList;

    public Club_Coaches_Recycler_Adapter(Context context, List<Coach> coachList) {
        this.context = context;
        this.coachList = coachList;

    }


    @NonNull
    @Override

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.club_item_recycler_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txt_item_name.setText(coachList.get(position).getName());
        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        holder.item.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                final CharSequence[] items = {context.getString(R.string.delete),
                        context.getString(R.string.delete_selected),
                        context.getString(R.string.edit)};

                AlertDialog.Builder builder = new AlertDialog.Builder(context);

                //builder.setTitle("Select The Action");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                    }
                });
                builder.show();
                return true;
            }
        });

        holder.img_item_image.setBackgroundResource(R.drawable.coach_bold);
    }

/*
    private void removeItem(int position) {

        repository.delete(coachList.get(position).getId());
        *//*coachList.clear();
        coachList=repository.getAll();*//*
        notifyDataSetChanged();
        *//*notifyItemRemoved(position);
        notifyItemRangeChanged(position,coachList.size());*//*
        repository.close();
    }*/


    @Override
    public int getItemCount() {
        return coachList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txt_item_name;
        ImageView img_item_image;
        View item;
        public ViewHolder(View itemView) {
            super(itemView);
            txt_item_name = itemView.findViewById(R.id.txt_recycler_row_club_item_name);
            img_item_image = itemView.findViewById(R.id.img_recycler_row_club_item_image);
            item = itemView.findViewById(R.id.lay_recycler_row_club_item);
        }
    }
}
