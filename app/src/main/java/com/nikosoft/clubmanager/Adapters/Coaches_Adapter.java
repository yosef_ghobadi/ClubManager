package com.nikosoft.clubmanager.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.SparseBooleanArray;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.CoachDetail.CoachDetailActivity;
import com.nikosoft.clubmanager.CoachModify.CoachModifyActivity;
import com.nikosoft.clubmanager.Data.DBRepository.CoachRepository;
import com.nikosoft.clubmanager.Data.Model.Coach;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.Utiles.Utility;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class Coaches_Adapter extends RecyclerView.Adapter<Coaches_Adapter.ViewHolder> {

    private List<Coach> coachList;
    private Context context;
    private ClickAdapterListener  listener;
    private static int currentSelectedIndex = -1;
    private SparseBooleanArray selectedItems;
    private int clickedItemPosition;
    private CoachRepository coachRepository;
    @Inject
    Utility utility;

    public Coaches_Adapter(List<Coach> coachList, Context context,ClickAdapterListener listener) {
        this.coachList = coachList;
        this.context = context;
        this.listener=listener;
        selectedItems = new SparseBooleanArray();
        coachRepository=new CoachRepository();
        ClubManager.getApplicationComponent().inject(this);
    }




    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.coach_athlete_row_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Coach coach = coachList.get(position);
        Utility utility=new Utility();

        //if the coach's image is null
        if(utility.getImage(coach.getImage())==null) {
            holder.img_profile.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_account_circle));
        }
        else
            holder.img_profile.setImageBitmap(utility.getImage(coach.getImage()));

        holder.txt_name.setText(coach.getName());
        Object birthdate = utility.getNormalPersianDate(coach.getBirthDate());
        holder.txt_desc.setText(String.format("%s : %s", context.getString(R.string.birth_date), utility.farsinumber(birthdate.toString())));


        holder.itemView.setActivated(selectedItems.get(position, false));

        applyClickEvents(holder, position);
    }

    private void applyClickEvents(ViewHolder holder, final int position) {
        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                    clickedItemPosition = position;
                    listener.onRowClicked(position);

            }
        });

        holder.root.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                listener.onRowLongClicked(position);
                view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);

                return true;
            }
        });


    }

    @Override
    public int getItemCount() {

        return coachList == null ? 0 : coachList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener{

        CircularImageView img_profile;
        TextView txt_name;
        TextView txt_desc;
        View root;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_profile = itemView.findViewById(R.id.coach_athlete_item_img_profile);
            txt_name = itemView.findViewById(R.id.coach_athlete_item_txt_name);
            txt_desc = itemView.findViewById(R.id.coach_athlete_item_txt_desc);
            root = itemView.findViewById(R.id.coach_athlete_item_root);

            itemView.setOnLongClickListener(this);
        }

        @Override
        public boolean onLongClick(View view) {
            listener.onRowLongClicked(getAdapterPosition());
            view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
            return true;
        }
    }


    public void toggleSelection(int pos) {
        currentSelectedIndex = pos;
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        } else {
            selectedItems.put(pos, true);
            clickedItemPosition = pos;
        }
        notifyItemChanged(pos);
    }

    public void selectAll() {

        for (int i = 0; i < getItemCount(); i++)
            selectedItems.put(i, true);
        notifyDataSetChanged();

    }


    public void clearSelections() {
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List getSelectedItems() {
        List items =
                new ArrayList(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    public void selectItem(int position)
    {
        Intent intent=new Intent(context, CoachDetailActivity.class);
        intent.putExtra("coachId",coachList.get(clickedItemPosition).getId());
        context.startActivity(intent);
        resetCurrentIndex();
    }


    public void removeData(int position) {
        //delete coach's image from storage
        utility.deleteFile(coachList.get(position).getImage());
        coachRepository.delete(coachList.get(position).getId());
        resetCurrentIndex();
        notifyDataSetChanged();
    }

    public void updateData() {
        Intent intent = new Intent(context, CoachModifyActivity.class);
        intent.putExtra("coachId", coachList.get(clickedItemPosition).getId());
        context.startActivity(intent);
        resetCurrentIndex();
    }

    private void resetCurrentIndex() {
        currentSelectedIndex = -1;
    }

    public interface ClickAdapterListener {

        void onRowClicked(int position);

        void onRowLongClicked(int position);
    }
}
