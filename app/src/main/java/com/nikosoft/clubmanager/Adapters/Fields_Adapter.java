package com.nikosoft.clubmanager.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.nikosoft.clubmanager.Data.DBRepository.FieldRepository;
import com.nikosoft.clubmanager.Data.Model.Field;
import com.nikosoft.clubmanager.Field.OnFieldEditMenuClickListener;
import com.nikosoft.clubmanager.R;

import java.util.List;

public class Fields_Adapter extends RecyclerView.Adapter<Fields_Adapter.ViewHolder> {

    OnFieldEditMenuClickListener onFieldEditMenuClickListener;
    FieldRepository fieldRepository;
    public Context context;
    List<Field> fieldList;

    public Fields_Adapter(Context context,List<Field> fieldList,OnFieldEditMenuClickListener onFieldEditMenuClickListener)
    {
        this.context=context;
        this.fieldList=fieldList;
        fieldRepository=new FieldRepository();
        this.onFieldEditMenuClickListener=onFieldEditMenuClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(context).inflate(R.layout.field_row_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Field field=fieldList.get(position);
        holder.txt_name.setText(field.getTitle());
        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] array=new String[]{context.getString(R.string.edit), context.getString(R.string.delete)};
                AlertDialog.Builder builder=new AlertDialog.Builder(context);
                builder.setItems(array, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if(i==0)
                            onFieldEditMenuClickListener.onEditMenuClick(field);
                        else
                            deleteField(field);


                    }
                });
                 builder.create().show();
            }
        });
    }

    private void deleteField(Field field) {

        AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.delete_field))
                .setMessage(field.getTitle())
                .setCancelable(true)
                .setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        fieldRepository.delete(field.getId());
                        notifyDataSetChanged();

                    }
                })
                .setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).create();
        dialog.show();


    }

    @Override
    public int getItemCount() {
        return (fieldList!=null)?fieldList.size():0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder  {

        View root;
        TextView txt_name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            root=itemView.findViewById(R.id.field_row_item_ripple);
            txt_name=itemView.findViewById(R.id.field_row_item_name);


        }


    }
}
