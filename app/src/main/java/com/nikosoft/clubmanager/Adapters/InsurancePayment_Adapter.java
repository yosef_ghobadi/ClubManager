package com.nikosoft.clubmanager.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.nikosoft.clubmanager.Data.DBRepository.InsurancePaymentRepository;
import com.nikosoft.clubmanager.Data.Model.InsurancePayment;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.Utiles.Utility;

import java.util.List;

public class InsurancePayment_Adapter extends RecyclerView.Adapter<InsurancePayment_Adapter.ViewHolder> {


    Utility utility;
    InsurancePaymentRepository insurancePaymentRepository;
    public Context context;
    List<InsurancePayment> insurancePayments;
    String title;
    public InsurancePayment_Adapter(Context context, List<InsurancePayment> insurancePaymentList)
    {
        this.context=context;
        this.insurancePayments =insurancePaymentList;
        insurancePaymentRepository =new InsurancePaymentRepository();
        utility=new Utility();
    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(context).inflate(R.layout.field_row_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        InsurancePayment insurancePayment= insurancePayments.get(position);

        title=utility.farsinumber(utility.getFormattedDate(insurancePayment.getDate(),0));
        if(insurancePayment.getInsurance()!=null)
        {
            title+="\n"
                    +insurancePayment.getInsurance().getTitle()
                    +"   "
                    + utility.threeDigit(utility.farsinumber(insurancePayment.getInsurance().getCost()+""),',');
        }
        holder.txt_name.setText(title);



        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] array=new String[]{ context.getString(R.string.delete)};
                AlertDialog.Builder builder=new AlertDialog.Builder(context);
                builder.setItems(array, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                            deleteInsurance(insurancePayment,title);


                    }
                });
                builder.create().show();
            }
        });

    }

    private void deleteInsurance(InsurancePayment insurancePayment,String title) {

        AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.delete_insurance_payment))
                .setMessage(title)
                .setCancelable(true)
                .setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        insurancePaymentRepository.delete(insurancePayment.getId());
                        notifyDataSetChanged();

                    }
                })
                .setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).create();
        dialog.show();


    }

    @Override
    public int getItemCount() {
        return (insurancePayments !=null)? insurancePayments.size():0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder  {

        View root;
        TextView txt_name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            root=itemView.findViewById(R.id.field_row_item_ripple);
            txt_name=itemView.findViewById(R.id.field_row_item_name);


        }


    }
}
