package com.nikosoft.clubmanager.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.nikosoft.clubmanager.Data.DBRepository.InsuranceRepository;
import com.nikosoft.clubmanager.Data.Model.Insurance;
import com.nikosoft.clubmanager.Insurance.OnInsuranceEditMenuClickListener;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.Utiles.Utility;

import java.util.List;

public class Insurance_Adapter extends RecyclerView.Adapter<Insurance_Adapter.ViewHolder> {

    OnInsuranceEditMenuClickListener onEditMenuClickListener;
    Utility utility;
    InsuranceRepository insuranceRepository;
    public Context context;
    List<Insurance> insuranceList;

    public Insurance_Adapter(Context context, List<Insurance> insuranceList, OnInsuranceEditMenuClickListener onInsuranceEditMenuClickListener)
    {
        this.context=context;
        this.insuranceList =insuranceList;
        insuranceRepository =new InsuranceRepository();
        utility=new Utility();
        this.onEditMenuClickListener=onInsuranceEditMenuClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(context).inflate(R.layout.field_row_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Insurance insurance= insuranceList.get(position);
        holder.txt_name.setText(insurance.getTitle()+"\n"+utility.threeDigit(utility.farsinumber(insurance.getCost()+""),','));


        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] array=new String[]{context.getString(R.string.edit), context.getString(R.string.delete)};
                AlertDialog.Builder builder=new AlertDialog.Builder(context);
                builder.setItems(array, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if(i==0)
                            onEditMenuClickListener.onEditMenuClick(insurance);
                        else
                            deleteInsurance(insurance);


                    }
                });
                 builder.create().show();
            }
        });
    }

    private void deleteInsurance(Insurance insurance) {

        AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.delete_insurance))
                .setMessage(insurance.getTitle()+"\n"+utility.threeDigit(utility.farsinumber(insurance.getCost()+""),','))
                .setCancelable(true)
                .setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        insuranceRepository.delete(insurance.getId());
                        notifyDataSetChanged();

                    }
                })
                .setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).create();
        dialog.show();


    }

    @Override
    public int getItemCount() {
        return (insuranceList !=null)? insuranceList.size():0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder  {

        View root;
        TextView txt_name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            root=itemView.findViewById(R.id.field_row_item_ripple);
            txt_name=itemView.findViewById(R.id.field_row_item_name);


        }


    }
}
