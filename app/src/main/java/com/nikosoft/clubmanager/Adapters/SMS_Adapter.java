package com.nikosoft.clubmanager.Adapters;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.Data.DBRepository.SMSRepository;
import com.nikosoft.clubmanager.Data.DBRepository.StudentRepository;
import com.nikosoft.clubmanager.Data.Model.SMS;
import com.nikosoft.clubmanager.Data.Model.Student;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.Utiles.Utility;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class SMS_Adapter extends RecyclerView.Adapter<SMS_Adapter.ViewHolder>  {

    private List<SMS> smsList;
    //private List<Student> filtredStudentList;
    private Context context;
    private ClickAdapterListener listener;
    private static int currentSelectedIndex = -1;
    private SparseBooleanArray selectedItems;
    private int clickedItemPosition;

    @Inject
    SMSRepository smsRepository;
    @Inject
    StudentRepository studentRepository;

    @Inject
    Utility utility;

    Student student;

    public SMS_Adapter(List<SMS> smsList, Context context, ClickAdapterListener listener) {
        this.smsList = smsList;
        this.context = context;
        this.listener = listener;
        selectedItems = new SparseBooleanArray();
        ClubManager.getApplicationComponent().inject(this);
        //this.filtredStudentList = smsList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.sms_rcycler_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        SMS sms = smsList.get(position);
        if(sms.getSent()==2)
            holder.img_state.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_delivered));
        else if (sms.getSent()==1)
            holder.img_state.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_sent));
        else
            holder.img_state.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_fail));

        student=studentRepository.get(sms.getStudentId());
        holder.txt_name.setText(student.getName());
        holder.txt_message.setText(sms.getMessage());
        holder.txt_date.setText(utility.farsinumber(utility.getFormattedDate(sms.getDate(),0)));

        holder.itemView.setActivated(selectedItems.get(position, false));

        applyClickEvents(holder, position);
    }

    private void applyClickEvents(ViewHolder holder, final int position) {
        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                clickedItemPosition = position;
                listener.onRowClicked(position);

            }
        });

        holder.root.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                listener.onRowLongClicked(position);
                view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);

                return true;
            }
        });


    }

    @Override
    public int getItemCount() {

        return smsList == null ? 0 : smsList.size();
    }

   /* @Override
    public Filter getFilter() {
        filterAsync filterAsync = new filterAsync(this);
        return filterAsync;
    }

    private class filterAsync extends Filter {

        SMS_Adapter students_adapter;

        filterAsync(SMS_Adapter students_adapter) {
            this.students_adapter = students_adapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            return new FilterResults();
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            students_adapter.filter(charSequence.toString());
            students_adapter.notifyDataSetChanged();
        }
    }

    public void filter(String s) {
        List<Student> students = new StudentRepository().getAll();
        if (s.isEmpty()) {

            filtredStudentList = students;
        } else {
            List<Student> filtred_student = new ArrayList<>();
            for (Student student : students) {
                if (student.getName().toLowerCase().contains(s))
                    filtred_student.add(student);
            }
            filtredStudentList = filtred_student;
        }
    }*/

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {

        ImageView img_state;
        TextView txt_name;
        TextView txt_message;
        TextView txt_date;
        View root;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_state = itemView.findViewById(R.id.sms_row_img_state);
            txt_name = itemView.findViewById(R.id.sms_row_student_name);
            txt_message = itemView.findViewById(R.id.sms_row_message);
            txt_date = itemView.findViewById(R.id.sms_row_date);
            root = itemView.findViewById(R.id.sms_row_root);

            itemView.setOnLongClickListener(this);
        }

        @Override
        public boolean onLongClick(View view) {
            listener.onRowLongClicked(getAdapterPosition());
            view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
            return true;
        }
    }


    public void toggleSelection(int pos) {
        currentSelectedIndex = pos;
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        } else {
            selectedItems.put(pos, true);
            clickedItemPosition = pos;
        }
        notifyItemChanged(pos);
    }

    public void selectAll() {

        for (int i = 0; i < getItemCount(); i++)
            selectedItems.put(i, true);
        notifyDataSetChanged();

    }


    public void clearSelections() {
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List getSelectedItems() {
        List items =
                new ArrayList(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    public void removeData(int position) {

        //delete sms
        smsRepository.delete(smsList.get(position).getId());
        resetCurrentIndex();
        notifyDataSetChanged();
    }

    /*public void updateData() {
        Intent intent = new Intent(context, StudentModifyActivity.class);
        intent.putExtra("coachId", smsList.get(clickedItemPosition).getId());
        context.startActivity(intent);
        resetCurrentIndex();
    }*/

   /* public void selectItem(int position) {
        Intent intent = new Intent(context, StudentDetailActivity.class);
        intent.putExtra("studentId", smsList.get(clickedItemPosition).getId());
        context.startActivity(intent);
        resetCurrentIndex();
    }*/

    private void resetCurrentIndex() {
        currentSelectedIndex = -1;
    }

    public interface ClickAdapterListener {

        void onRowClicked(int position);

        void onRowLongClicked(int position);
    }
}
