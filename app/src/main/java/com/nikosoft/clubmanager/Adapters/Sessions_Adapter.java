package com.nikosoft.clubmanager.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.SparseBooleanArray;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.Data.DBRepository.SessionRepository;
import com.nikosoft.clubmanager.Data.Model.Session;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.SessionModify.SessionModifyActivity;
import com.nikosoft.clubmanager.Utiles.Utility;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class Sessions_Adapter extends RecyclerView.Adapter<Sessions_Adapter.ViewHolder> {

    private List<Session> sessionList;
    private Context context;
    private ClickAdapterListener listener;
    private static int currentSelectedIndex = -1;
    private SparseBooleanArray selectedItems;
    private int clickedItemPosition;
    private SessionRepository sessionRepository;
    @Inject
    Utility utility;

    public Sessions_Adapter(List<Session> sessionList, Context context, ClickAdapterListener listener) {
        this.sessionList = sessionList;
        this.context = context;
        this.listener = listener;
        selectedItems = new SparseBooleanArray();
        sessionRepository = new SessionRepository();
        ClubManager.getApplicationComponent().inject(this);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.session_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Session session = sessionList.get(position);

        if (session.getClub() != null)
            holder.txt_club.setText(session.getClub().getName());

        if (session.getField() != null)
            holder.txt_field.setText(session.getField().getTitle());

        if (session.getCoach() != null)
            holder.txt_coach.setText(session.getCoach().getName());

        holder.txt_time.setText(utility.farsinumber(session.getStartTime()) + context.getString(R.string.to) + utility.farsinumber(session.getEndTime()));

        if (session.getStudents().size() > 0)
            holder.txt_students.setText(utility.farsinumber(String.valueOf(session.getStudents().size())) + context.getString(R.string.people));
        else
            holder.txt_students.setText(utility.farsinumber("0") + context.getString(R.string.people));


        String days = session.getDaysOfWeek();
        for (int i = 0; i < days.length(); i++) {
            switch (days.charAt(i)) {
                case '0':
                    holder.txt_sat.setSelected(true);
                    break;
                case '1':
                    holder.txt_sun.setSelected(true);
                    break;
                case '2':
                    holder.txt_mon.setSelected(true);
                    break;
                case '3':
                    holder.txt_tue.setSelected(true);
                    break;
                case '4':
                    holder.txt_wed.setSelected(true);
                    break;
                case '5':
                    holder.txt_thu.setSelected(true);
                    break;

            }
        }

        holder.itemView.setActivated(selectedItems.get(position, false));

        applyClickEvents(holder, position);
    }

    private void applyClickEvents(ViewHolder holder, final int position) {
        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                clickedItemPosition = position;
                listener.onRowClicked(position);

            }
        });

        holder.root.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                listener.onRowLongClicked(position);
                view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);

                return true;
            }
        });


    }

    @Override
    public int getItemCount() {

        return sessionList == null ? 0 : sessionList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {

        TextView txt_club, txt_field, txt_coach, txt_time, txt_students;
        TextView txt_sat, txt_sun, txt_mon, txt_tue, txt_wed, txt_thu;

        View root;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_club = itemView.findViewById(R.id.session_row_club_name);
            txt_field = itemView.findViewById(R.id.session_row_field_name);
            txt_coach = itemView.findViewById(R.id.session_row_coach_name);
            txt_time = itemView.findViewById(R.id.session_row_time);
            txt_students = itemView.findViewById(R.id.session_row_students);

            txt_sat = itemView.findViewById(R.id.session_row_saturday);
            txt_sun = itemView.findViewById(R.id.session_row_sunday);
            txt_mon = itemView.findViewById(R.id.session_row_monday);
            txt_tue = itemView.findViewById(R.id.session_row_tuesday);
            txt_wed = itemView.findViewById(R.id.session_row_wednesday);
            txt_thu = itemView.findViewById(R.id.session_row_thursday);

            root = itemView.findViewById(R.id.lay_session_row_root);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public boolean onLongClick(View view) {
            listener.onRowLongClicked(getAdapterPosition());
            view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
            return true;
        }
    }


    public void toggleSelection(int pos) {
        currentSelectedIndex = pos;
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        } else {
            selectedItems.put(pos, true);
            clickedItemPosition = pos;
        }
        notifyItemChanged(pos);
    }

    public void selectAll() {

        for (int i = 0; i < getItemCount(); i++)
            selectedItems.put(i, true);
        notifyDataSetChanged();

    }


    public void clearSelections() {
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List getSelectedItems() {
        List items =
                new ArrayList(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    public void removeData(int position) {

        sessionRepository.delete(sessionList.get(position).getId());
        resetCurrentIndex();
        notifyDataSetChanged();
    }

    public void updateData() {
        Intent intent = new Intent(context, SessionModifyActivity.class);
        intent.putExtra("sessionId", sessionList.get(clickedItemPosition).getId());
        context.startActivity(intent);
        resetCurrentIndex();
    }

    private void resetCurrentIndex() {
        currentSelectedIndex = -1;
    }

    public interface ClickAdapterListener {

        void onRowClicked(int position);

        void onRowLongClicked(int position);
    }
}
