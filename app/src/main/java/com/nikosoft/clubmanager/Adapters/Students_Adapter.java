package com.nikosoft.clubmanager.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.SparseBooleanArray;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.Data.DBRepository.StudentRepository;
import com.nikosoft.clubmanager.Data.DBRepository.TuitionPaymentRepository;
import com.nikosoft.clubmanager.Data.Model.Student;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.StudentDetail.StudentDetailActivity;
import com.nikosoft.clubmanager.StudentModify.StudentModifyActivity;
import com.nikosoft.clubmanager.Utiles.Utility;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class Students_Adapter extends RecyclerView.Adapter<Students_Adapter.ViewHolder> implements Filterable {

    private List<Student> studentList;
    private List<Student> filtredStudentList;
    private Context context;
    private ClickAdapterListener listener;
    private static int currentSelectedIndex = -1;
    private SparseBooleanArray selectedItems;
    private int clickedItemPosition;

    @Inject
    StudentRepository studentRepository;
    @Inject
    TuitionPaymentRepository tuitionPaymentRepository;

    @Inject
    Utility utility;

    public Students_Adapter(List<Student> studentList, Context context, ClickAdapterListener listener) {
        this.studentList = studentList;
        this.context = context;
        this.listener = listener;
        selectedItems = new SparseBooleanArray();
        ClubManager.getApplicationComponent().inject(this);
        this.filtredStudentList = studentList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.coach_athlete_row_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        Student student = filtredStudentList.get(position);

        //if the coach's image is null
        if (utility.getImage(student.getImage()) == null) {
            holder.img_profile.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_account_circle));
        } else
            holder.img_profile.setImageBitmap(utility.getImage(student.getImage()));

        if (student.isActivityStatus()) {
            holder.img_profile.setBorderWidth(1);
            holder.img_profile.setBorderColor(context.getResources().getColor(R.color.white));
        }
        else{
            holder.img_profile.setBorderWidth(4);
            holder.img_profile.setBorderColor(context.getResources().getColor(R.color.red_light));
        }

        holder.txt_name.setText(student.getName());
        Object joindate = utility.getNormalPersianDate(student.getJoinDate());
        holder.txt_desc.setText(String.format("%s : %s", context.getString(R.string.join_date), utility.farsinumber(joindate.toString())));


        holder.itemView.setActivated(selectedItems.get(position, false));

        applyClickEvents(holder, position);
    }

    private void applyClickEvents(ViewHolder holder, final int position) {
        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                clickedItemPosition = position;
                listener.onRowClicked(position);

            }
        });

        holder.root.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                listener.onRowLongClicked(position);
                view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);

                return true;
            }
        });


    }

    @Override
    public int getItemCount() {

        return filtredStudentList == null ? 0 : filtredStudentList.size();
    }

    @Override
    public Filter getFilter() {
        filterAsync filterAsync = new filterAsync(this);
        return filterAsync;
    }

    private class filterAsync extends Filter {

        Students_Adapter students_adapter;

        filterAsync(Students_Adapter students_adapter) {
            this.students_adapter = students_adapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            return new FilterResults();
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            students_adapter.filter(charSequence.toString());
            students_adapter.notifyDataSetChanged();
        }
    }

    public void filter(String s) {
        List<Student> students = new StudentRepository().getAll();
        if (s.isEmpty()) {

            filtredStudentList = students;
        } else {
            List<Student> filtred_student = new ArrayList<>();
            for (Student student : students) {
                if (student.getName().toLowerCase().contains(s))
                    filtred_student.add(student);
            }
            filtredStudentList = filtred_student;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {

        CircularImageView img_profile;
        TextView txt_name;
        TextView txt_desc;
        View root;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_profile = itemView.findViewById(R.id.coach_athlete_item_img_profile);
            txt_name = itemView.findViewById(R.id.coach_athlete_item_txt_name);
            txt_desc = itemView.findViewById(R.id.coach_athlete_item_txt_desc);
            root = itemView.findViewById(R.id.coach_athlete_item_root);

            itemView.setOnLongClickListener(this);
        }

        @Override
        public boolean onLongClick(View view) {
            listener.onRowLongClicked(getAdapterPosition());
            view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
            return true;
        }
    }


    public void toggleSelection(int pos) {
        currentSelectedIndex = pos;
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        } else {
            selectedItems.put(pos, true);
            clickedItemPosition = pos;
        }
        notifyItemChanged(pos);
    }

    public void selectAll() {

        for (int i = 0; i < getItemCount(); i++)
            selectedItems.put(i, true);
        notifyDataSetChanged();

    }


    public void clearSelections() {
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List getSelectedItems() {
        List items =
                new ArrayList(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    public void removeData(int position) {
        //delete students image from storage
        utility.deleteFile(studentList.get(position).getImage());
        //delete student tuition payments
        tuitionPaymentRepository.deleteByStudent(studentList.get(position).getId());
        //delete student
        studentRepository.delete(studentList.get(position).getId());
        resetCurrentIndex();
        notifyDataSetChanged();
    }

    public void updateData() {
        Intent intent = new Intent(context, StudentModifyActivity.class);
        intent.putExtra("studentId", studentList.get(clickedItemPosition).getId());
        context.startActivity(intent);
        resetCurrentIndex();
    }

    public void selectItem(int position) {
        Intent intent = new Intent(context, StudentDetailActivity.class);
        intent.putExtra("studentId", studentList.get(clickedItemPosition).getId());
        context.startActivity(intent);
        resetCurrentIndex();
    }

    private void resetCurrentIndex() {
        currentSelectedIndex = -1;
    }

    public interface ClickAdapterListener {

        void onRowClicked(int position);

        void onRowLongClicked(int position);
    }
}
