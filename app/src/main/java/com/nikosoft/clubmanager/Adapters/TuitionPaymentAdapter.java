package com.nikosoft.clubmanager.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.Data.Model.TuitionPayment;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.StudentDetail.Fragments.TuitionPayment.ActivityHistoryAccounting;
import com.nikosoft.clubmanager.Utiles.Utility;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import java.util.List;

import javax.inject.Inject;

import saman.zamani.persiandate.PersianDateFormat;

import static android.view.animation.Animation.RELATIVE_TO_SELF;

public class TuitionPaymentAdapter extends ExpandableRecyclerViewAdapter<TuitionPaymentAdapter.ActivityHistoryViewHolder, TuitionPaymentAdapter.AccountingRowViewHolder> {


    Context context;
    @Inject
    Utility utility;

    public TuitionPaymentAdapter(Context context, List<? extends ExpandableGroup> groups) {
        super(groups);
        ClubManager.getApplicationComponent().inject(this);
        this.context = context;
    }

    @Override
    public ActivityHistoryViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = (View) LayoutInflater.from(context).inflate(R.layout.row_recycler_year_title, parent,false);
        return new ActivityHistoryViewHolder(view);
    }

    @Override
    public AccountingRowViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = (View) LayoutInflater.from(context).inflate(R.layout.row_recycler_year_more, parent,false);
        return new AccountingRowViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(AccountingRowViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        TuitionPayment row=((ActivityHistoryAccounting) group).getItems().get(childIndex);
        holder.setAccountingRow(row);
    }

    @Override
    public void onBindGroupViewHolder(ActivityHistoryViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.setAhTitle(group);
    }




    public class ActivityHistoryViewHolder extends GroupViewHolder {

        private TextView ahTitle;
        private ImageView arrow;

        public ActivityHistoryViewHolder(View itemView) {
            super(itemView);

            ahTitle = itemView.findViewById(R.id.txt_row_year_title);
            arrow=itemView.findViewById(R.id.row_year_arrow);
        }

        public void setAhTitle(ExpandableGroup expandableGroup) {
            ahTitle.setText(expandableGroup.getTitle());
        }

        @Override
        public void expand() {
            animateExpand();
        }

        @Override
        public void collapse() {
            animateCollapse();
        }

        private void animateExpand() {
            RotateAnimation rotate =
                    new RotateAnimation(360, 180, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
            rotate.setDuration(300);
            rotate.setFillAfter(true);
            arrow.setAnimation(rotate);
        }

        private void animateCollapse() {
            RotateAnimation rotate =
                    new RotateAnimation(180, 360, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
            rotate.setDuration(300);
            rotate.setFillAfter(true);
            arrow.setAnimation(rotate);
        }
    }


    public class AccountingRowViewHolder extends ChildViewHolder {

        private TextView startDate,endDate,cost,payDate,club;

        public AccountingRowViewHolder(View itemView) {
            super(itemView);

            startDate = itemView.findViewById(R.id.txt_row_activity_history_startdate);
            //endDate = itemView.findViewById(R.id.txt_row_activity_history_enddate);
            payDate = itemView.findViewById(R.id.txt_row_activity_history_paydate);
            cost = itemView.findViewById(R.id.txt_row_activity_history_cost);
            club = itemView.findViewById(R.id.txt_row_activity_history_club);
        }

        public void setAccountingRow(TuitionPayment tuitionPayment) {
            PersianDateFormat pdformater = new PersianDateFormat("F");
            String due_date= pdformater.format(utility.getPersianDate(tuitionPayment.getDueDate()))+ "  " +utility.farsinumber(String.valueOf(utility.getPersianDate(tuitionPayment.getDueDate()).getShYear())) ;

            startDate.setText(due_date);
            payDate.setText(utility.farsinumber((utility.getFormattedDate(tuitionPayment.getPayDate(),0))));
            cost.setText(utility.farsinumber(utility.threeDigit(tuitionPayment.getTuition().getTuitionCost()+"",',')));
            club.setText(tuitionPayment.getClub().getName());
        }


    }
}
