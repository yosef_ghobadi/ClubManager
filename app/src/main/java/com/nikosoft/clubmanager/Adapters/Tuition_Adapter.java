package com.nikosoft.clubmanager.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.nikosoft.clubmanager.Data.DBRepository.TuitionRepository;
import com.nikosoft.clubmanager.Data.Model.Tuition;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.Tuition.OnTuitionEditMenuClickListener;
import com.nikosoft.clubmanager.Utiles.Utility;

import java.util.List;

public class Tuition_Adapter extends RecyclerView.Adapter<Tuition_Adapter.ViewHolder> {

    OnTuitionEditMenuClickListener onEditMenuClickListener;
    Utility utility;
    TuitionRepository tuitionRepository;
    public Context context;
    List<Tuition> tuitionList;

    public Tuition_Adapter(Context context, List<Tuition> tuitionList, OnTuitionEditMenuClickListener onTuitionEditMenuClickListener) {
        this.context = context;
        this.tuitionList = tuitionList;
        tuitionRepository = new TuitionRepository();
        utility = new Utility();
        this.onEditMenuClickListener = onTuitionEditMenuClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.field_row_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Tuition tuition = tuitionList.get(position);
        holder.txt_name.setText(new StringBuilder()
                .append(utility.threeDigit(utility.farsinumber(tuition.getTuitionCost() + ""), ','))
                .append("\n")
                .append(utility.farsinumber(utility.getFormattedDate(tuition.getStartDate(), 0)))
                .append(context.getString(R.string.to)).append(utility.farsinumber(utility.getFormattedDate(tuition.getEndDate(), 0))).toString()
        );

        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] array = new String[]{context.getString(R.string.edit), context.getString(R.string.delete)};
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setItems(array, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i == 0)
                            onEditMenuClickListener.onEditMenuClick(tuition);
                        else
                            deleteTuition(tuition);


                    }
                });
                builder.create().show();
            }
        });
    }

    private void deleteTuition(Tuition tuition) {

        AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.delete_tuition))
                .setMessage(utility.threeDigit(utility.farsinumber(tuition.getTuitionCost() + ""), ','))
                .setCancelable(true)
                .setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        tuitionRepository.delete(tuition.getId());
                        notifyDataSetChanged();

                    }
                })
                .setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).create();
        dialog.show();


    }

    @Override
    public int getItemCount() {
        return (tuitionList != null) ? tuitionList.size() : 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        View root;
        TextView txt_name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            root = itemView.findViewById(R.id.field_row_item_ripple);
            txt_name = itemView.findViewById(R.id.field_row_item_name);


        }


    }
}
