package com.nikosoft.clubmanager.Base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * Created by Yosef on 12/01/2019.
 */

public abstract class BaseFragment extends Fragment {

    public View rootView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if(rootView==null)
        {
            rootView=inflater.inflate(getLayout(),container,false);
            setupViews() ;
        }

        return rootView;
    }

    public abstract int getLayout() ;
    public abstract void setupViews() ;

}
