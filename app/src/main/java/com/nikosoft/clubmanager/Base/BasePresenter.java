package com.nikosoft.clubmanager.Base;


/**
 * Created by Yosef on 02/02/2019.
 */

public interface BasePresenter<T extends BaseView> {
    void attachView(T view);
    void detachView();
}
