package com.nikosoft.clubmanager.Base;

import android.content.Context;

/**
 * Created by Yosef on 02/02/2019.
 */

public interface BaseView {
    Context getViewContext();
}
