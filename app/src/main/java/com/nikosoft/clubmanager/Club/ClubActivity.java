package com.nikosoft.clubmanager.Club;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.ClubModify.ClubModifyActivity;
import com.nikosoft.clubmanager.Data.DBRepository.ClubRepository;
import com.nikosoft.clubmanager.Data.Model.Club;
import com.nikosoft.clubmanager.Data.Model.Coach;
import com.nikosoft.clubmanager.Data.Model.Field;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.Utiles.Utility;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ClubActivity extends AppCompatActivity implements ClubContract.View {

    @BindView(R.id.club_detail_collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.toolbar_club)
    Toolbar toolbar;
    @BindView(R.id.txt_club_info_monthly_cost)
    TextView monthly_cost;
    @BindView(R.id.txt_club_info_tuition_day)
    TextView tuition_day;
    @BindView(R.id.txt_club_info_tuition)
    TextView tuition;
    @BindView(R.id.txt_info_club_fields)
    TextView fields;
    @BindView(R.id.txt_info_club_coaches)
    TextView coaches;
    @BindView(R.id.txt_club_info_athlete_count)
    TextView students;
    @BindView(R.id.txt_club_detail_name)
    TextView name;
    @BindView(R.id.txt_club_detail_activity_times)
    TextView activity_time;
    @BindView(R.id.fab_club_detail)
    FloatingActionButton fab_edit_club;


    Club club = new Club();
    int clubId = 0;
    ClubContract.Presenter presenter;

    @Inject
    ClubRepository clubRepository;
    @Inject
    Utility utility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_club);

        //get club id from bundle for edit if exist
        getClubId();

        ClubManager.getApplicationComponent().inject(this);
        AppCompatDelegate.setDefaultNightMode(utility.getThemeNight());

        presenter.attachView(this);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);



        setupView();
    }

    /**
     * get club id from intent
     */
    private void getClubId() {
        Bundle bundle = getIntent().getExtras();
        int id = 0;
        String name = "";

        try {
            id = bundle.getInt("id");
            name = bundle.getString("name");
        } catch (Exception e) {
        }

        if (id > 0) {
            //getSupportActionBar().setTitle(name);
            clubId = id;

        }
    }

    /**
     * injecting presenter to activity by dagger2
     *
     * @param presenter
     */
    @Inject
    public void setPresenter(ClubPresenter presenter) {
        this.presenter = presenter;
        presenter.clubId = clubId;
    }

    /**
     * setup all view in activity
     */
    private void setupView() {
        //action bar

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.title_activity_club));

        collapsingToolbarLayout.setExpandedTitleTextColor(Objects.requireNonNull(ContextCompat.getColorStateList(getViewContext(), android.R.color.transparent)));

        //club name
        name.setText(club.getName());

        //activity times
        activity_time.setText(utility.farsinumber(club.getStartTime() + "") + getString(R.string.to) + utility.farsinumber(club.getEndTime() + ""));

        //monthly_cost
        monthly_cost.setText(utility.farsinumber(utility.threeDigit(club.getMonthlyCost() + "", ',')) + getString(R.string.toman));

        //tuition_day
        tuition_day.setText(utility.farsinumber(club.getTuitionDay() + "") + getString(R.string.every_month));

        //tuition
        tuition.setText(utility.farsinumber(utility.threeDigit(club.gettuition().getTuitionCost() + "", ',')) + getString(R.string.toman));

        //students
        if(club.getStudents()!=null )
            students.setText(utility.farsinumber(club.getStudents().size()+"")+getString(R.string.people));

        //fields
        fields.setText("");
        if (club.getFields()!=null & club.getFields().size() > 0) {
            for (Field field : club.getFields()) {
                fields.setText(fields.getText() + field.getTitle() + " , ");
            }
            fields.setText(fields.getText().subSequence(0, fields.getText().length() - 2));
        }

        //coaches
        coaches.setText("");
        if (club.getCoaches()!=null & club.getCoaches().size() > 0) {
            for (Coach coach : club.getCoaches()) {
                coaches.setText(coaches.getText() + coach.getName() + " , ");
            }
            coaches.setText(coaches.getText().subSequence(0, coaches.getText().length() - 2));
        }


        //fab
        fab_edit_club.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getViewContext(), ClubModifyActivity.class);
                intent.putExtra("id", clubId);
                startActivity(intent);
            }
        });


    }

    /**
     * back arrow action
     *
     * @return
     */
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public Context getViewContext() {
        return this;
    }

    @Override
    public void club(Club club) {
        this.club = club;
    }
}
