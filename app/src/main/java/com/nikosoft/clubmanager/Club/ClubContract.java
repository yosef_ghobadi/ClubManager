package com.nikosoft.clubmanager.Club;

import com.nikosoft.clubmanager.Base.BasePresenter;
import com.nikosoft.clubmanager.Base.BaseView;
import com.nikosoft.clubmanager.Data.Model.Club;

public interface ClubContract {

     interface View extends BaseView{
        void club(Club club);
    }

     interface Presenter extends BasePresenter<View>{
        void getClub(int clubId);
    }
}
