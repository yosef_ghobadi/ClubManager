package com.nikosoft.clubmanager.Club;

import com.nikosoft.clubmanager.Data.DataSource;

import javax.inject.Inject;

public class ClubPresenter implements ClubContract.Presenter {

    public ClubContract.View view;
    private DataSource dataSource;
    public int clubId=0;

    @Inject
    public ClubPresenter(DataSource dataSource){
        this.dataSource=dataSource;
    }
    @Override
    public void attachView(ClubContract.View view) {
        this.view=view;
        getClub(clubId);
    }

    @Override
    public void getClub(int clubId) {
        view.club(dataSource.getClub(clubId));
    }

    @Override
    public void detachView() {
        this.view=null;
    }
}
