package com.nikosoft.clubmanager;

import android.content.Context;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.multidex.MultiDexApplication;

import com.nikosoft.clubmanager.Utiles.FontsOverride;
import com.nikosoft.clubmanager.Utiles.Utility;
import com.nikosoft.clubmanager.zDagger2.Components.ApplicationComponent;
import com.nikosoft.clubmanager.zDagger2.Components.DaggerApplicationComponent;
import com.nikosoft.clubmanager.zDagger2.Modules.ApplicationModule;
import com.nikosoft.clubmanager.zDagger2.Modules.DataSourceModule;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import ir.tapsell.sdk.Tapsell;

/**
 * Created by Yosef on 25/01/2019.
 */

public class ClubManager extends MultiDexApplication {

    private static ApplicationComponent applicationComponent;

    String font="iransans_web_light.ttf";
    String dbName="ClubManagerDB.realm";
    @Inject
    Context context;
    @Inject
    Utility utility;




    @Override
    public void onCreate() {

        super.onCreate();


        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);


        applicationComponent= DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .dataSourceModule(new DataSourceModule())
                .build();
        getApplicationComponent().inject(this);

        utility.setThemeNight(false,context);

        setFont(font);

        initeDatabase(context);

        //tebsell ads
        Tapsell.initialize(this, "ccecorhqsehpgtbpcfdfgrnpjgosnsqodmrmeaekrminiiaikqhjjohejhjqtstofiojcm");

    }

    public static ApplicationComponent getApplicationComponent()
    {
        return applicationComponent;
    }
    void setFont(String fontName)
    {

        FontsOverride.setDefaultFont(this, "DEFAULT", fontName);
        FontsOverride.setDefaultFont(this, "MONOSPACE", fontName);
        FontsOverride.setDefaultFont(this, "SERIF", fontName);
        FontsOverride.setDefaultFont(this, "SANS_SERIF", fontName);
    }
    void initeDatabase(Context context)
    {
        Realm.init(context);
        RealmConfiguration configuration=new RealmConfiguration.Builder()
        .name(dbName)
                .schemaVersion(1)
                .migration(new ClubManagerMigration())
                .build();
        Realm.setDefaultConfiguration(configuration);

    }
}
