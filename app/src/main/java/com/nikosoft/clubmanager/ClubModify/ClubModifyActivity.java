package com.nikosoft.clubmanager.ClubModify;

import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.textfield.TextInputLayout;
import com.mcsoft.timerangepickerdialog.RangeTimePickerDialog;
import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.Data.DBRepository.ClubRepository;
import com.nikosoft.clubmanager.Data.DBRepository.CoachRepository;
import com.nikosoft.clubmanager.Data.DBRepository.FieldRepository;
import com.nikosoft.clubmanager.Data.DBRepository.TuitionRepository;
import com.nikosoft.clubmanager.Data.Model.Club;
import com.nikosoft.clubmanager.Data.Model.Field;
import com.nikosoft.clubmanager.Data.Model.Tuition;
import com.nikosoft.clubmanager.Field.FieldActivity;
import com.nikosoft.clubmanager.Home.HomeActivity;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.Utiles.Utility;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.ganfra.materialspinner.MaterialSpinner;
import io.realm.RealmList;

public class ClubModifyActivity extends AppCompatActivity implements ClubModifyContract.View, RangeTimePickerDialog.ISelectedTime {


    @BindView(R.id.spinner_tuition_day)
    MaterialSpinner spinner_tuitionDay;
    @BindView(R.id.toolbar_other)
    Toolbar toolbar;

    @BindView(R.id.txt_name)
    EditText txt_name;
    @BindView(R.id.txt_tuition)
    AutoCompleteTextView txt_tuition;

    @BindView(R.id.input_monthly_cost)
    TextInputLayout lay_monthly_cost;
    @BindView(R.id.txt_monthly_cost)
    EditText txt_monthly_cost;

    @BindView(R.id.txt_activity_time_caption)
    TextView activity_time_caption;
    @BindView(R.id.txt_fields_caption)
    TextView fields_caption;


    @BindView(R.id.input_description)
    TextInputLayout lay_description;
    @BindView(R.id.txt_description)
    EditText txt_description;

    @BindView(R.id.club_txt_fields)
    TextView txt_fields;
    @BindView(R.id.club_txt_time)
    TextView txt_time;
    @BindView(R.id.lay_add_coach)
    View addClub;
    /**
     * for show days of month
     */
    ArrayAdapter<String> days_adapter;


    /**
     * for check or uncheck items by user
     */
    //boolean[] checkedTuitions = new boolean[0];
    boolean[] checkedFields = new boolean[0];

    /**
     * list of coachs and fields that saved in db
     */
    List<Tuition> TuitionList = new ArrayList<>();
    List<Field> FieldList = new ArrayList<>();

    ClubModifyContract.Presenter presenter;

    private int startHour = 0;
    private int startMinute = 0;
    private int endHour = 0;
    private int endMinute = 0;
    int id = 0;
    private boolean EDIT_MODE=false;

    private int SELECTED_TUITION_DAY=-1;

    AlertDialog.Builder builder;
    private AlertDialog dialog_fields;
    @Inject
    TuitionRepository tuitionRepository;
    @Inject
    Utility utility;
    @Inject
    CoachRepository coachRepository;
    @Inject
    FieldRepository fieldRepository;
    @Inject
    ClubRepository clubRepository;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //set dependency injection by dagger2
        ClubManager.getApplicationComponent().inject(this);
        //set day night theme
        Log.i("THEME",utility.getThemeNight()+"");
        AppCompatDelegate.setDefaultNightMode(utility.getThemeNight(getViewContext()));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_club_modify);

        //setting butter knife lib
        ButterKnife.bind(this);



        //load field list for show in dialog
        //new loadFieldsDialog().execute();


        //set presenter
        presenter.attachView(this);

        //set activity views
        setupViews();
    }

    @Inject
    public void setPresenter(ClubModifyPresenter presenter)
    {
        this.presenter=presenter;
    }
    /**
     * back arrow action
     *
     * @return
     */
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }




    /**
     * setup all view for ClubModifyactivity
     */
    private void setupViews() {
        setSupportActionBar(toolbar);
        //نمایش دکمه برگشت
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //تنظیم ایکون برای ورودی ها
        utility.setViewDrawable(txt_name,getViewContext(),R.drawable.ic_gym,4,10);
        utility.setViewDrawable(txt_tuition,getViewContext(),R.drawable.ic_contract_price,4,10);
        utility.setViewDrawable(txt_monthly_cost,getViewContext(),R.drawable.ic_salary_sum,4,10);
        utility.setViewDrawable(activity_time_caption,getViewContext(),R.drawable.ic_activity_time,4,10);
        utility.setViewDrawable(fields_caption,getViewContext(),R.drawable.ic_fields,4,10);
        utility.setViewDrawable(txt_description,getViewContext(),R.drawable.ic_description,4,10);




        // پر کردن لیست مربیان رشته ها و روزهای ماه
        days_adapter = new ArrayAdapter<String>(getViewContext(), android.R.layout.simple_spinner_dropdown_item, utility.getDays());
        spinner_tuitionDay.setAdapter(days_adapter);
        spinner_tuitionDay.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                //get selected item position
                SELECTED_TUITION_DAY=position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        //spinner_tuitionDay rtl direction
        if(!utility.deviceLanguageDirection())
        spinner_tuitionDay.setRtl();

        //اگر دستور ویرایش ارسال شده بود
        Bundle bundle = getIntent().getExtras();

        try {
            id = bundle.getInt("id");
            if (id > 0) {
                setEditValues(id);
                EDIT_MODE=true;
            }
        } catch (Exception e) {
        }




        //جدا کردن ارقام هزینه ماهیانه و فارسی کردن
        txt_monthly_cost.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                utility.handleNumberInput(txt_monthly_cost,this);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        //جدا کردن ارقام شهریه و فارسی کردن
        txt_tuition.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                utility.handleNumberInput(txt_tuition,this);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        if(TuitionList!=null) {
            String[] array=new String[TuitionList.size()];
            for(int i=0;i<TuitionList.size();i++)
            {
                array[i]=utility.farsinumber(TuitionList.get(i).getTuitionCost()+"");
            }
            txt_tuition.setThreshold(0);
            txt_tuition.setAdapter(new ArrayAdapter<>(getViewContext(),android.R.layout.simple_list_item_1,array));

        }


        //انتخاب ساعات فعالیت
        txt_time.setOnClickListener(view -> {

            //set start and end time
            startHour=(startHour==0)?16:startHour;
            startMinute=(startMinute==0)?30:startMinute;

            endHour=(endHour==0)?22:endHour;
            endMinute=(endMinute==0)?30:endMinute;

            // Create an instance of the dialog fragment and show it
            RangeTimePickerDialog dialog = new RangeTimePickerDialog();
            dialog.newInstance(R.color.colorPrimary, R.color.gray_overlay, R.color.white, R.color.colorAccent, true);
            dialog.setRadiusDialog(20); // Set radius of dialog (default is 50)
            dialog.setIs24HourView(true); // Indicates if the format should be 24 hours
            dialog.setTextBtnPositive(getString(R.string.ok));
            dialog.setTextBtnNegative(getString(R.string.cancel));
            dialog.setTextTabStart(getString(R.string.start_time_caption));
            dialog.setTextTabEnd(getString(R.string.end_time_caption));
            dialog.setInitialStartClock(startHour, startMinute);
            dialog.setInitialEndClock(endHour, endMinute);
            dialog.setValidateRange(true);
            dialog.setMessageErrorRangeTime(getString(R.string.time_start_end_error));
            FragmentManager fragmentManager = getFragmentManager();
            dialog.show(fragmentManager, "");
        });

        //نمایش لیست رشته ها
        txt_fields.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getViewContext(), FieldActivity.class);
                AlertDialog.Builder builder=new AlertDialog.Builder(getViewContext());
                dialog_fields = showFieldMultiChoiceDialog(
                        getString(R.string.select_fields), FieldList, txt_fields, checkedFields, intent,builder).create();
                dialog_fields.show();
            }
        });


        //ذخیره باشگاه
        addClub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String sminute, eminute;
                if (startMinute == 0)
                    sminute = "00";
                else
                    sminute = startMinute + "";
                if (endMinute == 0)
                    eminute = "00";
                else
                    eminute = endMinute + "";

                //check empty club name
                if(utility.isEmpty(txt_name)) {
                    utility.setError(txt_name, getString(R.string.emtpy_value_error));
                    return;
                }
                else {
                    if (!EDIT_MODE)
                    {
                        //check exist club name
                        for (Club item : presenter.getClubList()) {
                            if (item.getName().equals(txt_name.getText().toString())) {
                                utility.setError(txt_name, getString(R.string.exist_value_error));
                                return;
                            }
                        }
                    }
                }

                //check empty monthly cost
                if(utility.isEmpty(txt_monthly_cost))
                    utility.setError(txt_monthly_cost, getString(R.string.emtpy_value_error));

                //check empty monthly cost
                else if(utility.isEmpty(txt_tuition))
                    utility.setError(txt_tuition, getString(R.string.emtpy_value_error));

                //check unselected tuition day
                else if( SELECTED_TUITION_DAY==-1)
                    utility.setError(spinner_tuitionDay, getString(R.string.unselected_value_error));
                else {

                    int tuition_cost=Integer.parseInt(utility.removeSpaces(txt_tuition.getText().toString()));
                    Tuition tuition=tuitionRepository.getTuition(utility.getFirstThisYear(),utility.getEndThisYear());
                    if(tuition==null)
                    {
                        int id=tuitionRepository.save(new Tuition(0,tuition_cost,utility.getFirstThisYear(),utility.getEndThisYear()));
                        tuition=new Tuition(id,tuition_cost,utility.getFirstThisYear(),utility.getEndThisYear());
                        
                    }
                    else{
                        //if a tuition exist with entered date, update it
                        tuition=new Tuition(tuition.getId(),tuition_cost,utility.getFirstThisYear(),utility.getEndThisYear());
                        tuitionRepository.save(tuition);
                    }
                    clubRepository.save(new Club(id, txt_name.getText().toString(),
                            Integer.parseInt(utility.removeSpaces(txt_monthly_cost.getText().toString())),
                            Integer.parseInt(spinner_tuitionDay.getSelectedItem().toString()),
                            startHour + ":" + sminute,
                            endHour + ":" + eminute,
                            txt_description.getText().toString(),
                            tuition,
                            getSelectedFileds(checkedFields, FieldList)));

                    Intent intent = new Intent(getViewContext(), HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
            }
        });


    }

    /**
     * set selected club data to view for edit it
     *
     * @param clubId
     */
    private void setEditValues(int clubId) {
        Club club = clubRepository.getClub(clubId);
        txt_name.setText(club.getName());

        txt_monthly_cost.setText(utility.farsinumber(utility.threeDigit(club.getMonthlyCost() + "", ' ')));

        if(club.gettuition()!=null)
        txt_tuition.setText(utility.farsinumber(utility.threeDigit(club.gettuition().getTuitionCost() + "", ' ')));

        spinner_tuitionDay.setSelection(club.getTuitionDay() );

        if(!club.getStartTime().equals("0:00")) {
            txt_time.setText(getString(R.string.start_time_caption) + utility.farsinumber(club.getStartTime())
                    + "\n" + getString(R.string.end_time_caption) + utility.farsinumber(club.getEndTime()));
            String[] start_time = club.getStartTime().split(":");
            String[] end_time = club.getEndTime().split(":");
            startHour = Integer.parseInt(start_time[0]);
            startMinute = Integer.parseInt(start_time[1]);
            endHour = Integer.parseInt(end_time[0]);
            endMinute = Integer.parseInt(end_time[1]);
        }
        else
            txt_time.setText(getString(R.string.choose));

        if (club.getFields().size() > 0) {
            txt_fields.setText("");
            for (Field item : club.getFields()) {
                txt_fields.setText(txt_fields.getText() + item.getTitle() + ",");
                for (int i = 0; i < FieldList.size(); i++) {
                    if (item.getId() == FieldList.get(i).getId()) {
                        checkedFields[i] = true;
                    }
                }
            }
            //برای حذف ویرگول اخر
            txt_fields.setText(txt_fields.getText().subSequence(0, txt_fields.getText().length() - 1));
        }



        txt_description.setText(club.getDescription());
    }



    /**
     * get selected fields by user
     *
     * @param checkedItems checked values
     * @param fields       fields list
     * @return
     */
    private RealmList<Field> getSelectedFileds(boolean[] checkedItems, List<Field> fields) {
        RealmList<Field> selectedFields = new RealmList<>();

        for (int i = 0; i < checkedItems.length; i++) {
            if (checkedItems[i])
                selectedFields.add(fields.get(i));
        }

        return selectedFields;
    }



    /**
     * show dialog for choose fields
     *
     * @param title        dialog title
     * @param itemsName    fields name
     * @param textView     textview that shows fields name after selection
     * @param checkedItems save checked fields
     * @param intent       start coaches activity for insert fields
     * @return
     */
    private AlertDialog.Builder showFieldMultiChoiceDialog( String title,
                                                           List<Field> itemsName,
                                                           TextView textView,
                                                           boolean[] checkedItems,
                                                           Intent intent,
                                                           AlertDialog.Builder builder) {

        builder.setTitle(title);
        builder.setCancelable(false);

        //ارایه ای برای نمایش نام ایتم ها
        int itemsSize = 0;

        if (itemsName != null)
            itemsSize = itemsName.size();

        String[] filds = new String[itemsSize];
        boolean[] checkedItems_temp =new boolean[itemsSize];

        if (itemsName != null) {
            for (int i = 0; i < itemsSize; i++) {
                filds[i] = itemsName.get(i).getTitle();
                checkedItems_temp[i] = checkedItems[i];
            }
        }
        builder.setMultiChoiceItems(filds, checkedItems_temp, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i, boolean b) {
                checkedItems_temp[i] = b;
            }
        });

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                textView.setText("");

                //مشخص کردن گزینه های انتخاب شده از روی ارایه check
                for (int j = 0; j < checkedItems_temp.length; j++) {
                    checkedItems[j] = checkedItems_temp[j];
                    if (checkedItems_temp[j]) {
                        textView.setText(textView.getText() + itemsName.get(j).getTitle() + " ,");
                    }
                }

                //برای حذف وبرگول اضافی آخر
                if (textView.getText().length() == 0)
                    textView.setText(R.string.choose);
                else
                    textView.setText(textView.getText().subSequence(0, textView.getText().length() - 1));
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                /*for (boolean item:checkedItems) {
                    item=false;
                }*/
            }
        });
        builder.setNeutralButton(R.string.add, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivity(intent);
            }
        });
        builder.setCancelable(true);
        return builder;
    }




    @Override
    public Context getViewContext() {
        return this;
    }


    /**
     * get tuition list from db
     * @param tuitions
     */
    @Override
    public void TuitionsList(List<Tuition> tuitions) {
        TuitionList = tuitions;
        //checkedTuitions = new boolean[tuitions.size()];
    }

    /**
     * get fields list from db
     *
     * @param fields
     */
    @Override
    public void FieldsList(List<Field> fields) {
        FieldList = fields;
        checkedFields = new boolean[fields.size()];
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
        clubRepository.close();
        coachRepository.close();
        fieldRepository.close();
    }

    /**
     * get selected start and end time
     *
     * @param hourStart
     * @param minuteStart
     * @param hourEnd
     * @param minuteEnd
     */
    @Override
    public void onSelectedTime(int hourStart, int minuteStart, int hourEnd, int minuteEnd) {
        String s_hour, s_minute, e_hour, e_minute;
        this.startHour = hourStart;
        this.startMinute = minuteStart;
        this.endHour = hourEnd;
        this.endMinute = minuteEnd;
        s_hour = String.valueOf(startHour);
        if (startMinute != 0)
            s_minute = String.valueOf(startMinute);
        else
            s_minute = "00";

        e_hour = String.valueOf(endHour);
        if (endMinute != 0)
            e_minute = String.valueOf(endMinute);
        else
            e_minute = "00";

        String startTime = getString(R.string.start_caption) + utility.farsinumber(s_hour + ":" + s_minute);
        String endTime = getString(R.string.end_caption) + utility.farsinumber(e_hour + ":" + e_minute);
        txt_time.setText(startTime + "\n" + endTime);
    }


}
