package com.nikosoft.clubmanager.ClubModify;

import com.nikosoft.clubmanager.Base.BasePresenter;
import com.nikosoft.clubmanager.Base.BaseView;
import com.nikosoft.clubmanager.Data.Model.Club;
import com.nikosoft.clubmanager.Data.Model.Field;
import com.nikosoft.clubmanager.Data.Model.Tuition;

import java.util.List;

/**
 * Created by Yosef on 16/02/2019.
 */

public interface ClubModifyContract {

    interface View extends BaseView{
        void TuitionsList(List<Tuition> tuitions);
        void FieldsList(List<Field> fields);


    }


    interface Presenter extends BasePresenter<View>{
        void getTuitions();
        void getFields();

        List<Club> getClubList();
    }
}
