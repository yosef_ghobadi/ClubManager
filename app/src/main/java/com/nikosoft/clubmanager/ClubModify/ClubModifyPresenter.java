package com.nikosoft.clubmanager.ClubModify;

import com.nikosoft.clubmanager.Data.DBRepository.ClubRepository;
import com.nikosoft.clubmanager.Data.DataSource;
import com.nikosoft.clubmanager.Data.Model.Club;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Yosef on 16/02/2019.
 */

public class ClubModifyPresenter implements ClubModifyContract.Presenter {

    private ClubModifyContract.View view;
    private DataSource dataSource;

    private List<Club> clubList;

    public void setClubList(List<Club> clubList) {
        this.clubList = clubList;
    }

    @Inject
    ClubRepository repository;

    @Inject
    ClubModifyPresenter(DataSource dataSource)
    {
        this.dataSource=dataSource;
    }
    @Override
    public void attachView(ClubModifyContract.View view) {
        this.view=view;
        getTuitions();
        getFields();
    }

    @Override
    public void detachView() {
        this.view=null;
    }

    @Override
    public void getTuitions() {
        view.TuitionsList(dataSource.getTuitions());
    }

    @Override
    public void getFields() {
        view.FieldsList(dataSource.getFields());
    }

    @Override
    public List<Club> getClubList() {
        getClubs();
        return clubList;
    }

    private void getClubs()
    {
        Observable<List<Club>> observable=Observable.just(repository.getAllAsync());
        observable.subscribe(new Observer<List<Club>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<Club> clubs) {
                setClubList(clubs);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }
}
