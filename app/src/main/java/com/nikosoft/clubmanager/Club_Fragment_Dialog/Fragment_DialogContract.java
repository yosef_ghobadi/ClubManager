package com.nikosoft.clubmanager.Club_Fragment_Dialog;

import com.nikosoft.clubmanager.Base.BasePresenter;
import com.nikosoft.clubmanager.Base.BaseView;
import com.nikosoft.clubmanager.Data.Model.Club;

import java.util.List;

/**
 * Created by Yosef on 18/02/2019.
 */

public interface Fragment_DialogContract {

    interface View extends BaseView{
        void showClubs (List<Club> clubList);
    }

    interface Presenter extends BasePresenter<View>{
        void getClubs();
    }
}
