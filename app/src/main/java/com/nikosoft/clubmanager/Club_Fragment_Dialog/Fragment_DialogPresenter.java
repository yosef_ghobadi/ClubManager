package com.nikosoft.clubmanager.Club_Fragment_Dialog;

import com.nikosoft.clubmanager.Data.DataSource;

import javax.inject.Inject;

/**
 * Created by Yosef on 18/02/2019.
 */

public class Fragment_DialogPresenter implements Fragment_DialogContract.Presenter {

    Fragment_DialogContract.View view;
    DataSource dataSource;

    @Inject
    public Fragment_DialogPresenter(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void attachView(Fragment_DialogContract.View view) {
        this.view = view;
        getClubs();

    }

    @Override
    public void detachView() {
        this.view = null;
    }


    @Override
    public void getClubs() {
        view.showClubs(dataSource.getClubs());
    }
}
