package com.nikosoft.clubmanager.Club_Fragment_Dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.nikosoft.clubmanager.Adapters.Club_Bottom_Sheet_view_Recycler_Adapter;
import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.ClubModify.ClubModifyActivity;
import com.nikosoft.clubmanager.Data.Model.Club;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.Utiles.Utility;

import java.util.List;

import javax.inject.Inject;

public class Fragment_dialog_modal extends BottomSheetDialogFragment implements Fragment_DialogContract.View {


    LinearLayoutManager manager;
    RecyclerView recy_modal;
    Club_Bottom_Sheet_view_Recycler_Adapter adapter;
    View addclub;
    private Fragment_DialogContract.Presenter presenter;
    @Inject
    Utility utility;
    public Fragment_dialog_modal() {

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ClubManager.getApplicationComponent().inject(this);
        AppCompatDelegate.setDefaultNightMode(utility.getThemeNight());
        presenter.attachView(this);

    }

    @Inject
    public void setPresenter(Fragment_DialogPresenter presenter)
    {
        this.presenter=presenter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root_view = inflater.inflate(R.layout.bottom_sheet_view, container, false);

        addclub=root_view.findViewById(R.id.lay_add_coach);
        addclub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getViewContext(), ClubModifyActivity.class));
                getDialog().dismiss();
            }
        });

        recy_modal = root_view.findViewById(R.id.recy_list_view_club);

        manager = new LinearLayoutManager(getViewContext(), RecyclerView.VERTICAL, false);

        recy_modal.setAdapter(adapter);

        recy_modal.setLayoutManager(manager);


        return root_view;
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    /**
     *
     * @param clubList list of saved club in db
     */
    @Override
    public void showClubs(List<Club> clubList) {
        adapter = new Club_Bottom_Sheet_view_Recycler_Adapter(getViewContext(), clubList,this);
        adapter.notifyDataSetChanged();
    }
}
