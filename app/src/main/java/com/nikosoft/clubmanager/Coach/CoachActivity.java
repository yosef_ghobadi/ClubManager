package com.nikosoft.clubmanager.Coach;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nikosoft.clubmanager.Adapters.Coaches_Adapter;
import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.CoachModify.CoachModifyActivity;
import com.nikosoft.clubmanager.Data.Model.Coach;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.Utiles.Utility;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CoachActivity extends AppCompatActivity implements CoachContract.View, Coaches_Adapter.ClickAdapterListener {

    @BindView(R.id.coach_modify_recycler)
    RecyclerView recyclerView;
    @BindView(R.id.fab_add_coach)
    FloatingActionButton fab_addCoach;
    Coaches_Adapter coaches_adapter;
    CoachContract.Presenter presenter;

    private ActionModeCallback actionModeCallback;
    private ActionMode actionMode;
    private AlertDialog.Builder alert_builder;
    //detect when long press is enabled
    private boolean isLongselectionEnabled = false;

    @Inject
    Utility utility;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ClubManager.getApplicationComponent().inject(this);

        AppCompatDelegate.setDefaultNightMode(utility.getThemeNight());

        setContentView(R.layout.activity_coaches);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        presenter.attachView(this);

        setupViews();

    }

    @Inject
    public void setPresenter(CoachPresenter presenter)
    {
        this.presenter=presenter;
    }
    private void setupViews() {
        alert_builder = new AlertDialog.Builder(getViewContext());
        fab_addCoach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getViewContext(), CoachModifyActivity.class));
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(getViewContext()));
        recyclerView.setAdapter(coaches_adapter);
        actionModeCallback = new ActionModeCallback();



    }

    @Override
    public void CoachList(List<Coach> coachList) {
        coaches_adapter = new Coaches_Adapter(coachList, getViewContext(), this);
    }

    @Override
    public Context getViewContext() {
        return this;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }


    @Override
    public void onRowClicked(int position) {
        if (isLongselectionEnabled)
            enableActionMode(position);
        else
            coaches_adapter.selectItem(position);
    }

    @Override
    public void onRowLongClicked(int position) {
        isLongselectionEnabled=true;
        enableActionMode(position);
    }

    private void enableActionMode(int position) {
        if (actionMode == null) {
            actionMode = startSupportActionMode(actionModeCallback);
        }
        toggleSelection(position);
    }

    private void toggleSelection(int position) {
        coaches_adapter.toggleSelection(position);
        int count = coaches_adapter.getSelectedItemCount();

        if (count == 0) {
            actionMode.finish();
            actionMode = null;
            isLongselectionEnabled=false;
        } else {
            actionMode.setTitle(String.valueOf(count));
            actionMode.invalidate();
        }
    }

    private void selectAll() {
        try {
            coaches_adapter.selectAll();
            int count = coaches_adapter.getSelectedItemCount();

            if (count == 0) {
                actionMode.finish();
            } else {
                actionMode.setTitle(String.valueOf(count));
                actionMode.invalidate();
            }

            actionMode = null;
        } catch (Exception ex) {
        }
    }

    private void deleteRows() {
        List selectedItemPositions =
                coaches_adapter.getSelectedItems();
        for (int i = selectedItemPositions.size() - 1; i >= 0; i--) {
            coaches_adapter.removeData((Integer) selectedItemPositions.get(i));
        }
        isLongselectionEnabled=false;
        coaches_adapter.notifyDataSetChanged();


        actionMode = null;
    }

    private class ActionModeCallback implements ActionMode.Callback {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.menu_coach_list_option, menu);

            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            //Log.d("API123", "here");
            switch (item.getItemId()) {


                case R.id.coach_menu_delete:
                    // delete all the selected rows
                    alert_builder.setTitle(getViewContext().getString(R.string.warning));
                    alert_builder.setMessage(getViewContext().getString(R.string.are_you_sure));
                    alert_builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            deleteRows();
                            mode.finish();
                            coaches_adapter.notifyDataSetChanged();
                        }
                    });
                    alert_builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });
                    alert_builder.show();

                    return true;


                case R.id.coach_menu_select_all:
                    selectAll();
                    return true;

                case R.id.coach_menu_edit:
                    mode.finish();
                    coaches_adapter.updateData();
                    isLongselectionEnabled=false;
                    return true;

                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            coaches_adapter.clearSelections();
            actionMode = null;
            isLongselectionEnabled=false;
        }
    }
}
