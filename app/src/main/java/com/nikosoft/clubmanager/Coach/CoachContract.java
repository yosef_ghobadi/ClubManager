package com.nikosoft.clubmanager.Coach;

import com.nikosoft.clubmanager.Base.BasePresenter;
import com.nikosoft.clubmanager.Base.BaseView;
import com.nikosoft.clubmanager.Data.Model.Coach;

import java.util.List;

public interface CoachContract {

    interface View extends BaseView{
        void CoachList(List<Coach> coachList);
    }

    interface Presenter extends BasePresenter<View>{
        void getCoaches();
    }
}
