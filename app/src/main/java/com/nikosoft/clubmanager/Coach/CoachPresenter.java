package com.nikosoft.clubmanager.Coach;

import com.nikosoft.clubmanager.Data.DataSource;

import javax.inject.Inject;

public class CoachPresenter implements CoachContract.Presenter {

    CoachContract.View view;
    DataSource dataSource;

    @Inject
    public CoachPresenter(DataSource dataSource)
    {
        this.dataSource=dataSource;
    }

    @Override
    public void getCoaches() {
        view.CoachList(dataSource.getCoaches());
    }

    @Override
    public void attachView(CoachContract.View view) {
        this.view=view;
        getCoaches();
    }

    @Override
    public void detachView() {
        this.view=null;
    }
}
