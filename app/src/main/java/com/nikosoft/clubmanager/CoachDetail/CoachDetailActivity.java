package com.nikosoft.clubmanager.CoachDetail;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.Data.DBRepository.CoachRepository;
import com.nikosoft.clubmanager.Data.Model.Club;
import com.nikosoft.clubmanager.Data.Model.Coach;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.Utiles.Utility;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CoachDetailActivity extends AppCompatActivity implements CoachDetailContract.View {


    @BindView(R.id.coach_detail_collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.coach_detail_toolbar)
    Toolbar toolbar;
    /*@BindView(R.id.tab_coach_detail)
    TabLayout tabLayout;
    @BindView(R.id.viewpager_coach_detail)
    ViewPager viewPager;*/
    @BindView(R.id.coach_detail_profile_image)
    CircularImageView profile_img;
    @BindView(R.id.txt_coach_detail_name)
    TextView txt_name;
    @BindView(R.id.txt_coach_detail_joindate)
    TextView txt_joindate;

    @BindView(R.id.txt_coach_info_coach_phone)
    TextView txt_phone;
    @BindView(R.id.txt_info_coach_birthdate)
    TextView txt_birthdate;
    @BindView(R.id.txt_info_coach_club)
    TextView txt_club;
    @BindView(R.id.txt_info_coach_athlete)
    TextView txt_students;
    @BindView(R.id.txt_info_coach_field)
    TextView txt_field;
    @BindView(R.id.txt_info_coach_contract_type)
    TextView txt_contractType;
    @BindView(R.id.txt_coach_info_coach_amount)
    TextView txt_contractAmount;
    @BindView(R.id.txt_info_coach_coach_session)
    TextView txt_session;


    CoachDetailContract.Presenter presenter;

    private Coach coach;
    private int coachId =0;


    @Inject
    Utility utility;
    @Inject
    CoachRepository coachRepository;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //get coach for show his info
        getCoach();

        ClubManager.getApplicationComponent().inject(this);

        AppCompatDelegate.setDefaultNightMode(utility.getThemeNight());

        setContentView(R.layout.activity_coach_detail);


        //addDefaultValues();
        ButterKnife.bind(this);


        presenter.attachView(this);

        setupViews();
    }

    private void getCoach() {
        Bundle bundle = getIntent().getExtras();

        try {
            if (bundle != null) {
                coachId = bundle.getInt("coachId");

            } else {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Inject
    public void setPresenter(CoachDetailPresenter presenter) {
        presenter.coachId= coachId;
        this.presenter = presenter;
    }


    private void setupViews() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.title_activity_coach_details));

        collapsingToolbarLayout.setExpandedTitleTextColor(Objects.requireNonNull(ContextCompat.getColorStateList(getViewContext(), android.R.color.transparent)));


        //setupViewPager(viewPager);

        //tabLayout.setupWithViewPager(viewPager);

        txt_name.setText(coach.getName());
        txt_joindate.setText(getString(R.string.join_date)+" : "+utility.farsinumber(utility.getFormattedDate(coach.getDate(), 0)));

        if (utility.getImage(coach.getImage()) == null) {
            profile_img.setImageDrawable(getViewContext().getResources().getDrawable(R.drawable.ic_account_circle));
        } else
            profile_img.setImageBitmap(utility.getImage(coach.getImage()));



        txt_phone.setText(utility.farsinumber(coach.getPhone()));
        txt_birthdate.setText(utility.farsinumber(utility.getFormattedDate(coach.getBirthDate(), 0)));

        if (coach.getClubs().size() > 0) {
            for (Club item : coach.getClubs()) {
                txt_club.setText(txt_club.getText() + item.getName() + " ,");
            }
            txt_club.setText(txt_club.getText().toString().substring(0, txt_club.getText().length() - 1));
        }
        if (coach.getStudents() != null)
            txt_students.setText(utility.farsinumber(String.valueOf(coach.getStudents().size())));

        if (coach.getField() != null)
            txt_field.setText(coach.getField().getTitle());

        txt_contractType.setText(getResources().getStringArray(R.array.contract_types)[coach.getContractType()]);

        switch (coach.getContractType()) {
            case 0:
                txt_contractAmount.setText(utility.farsinumber(String.valueOf(coach.getSalary())));
                break;
            case 1:
                txt_contractAmount.setText(utility.farsinumber(utility.threeDigit(String.valueOf(coach.getSalary()), ',')));
                break;
            case 2:
                txt_contractAmount.setText(utility.farsinumber(utility.threeDigit(String.valueOf(coach.getSalary()), ',')));
                break;
        }

        if(coach.getSessions().size()>0)
            txt_session.setText(utility.farsinumber(String.valueOf(coach.getSessions().size())));
    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_student_detail,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case R.id.student_detail_call:
                Intent intent_call=new Intent(Intent.ACTION_DIAL);
                intent_call.setData(Uri.parse("tel:"+ coach.getPhone()));
                startActivity(intent_call);
                break;

            case R.id.student_detail_sms:
                Intent intent_sms=new Intent(Intent.ACTION_VIEW);
                intent_sms.setData(Uri.parse("sms:"+ coach.getPhone()));
                startActivity(intent_sms);
                break;
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }


    @Override
    public void coach(Coach coach) {
    this.coach=coach;
    }

    @Override
    public Context getViewContext() {
        return this;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
        coachRepository.close();
    }
}
