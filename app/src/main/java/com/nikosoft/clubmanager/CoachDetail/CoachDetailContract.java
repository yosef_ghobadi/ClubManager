package com.nikosoft.clubmanager.CoachDetail;

import com.nikosoft.clubmanager.Base.BasePresenter;
import com.nikosoft.clubmanager.Base.BaseView;
import com.nikosoft.clubmanager.Data.Model.Coach;

public interface CoachDetailContract {

    public interface View extends BaseView{
        void coach(Coach coach);
    }

    public interface Presenter extends BasePresenter<View>{
        void getCoach(int coachId);
    }
}
