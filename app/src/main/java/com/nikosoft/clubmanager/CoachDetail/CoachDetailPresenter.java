package com.nikosoft.clubmanager.CoachDetail;

import com.nikosoft.clubmanager.Data.DataSource;

import javax.inject.Inject;

public class CoachDetailPresenter implements CoachDetailContract.Presenter {

    CoachDetailContract.View view;
    DataSource dataSource;

    public int coachId=0;

    @Inject
    public CoachDetailPresenter(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void getCoach(int coachId) {
        view.coach(dataSource.getCoach(coachId));
    }

    @Override
    public void attachView(CoachDetailContract.View view) {
        this.view=view;
        getCoach(coachId);
    }

    @Override
    public void detachView() {
        this.view=null;
    }
}
