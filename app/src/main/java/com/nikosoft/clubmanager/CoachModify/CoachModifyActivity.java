package com.nikosoft.clubmanager.CoachModify;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;

import com.balysv.materialripple.MaterialRippleLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.ClubModify.ClubModifyActivity;
import com.nikosoft.clubmanager.Coach.CoachActivity;
import com.nikosoft.clubmanager.Data.DBRepository.CoachRepository;
import com.nikosoft.clubmanager.Data.DBRepository.FieldRepository;
import com.nikosoft.clubmanager.Data.Model.Club;
import com.nikosoft.clubmanager.Data.Model.Coach;
import com.nikosoft.clubmanager.Data.Model.Field;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.Utiles.PersianDateModel;
import com.nikosoft.clubmanager.Utiles.Utility;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.ganfra.materialspinner.MaterialSpinner;
import io.realm.RealmList;

public class CoachModifyActivity extends AppCompatActivity implements CoachModifyContract.View {


    @BindView(R.id.coach_toolbar)
    Toolbar toolbar;
    @BindView(R.id.coach_collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.coach_choose_image_fab)
    FloatingActionButton image_fab;
    @BindView(R.id.coach_profile_image)
    CircularImageView profile_image;
    @BindView(R.id.txt_coach_name)
    EditText txt_name;
    @BindView(R.id.txt_coach_phone)
    EditText txt_phone;
    @BindView(R.id.spinner_coach_contract_type)
    MaterialSpinner spinner_contract;
    @BindView(R.id.spinner_coach_contract_percent)
    MaterialSpinner spinner_percent;
    @BindView(R.id.spinner_coach_field)
    MaterialSpinner spinner_field;
    @BindView(R.id.txt_coach_amount)
    EditText txt_salary;
    @BindView(R.id.birth_date_year)
    AppCompatSpinner sp_birthDate_year;
    @BindView(R.id.birth_date_month)
    AppCompatSpinner sp_birthDate_month;
    @BindView(R.id.birth_date_day)
    AppCompatSpinner sp_birthDate_day;
    @BindView(R.id.join_date_year)
    AppCompatSpinner sp_joinDate_year;
    @BindView(R.id.join_date_month)
    AppCompatSpinner sp_joinDate_month;
    @BindView(R.id.join_date_day)
    AppCompatSpinner sp_joinDate_day;
    @BindView(R.id.coach_clubs)
    TextView txt_coach_club;
    /*@BindView(R.id.coach_fields)
    TextView txt_coach_fields;*/
    @BindView(R.id.lay_add_coach)
    MaterialRippleLayout add_coach;
    @BindView(R.id.coachModify_coordinator)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.lay_coach_fields)
    LinearLayout root_content;
    @BindView(R.id.input_coach_amount)
    TextInputLayout inputLayout_amount;
    @BindView(R.id.lay_salary)
    LinearLayout lay_amount;

    /*@BindView(R.id.lay_percentage)
    RelativeLayout lay_percentage;*/
    @BindView(R.id.lay_spinner_percent_value)
    LinearLayout lay_spinner_percent;
    @BindView(R.id.txt_birthday_caption)
    TextView birthday_caption;
    @BindView(R.id.txt_join_date_caption)
    TextView join_date_caption;
    @BindView(R.id.txt_clubs_caption)
    TextView clubs_caption;
//    @BindView(R.id.spin_coach_clubs)
//    MultiSelectSpinner spin_coach_clubs;

    private CoachModifyContract.Presenter presenter;
    private Uri mCropImageUri;

    List<Field> FieldList = new ArrayList<>();
    boolean[] checkedFields = new boolean[0];
    List<Club> ClubList = new ArrayList<>();
    boolean[] checkedClubs = new boolean[0];
    private String imageUrl = null;
    private int coachId = 0;

    private int SELECTED_CONTRACT_TYPE = -1;
    private int SELECTED_PERCENTAGE = -1;
    private int SELECTED_FIELD = -1;


    private ArrayAdapter<String> arr_percentage;

    private ArrayAdapter<String> arr_field;
    private String[] field_items;

    private ArrayAdapter<String> arr_birthdate_year;
    private ArrayAdapter<String> arr_birthdate_month;
    private ArrayAdapter<String> arr_birthdate_day;

    private ArrayAdapter<String> arr_joinDate_year;
    private ArrayAdapter<String> arr_joinDate_month;
    private ArrayAdapter<String> arr_joinDate_day;

    //for detecting edit coach or add new
    private boolean EDIT_MODE = false;
    private Coach coach = null;

    private List<String> birthYearsList = new ArrayList<>();
    private List<String> monthsList = new ArrayList<>();
    private List<String> daysList = new ArrayList<>();
    private List<String> joinYearsList = new ArrayList<>();
    private List<String> percentagesList = new ArrayList<>();

    @Inject
    Utility utility;
    @Inject
    CoachRepository coachRepository;
    @Inject
    FieldRepository fieldRepository;
    private List<Field> fieldList;
    private List<String> Selected_clubs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //dependenvy injection by dagger2
        ClubManager.getApplicationComponent().inject(this);
        //set day night theme auto
        AppCompatDelegate.setDefaultNightMode(utility.getThemeNight());

        setContentView(R.layout.activity_coach_modify);

        ButterKnife.bind(this);


        presenter.attachView(this);

        //get the coach id for edit
        Bundle bundle = getIntent().getExtras();

        try {
            if (bundle != null) {
                coachId = bundle.getInt("coachId");
                EDIT_MODE = true;
                coach = coachRepository.get(coachId);

            } else {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        //load soinners adapter
        new getAdaptersData().execute(1);
        new getAdaptersData().execute(2);
        new getAdaptersData().execute(3);
        new getAdaptersData().execute(4);
        new getAdaptersData().execute(5);

        //set drawable to edittexts and textviews
        setViewsDrawable();

        setupViews();


    }


    @Inject
    public void setPresenter(CoachModifyPresenter presenter) {
        this.presenter = presenter;
    }




    private void setupViews() {
        //Toolbar
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.register_coach));

        collapsingToolbarLayout.setExpandedTitleTextColor(Objects.requireNonNull(ContextCompat.getColorStateList(getViewContext(), android.R.color.transparent)));

        //choose image from gallery or camera
        image_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSelectImageClick(view);
            }
        });


        if (EDIT_MODE) {
            //set image
            if (coach.getImage() != null) {
                imageUrl = coach.getImage();
                File file=new File(imageUrl);
                Picasso.get().load(file).into(profile_image);
            }

            //set name and family
            txt_name.setText(coach.getName());

            //set phone
            txt_phone.setText(coach.getPhone());
        }

        //set contract type
        spinner_contract.setAdapter(ArrayAdapter.createFromResource(getViewContext(), R.array.contract_types, android.R.layout.simple_spinner_dropdown_item));

        spinner_contract.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

                //get selected contract type
                SELECTED_CONTRACT_TYPE = position;


                //percentage fields
                if (position == -1) {
                    lay_spinner_percent.setVisibility(View.GONE);
                    lay_amount.setVisibility(View.GONE);


                } else if (position == 0) {
                    lay_spinner_percent.setVisibility(View.VISIBLE);
                    lay_amount.setVisibility(View.GONE);


                } else if (position == 1) {
                    inputLayout_amount.setHint(getViewContext().getString(R.string.salary_amount));
                    lay_spinner_percent.setVisibility(View.GONE);
                    lay_amount.setVisibility(View.VISIBLE);

                } else if (position == 2) {
                    inputLayout_amount.setHint(getViewContext().getString(R.string.rent_amount));
                    lay_spinner_percent.setVisibility(View.GONE);
                    lay_amount.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        if (EDIT_MODE) {
            spinner_contract.setSelection(coach.getContractType() + 1);
        }

        //contract percentage spinner

        spinner_percent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                //get selected item id
                SELECTED_PERCENTAGE = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //sest spinners floating label direction
        if (!utility.deviceLanguageDirection()) {
            spinner_contract.setRtl();
            spinner_percent.setRtl();
            spinner_field.setRtl();
        }

        //Separate digits and convert to persian digits
        txt_salary.addTextChangedListener(new TextWatcher() {
            String beforeText = "";

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                beforeText = txt_salary.getText().toString();
            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                try {
                    //if value is not empty
                    if (txt_salary.getText().length() > 0) {

                        if (Long.parseLong(utility.farsinumber(utility.removeSpaces(txt_salary.getText().toString()))) != 0) {
                            utility.handleNumberInput(txt_salary, this);
                        } else
                            txt_salary.setText("");
                    }
                } catch (Exception ex) {

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        if (EDIT_MODE) {
            //set salary amount
            if (coach.getContractType() != 0) {

                txt_salary.setText(coach.getSalary() + "");
                lay_amount.setVisibility(View.VISIBLE);
                lay_spinner_percent.setVisibility(View.GONE);
            }
        }
/*List<String> club_titles=new ArrayList<>();
        for (Club club:ClubList) {
            club_titles.add(club.getName());
        }
        spin_coach_clubs.setItems(club_titles);
        spin_coach_clubs.hasNoneOption(false);
        spin_coach_clubs.setListener(new MultiSelectSpinner.OnMultipleItemsSelectedListener() {
            @Override
            public void selectedIndices(List<Integer> indices) {

            }

            @Override
            public void selectedStrings(List<String> strings) {
                Selected_clubs=strings;
            }
        });*/

        //The clubs that the coach operates





        txt_coach_club.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getViewContext(), ClubModifyActivity.class);
                AlertDialog d = showClubMultiChoiceDialog(getViewContext(),
                        getString(R.string.select_clubs), ClubList, txt_coach_club, checkedClubs, intent).create();
                d.show();


            }
        });

        //set field spinner
        arr_field = new ArrayAdapter<>(getViewContext(), android.R.layout.simple_list_item_1);
        spinner_field.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SELECTED_FIELD=i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //set the coach clubs
        if (EDIT_MODE) {

            if (coach.getClubs().size() > 0) {
                txt_coach_club.setText("");
                for (Club item : coach.getClubs()) {
                    txt_coach_club.setText(txt_coach_club.getText() + item.getName() + ",");
                    for (int i = 0; i < ClubList.size(); i++) {
                        if (item.getId() == ClubList.get(i).getId()) {
                            checkedClubs[i] = true;
                        }
                    }
                }
                //برای حذف ویرگول اخر
                txt_coach_club.setText(txt_coach_club.getText().subSequence(0, txt_coach_club.getText().length() - 1));

                setFieldsSpinnerItems();
            }
        }




        //Save coach
        add_coach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //empty name
                if (utility.isEmpty(txt_name)) {
                    utility.setError(txt_name, getString(R.string.emtpy_value_error));
                    return;
                }
                //exsit name
                else {
                    if (EDIT_MODE == false) {
                        for (Coach item : presenter.getCoachList()) {
                            if (item.getName().equals(txt_name.getText().toString())) {
                                utility.setError(txt_name, getString(R.string.exist_value_error));
                                return;
                            }
                        }
                    }
                }
                //empty phone
                if (utility.isEmpty(txt_phone)) {
                    utility.setError(txt_phone, getString(R.string.emtpy_value_error));
                    return;
                }
                //check digit count
                else if (txt_phone.getText().length() < 11) {
                    utility.setError(txt_phone, getString(R.string.phone_digit_length_error));
                    return;
                }
                //exsit phone
                else {
                    if (EDIT_MODE == false) {
                        for (Coach item : presenter.getCoachList()) {
                            if (item.getPhone().equals(txt_phone.getText().toString())) {
                                utility.setError(txt_phone, getString(R.string.exist_value_error));
                                return;
                            }
                        }
                    }
                }
                //empty contract type

                if (SELECTED_CONTRACT_TYPE == -1) {
                    utility.setError(spinner_contract, getString(R.string.unselected_value_error));
                    return;
                }
                //contract type equals Percentage and percentage value empty
                else if (SELECTED_CONTRACT_TYPE == 0 & SELECTED_PERCENTAGE == -1) {

                    lay_spinner_percent.setVisibility(View.VISIBLE);
                    lay_amount.setVisibility(View.GONE);
                    utility.setError(spinner_percent, getString(R.string.unselected_value_error));
                    return;
                }
                //contract type not equals Percentage and empty salary
                else if (SELECTED_CONTRACT_TYPE > 0 & utility.isEmpty(txt_salary)) {

                    lay_spinner_percent.setVisibility(View.GONE);
                    lay_amount.setVisibility(View.VISIBLE);
                    utility.setError(txt_salary, getString(R.string.emtpy_value_error));
                    return;
                }
                else if(SELECTED_FIELD==-1)
                {
                    utility.setError(spinner_field, getString(R.string.unselected_value_error));
                    return;
                }
                //save coach
                else {

                    //salary value or percentage value
                    long salaryOrPercentage = 0;

                    if (SELECTED_CONTRACT_TYPE == 0)
                        salaryOrPercentage = Long.parseLong(spinner_percent.getSelectedItem().toString());
                    else if (SELECTED_CONTRACT_TYPE > 0)
                        salaryOrPercentage = Long.parseLong(utility.removeSpaces(txt_salary.getText().toString()));

                    coachRepository.save(new Coach(
                            coachId,
                            txt_name.getText().toString(),
                            Long.parseLong(sp_birthDate_year.getSelectedItem().toString() + sp_birthDate_month.getSelectedItem().toString() + sp_birthDate_day.getSelectedItem().toString()),
                            imageUrl,
                            Long.parseLong(sp_joinDate_year.getSelectedItem().toString() + sp_joinDate_month.getSelectedItem().toString() + sp_joinDate_day.getSelectedItem().toString()),
                            SELECTED_CONTRACT_TYPE,
                            salaryOrPercentage,
                            utility.removeSpaces(txt_phone.getText().toString()),
                            getSelectedClubs(Selected_clubs,ClubList),
                            fieldList.get(SELECTED_FIELD),
                            true
                    ));

                    Intent intent = new Intent(getViewContext(), CoachActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    /**
     * set edittext and textview drawable
     */
    private void setViewsDrawable() {
        utility.setViewDrawable(txt_name, getViewContext(), R.drawable.ic_account_box, 4, 10);
        utility.setViewDrawable(txt_phone, getViewContext(), R.drawable.ic_phone, 4, 10);
        utility.setViewDrawable(txt_salary, getViewContext(), R.drawable.ic_contract_price, 4, 10);
        utility.setViewDrawable(birthday_caption, getViewContext(), R.drawable.ic_birthday, 4, 10);
        utility.setViewDrawable(join_date_caption, getViewContext(), R.drawable.ic_join, 4, 10);
        utility.setViewDrawable(clubs_caption, getViewContext(), R.drawable.ic_gym, 4, 10);
        //utility.setViewDrawable(fields_caption, getViewContext(), R.drawable.ic_fields, 4, 10);
    }


    /**
     * Start pick image activity with chooser.
     *
     * @param view
     */
    public void onSelectImageClick(View view) {
        CropImage.startPickImageActivity(this);
    }

    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), result.getUri());
                    if(EDIT_MODE)
                        utility.deleteFile(imageUrl);
                    imageUrl = utility.saveImage(bitmap, getViewContext());
                    profile_image.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }


                //Toast.makeText(this, "Cropping successful, Sample: " + result.getSampleSize(), Toast.LENGTH_LONG).show();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                //Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // required permissions granted, start crop image activity
            startCropImageActivity(mCropImageUri);
        } else {
            Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .start(this);
    }


    /**
     * show dialog for choose coaches
     *
     * @param context
     * @param title        dialog title
     * @param itemsName    clubs names
     * @param textView     textview that shows clubs name after selection
     * @param checkedItems save checked clubs
     * @param intent       start clubs activity for insert
     * @return
     */
    private AlertDialog.Builder showClubMultiChoiceDialog(Context context, String title,
                                                          List<Club> itemsName,
                                                          TextView textView,
                                                          boolean[] checkedItems,
                                                          Intent intent) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setCancelable(false);

        //ارایه ای برای نمایش نام ایتم ها
        int itemsSize = 0;

        if (itemsName != null)
            itemsSize = itemsName.size();

        String[] filds = new String[itemsSize];
        boolean[] checkedItems_temp =new boolean[itemsSize];

        if (itemsName != null) {
            for (int i = 0; i < itemsSize; i++) {
                filds[i] = itemsName.get(i).getName();
                checkedItems_temp[i] = checkedItems[i];
            }
        }

        builder.setMultiChoiceItems(filds, checkedItems_temp, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i, boolean b) {
                checkedItems_temp[i] = b;
            }
        });

        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                textView.setText("");

                //مشخص کردن گزینه های انتخاب شده از روی ارایه check
                for (int j = 0; j < checkedItems_temp.length; j++) {
                    checkedItems[j] = checkedItems_temp[j];
                    if (checkedItems_temp[j]) {

                        textView.setText(textView.getText() + itemsName.get(j).getName() + " ,");
                    }
                }

                //برای حذف وبرگول اضافی آخر
                if (textView.getText().length() == 0)
                    textView.setText(R.string.choose);
                else
                    textView.setText(textView.getText().subSequence(0, textView.getText().length() - 1));

                setFieldsSpinnerItems();
            }
        });
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                for (boolean item:checkedItems) {
                    item=false;
                }
            }
        });
        builder.setNeutralButton(R.string.add, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivity(intent);
            }
        });
        builder.setCancelable(true);
        return builder;
    }

    /**
     * set field spinner item from selecteds clubs fields
     */
    private void setFieldsSpinnerItems() {
         fieldList = new ArrayList<>();
        int i = 0;
        for (Club item : ClubList) {
            if (checkedClubs[i]) {
                for(int j=0;j<item.getFields().size();j++) {
                    if (!fieldList.contains(item.getFields().get(j)))
                        fieldList.add(item.getFields().get(j));
                }
            }
            i++;
        }
        i=0;
        field_items=new String[fieldList.size()];
        for (Field item:fieldList) {
            field_items[i]=item.getTitle();
            i++;
        }
        arr_field=new ArrayAdapter<>(getViewContext(),android.R.layout.simple_list_item_1,field_items);
        spinner_field.setAdapter(arr_field);
        //set coachs field
        if (EDIT_MODE) {
            if(coach.getField()!=null) {
                String title = coach.getField().getTitle();
                int pos = arr_field.getPosition(title);
                spinner_field.setSelection(pos + 1);
            }
        }
    }




    /**
     * back arrow action
     *
     * @return
     */
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public Context getViewContext() {
        return this;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
        coachRepository.close();
        fieldRepository.close();
    }


    @Override
    public void ClubList(List<Club> clubs) {
        ClubList.addAll(clubs);
        checkedClubs = new boolean[clubs.size()];
    }

    @Override
    public void FieldList(List<Field> fields) {
        FieldList = fields;
        checkedFields = new boolean[fields.size()];
    }

    /**
     * get selected clubs by user
     * @param checkedItems
     * @param clubs
     * @return
     */
    private RealmList<Club> getSelectedClubs(boolean[] checkedItems, List<Club> clubs) {
        RealmList<Club> selectedClubs = new RealmList<>();

        for (int i = 0; i < checkedItems.length; i++) {
            if (checkedItems[i])
                selectedClubs.add(clubs.get(i));
        }

        return selectedClubs;
    }
    /**
     * get selected clubs by user
     * @param selected_titles
     * @param clubs
     * @return
     */
    private RealmList<Club> getSelectedClubs(List<String> selected_titles, List<Club> clubs) {
        RealmList<Club> selectedClubs = new RealmList<>();

        for (int i = 0; i < selected_titles.size(); i++) {
            for (Club club:clubs) {
                if (selected_titles.get(i).equals(club.getName()))
                    selectedClubs.add(club);
            }

        }

        return selectedClubs;
    }



    /**
     * laod all adapter in background thread
     */
    private class getAdaptersData extends AsyncTask<Integer, Void, Integer> {


        @Override
        protected Integer doInBackground(Integer... integers) {
            switch (integers[0]) {
                case 0:


                    break;
                case 1:
                    daysList = utility.getDays();
                    break;
                case 2:
                    monthsList = utility.getMonths();
                    break;
                case 3:
                    joinYearsList = utility.getJoinYears();
                    break;
                case 4:
                    birthYearsList = utility.getBirthYears();
                    break;
                case 5:
                    percentagesList = utility.getPercentages();
                    break;
            }

            return integers[0];
        }


        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);

            PersianDateModel birthDate = new PersianDateModel();
            PersianDateModel joinDate = new PersianDateModel();
            //set birth date
            if (EDIT_MODE) {
                birthDate = utility.getPersianDateModel(coach.getBirthDate());
                joinDate = utility.getPersianDateModel(coach.getDate());
            }

            switch (integer) {
                case 0:

                case 1:
                    arr_birthdate_day = new ArrayAdapter<String>(getViewContext(), android.R.layout.simple_spinner_dropdown_item, daysList);
                    sp_birthDate_day.setAdapter(arr_birthdate_day);


                    arr_joinDate_day = new ArrayAdapter<String>(getViewContext(), android.R.layout.simple_spinner_dropdown_item, daysList);
                    sp_joinDate_day.setAdapter(arr_joinDate_day);

                    //setting for values for edit if exist
                    if (EDIT_MODE) {
                        sp_birthDate_day.setSelection(arr_birthdate_day.getPosition(birthDate.getDay()));
                        sp_joinDate_day.setSelection(arr_joinDate_day.getPosition(joinDate.getDay()));
                    }

                    break;
                case 2:

                    arr_birthdate_month = new ArrayAdapter<String>(getViewContext(), android.R.layout.simple_spinner_dropdown_item, monthsList);
                    sp_birthDate_month.setAdapter(arr_birthdate_month);

                    arr_joinDate_month = new ArrayAdapter<String>(getViewContext(), android.R.layout.simple_spinner_dropdown_item, monthsList);
                    sp_joinDate_month.setAdapter(arr_joinDate_month);

                    if (EDIT_MODE) {
                        sp_birthDate_month.setSelection(arr_birthdate_month.getPosition(birthDate.getMonth()));
                        sp_joinDate_month.setSelection(arr_joinDate_month.getPosition(joinDate.getMonth()));
                    }

                    break;
                case 3:

                    arr_joinDate_year = new ArrayAdapter<String>(getViewContext(), android.R.layout.simple_spinner_dropdown_item, joinYearsList);
                    sp_joinDate_year.setAdapter(arr_joinDate_year);

                    if (EDIT_MODE) {
                        sp_joinDate_year.setSelection(arr_joinDate_year.getPosition(joinDate.getYear()));
                    }

                    break;
                case 4:

                    arr_birthdate_year = new ArrayAdapter<String>(getViewContext(), android.R.layout.simple_spinner_dropdown_item, birthYearsList);
                    sp_birthDate_year.setAdapter(arr_birthdate_year);

                    if (EDIT_MODE)
                        sp_birthDate_year.setSelection(arr_birthdate_year.getPosition(birthDate.getYear()));
                    else
                        sp_birthDate_year.setSelection(sp_birthDate_year.getCount() / 2);

                    break;
                case 5:
                    //setting spinner percentage adapter
                    arr_percentage = new ArrayAdapter<>(getViewContext(), android.R.layout.simple_spinner_dropdown_item, percentagesList);
                    spinner_percent.setAdapter(arr_percentage);


                    if (EDIT_MODE)
                        if (coach.getContractType() == 0) {
                            if (utility.deviceLanguage().equals(utility.Farsi))
                                spinner_percent.setSelection(arr_percentage.getPosition(utility.farsinumber(coach.getSalary() + "")) + 1);
                            else
                                spinner_percent.setSelection(arr_percentage.getPosition(coach.getSalary() + "") + 1);
                            lay_amount.setVisibility(View.GONE);
                            lay_spinner_percent.setVisibility(View.VISIBLE);
                        }

                    break;
            }
        }
    }
}
