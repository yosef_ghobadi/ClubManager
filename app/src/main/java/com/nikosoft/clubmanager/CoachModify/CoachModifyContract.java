package com.nikosoft.clubmanager.CoachModify;

import com.nikosoft.clubmanager.Base.BasePresenter;
import com.nikosoft.clubmanager.Base.BaseView;
import com.nikosoft.clubmanager.Data.Model.Club;
import com.nikosoft.clubmanager.Data.Model.Coach;
import com.nikosoft.clubmanager.Data.Model.Field;

import java.util.List;

public interface CoachModifyContract {

    interface View extends BaseView{
        void ClubList(List<Club> clubs);
        void FieldList(List<Field> fields);
    }


    interface Presenter extends BasePresenter<View>{
        List<Coach> getCoachList();

        void getClubs();
        void getFields();
    }
}
