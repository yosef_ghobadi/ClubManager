package com.nikosoft.clubmanager.CoachModify;

import com.nikosoft.clubmanager.Data.DBRepository.CoachRepository;
import com.nikosoft.clubmanager.Data.DataSource;
import com.nikosoft.clubmanager.Data.Model.Coach;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class CoachModifyPresenter implements CoachModifyContract.Presenter {

    private CoachModifyContract.View view;
    private DataSource dataSource;

    private List<Coach> coachList;
    @Override
    public List<Coach> getCoachList() {
        getCoaches();
        return coachList;
    }

    public void setCoachList(List<Coach> coachList) {
        this.coachList = coachList;
    }


    @Inject
    CoachRepository repository;

    @Inject
    public CoachModifyPresenter(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void attachView(CoachModifyContract.View view) {
        this.view = view;
        getClubs();
        getFields();
    }

    @Override
    public void detachView() {
        this.view = null;

    }

    @Override
    public void getClubs() {
        view.ClubList(dataSource.getClubs());
    }

    @Override
    public void getFields() {
        view.FieldList(dataSource.getFields());
    }

    /**
     * get coahes list async
     */
    private void getCoaches() {
        Observable<List<Coach>> observable = Observable.just(repository.getAllAsync());
        observable.subscribe(new Observer<List<Coach>>() {



            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<Coach> coaches) {
                setCoachList(coaches);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }
}
