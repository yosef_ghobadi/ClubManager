package com.nikosoft.clubmanager.Data;

import com.nikosoft.clubmanager.Models.Banner;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;


/**
 * Created by Yosef on 09/02/2019.
 */

public interface ApiService {

    @GET("banner/getbanners")
    Single<List<Banner>> getSliderImages();
}
