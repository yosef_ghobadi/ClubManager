package com.nikosoft.clubmanager.Data.DBRepository;

import com.nikosoft.clubmanager.Data.Model.Club;

import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;

/**
 * Created by Yosef on 07/02/2019.
 */

public class ClubRepository {

    public Realm realm;

    @Inject
    public ClubRepository() {
        realm = Realm.getDefaultInstance();

    }


    public void save(Club club) {

        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    //    ایجاد مقدار فیلد ای دی به صورت افزایشی
                    if (club.getId() == 0) {
                        int id;
                        Number currentId = realm.where(Club.class).max("id");
                        if (currentId == null)
                            id = 1;
                        else
                            id = currentId.intValue() + 1;
                        club.setId(id);
                    }

                    /*Club db_club = realm.createObject(Club.class, club.getId());
                    db_club.setName(club.getName());
                    db_club.setMonthlyCost(club.getMonthlyCost());
                    db_club.settuition(club.gettuition());
                    db_club.setTuitionDay(club.getTuitionDay());
                    db_club.setStartTime(club.getStartTime());
                    db_club.setEndTime(club.getEndTime());
                    db_club.setFields(club.getFields());
                    db_club.setDescription(club.getDescription());*/

                    realm.copyToRealmOrUpdate(club);

                }

            });
        } finally {
        }
    }

    public List<Club> getAll() {
        List<Club> clubs = realm.where(Club.class).findAll();
        return clubs;
    }

    public List<Club> getAllAsync() {
        List<Club> clubs = realm.where(Club.class).findAllAsync();
        return clubs;
    }

    public Club getClub(int id) {
        Club club = realm.where(Club.class).equalTo("id", id).findFirst();
        return club;
    }

    public void delete(int id) {
        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {

                    realm.where(Club.class).equalTo("id", id).findFirst().deleteFromRealm();
                }
            });
        } finally {
        }

    }




    public void close() {
        realm.close();
    }


}
