package com.nikosoft.clubmanager.Data.DBRepository;

import android.util.Log;

import com.nikosoft.clubmanager.Data.Model.Coach;

import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;

/**
 * Created by Yosef on 16/02/2019.
 */

public class CoachRepository {

    public Realm realm;

    @Inject
    public CoachRepository() {

        this.realm = Realm.getDefaultInstance();
    }

    public void save(Coach coach) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                //    ایجاد مقدار فیلد ای دی به صورت افزایشی
                if(coach.getId()==0) {
                    int id;
                    Number currentId = realm.where(Coach.class).max("id");
                    if (currentId == null)
                        id = 1;
                    else
                        id = currentId.intValue() + 1;
                    coach.setId(id);
                }
               /* Field db_field = realm.createObject(Field.class, id);
                db_field.setTitle(field.getTitle());
                db_field.setDescription(field.getDescription());
                db_field.setStudents(field.getStudents());
                db_field.setCoaches(field.getTuitions());
                db_field.setTimes(field.getTimes());*/
                realm.copyToRealmOrUpdate(coach);


            }
        });
    }

    public List<Coach> getAll(){
        return realm.where(Coach.class).findAll();
    }
    public List<Coach> getAllAsync(){
        return realm.where(Coach.class).findAllAsync();
    }


    public Coach get(int id)
    {
        return realm.where(Coach.class).equalTo("id",id).findFirst();
    }

    public void delete(int id)
    {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                realm.where(Coach.class).equalTo("id",id).findFirst().deleteFromRealm();

            }
        });
    }



    public void close()
    {
        realm.close();
    }

    public void deleteAll() {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(Coach.class).findAll().deleteAllFromRealm();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.i("DataBase_DeleteAllCoach", error.getMessage()+"");
            }
        });
    }
}
