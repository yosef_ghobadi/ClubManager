package com.nikosoft.clubmanager.Data.DBRepository;

import android.util.Log;

import com.nikosoft.clubmanager.Data.Model.Field;

import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;

/**
 * Created by Yosef on 16/02/2019.
 */

public class FieldRepository {
    private Realm realm;

    @Inject
    public FieldRepository()
    {
        realm=Realm.getDefaultInstance();
    }

    public void save(Field field) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                //    ایجاد مقدار فیلد ای دی به صورت افزایشی
                if (field.getId() == 0) {
                    int id;
                    Number currentId = realm.where(Field.class).max("id");
                    if (currentId == null)
                        id = 1;
                    else
                        id = currentId.intValue() + 1;
                    field.setId(id);
                }

                /*Field db_field = realm.createObject(Field.class, id);
                db_field.setTitle(field.getTitle());*/

                realm.copyToRealmOrUpdate(field);


            }
        });
    }

    public List<Field> getAll(){
        return realm.where(Field.class).sort("title").findAll();
    }


    public Field get(int id)
    {
        return realm.where(Field.class).equalTo("id",id).findFirst();
    }

    public Field get(String title)
    {
        return realm.where(Field.class).equalTo("title",title).findFirst();
    }

    public void delete(int id)
    {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(Field.class).equalTo("id",id).findFirst().deleteFromRealm();
            }
        });
    }


    public void update(Field field)
    {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(field);
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.i("DataBase_UpdateField", error.getMessage()+"");
            }
        });
    }

    public void close()
    {
        realm.close();
    }

    public void deleteAll() {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(Field.class).findAll().deleteAllFromRealm();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.i("DataBase_DeleteAllField", error.getMessage()+"");
            }
        });
    }
}
