package com.nikosoft.clubmanager.Data.DBRepository;

import com.nikosoft.clubmanager.Data.Model.InsurancePayment;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.Sort;

public class InsurancePaymentRepository {

    private Realm realm;
    private int insurancePaymentId;

    @Inject
    public InsurancePaymentRepository() {
        realm = Realm.getDefaultInstance();
    }

    public int save(InsurancePayment insurancePayment) {

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                //    ایجاد مقدار فیلد ای دی به صورت افزایشی

                if (insurancePayment.getId() == 0) {

                    Number currentId = realm.where(InsurancePayment.class).max("id");
                    if (currentId == null)
                        insurancePaymentId = 1;
                    else
                        insurancePaymentId = currentId.intValue() + 1;
                    insurancePayment.setId(insurancePaymentId);
                }


                realm.copyToRealmOrUpdate(insurancePayment);

            }

        });
        return insurancePaymentId;
    }

    public List<InsurancePayment> getAll() {
        return realm.where(InsurancePayment.class).findAll();
    }


    public InsurancePayment get(int id) {
        return realm.where(InsurancePayment.class).equalTo("id", id).findFirst();
    }

    public List<InsurancePayment> getInsurancePayment(int studentId) {
        return realm.where(InsurancePayment.class).equalTo("student.id", studentId).sort("date", Sort.DESCENDING).findAll();
    }

    public InsurancePayment getLastInsurance(int studentId)
    {
        return realm.where(InsurancePayment.class).equalTo("student.id",studentId).sort("date",Sort.DESCENDING).findFirst();
    }

    public List<InsurancePayment> getInsurancePayment(String studentPhone,String insuranceTitle) {
        return realm.where(InsurancePayment.class)
                .equalTo("student.phone", studentPhone)
                .and()
                .equalTo("insurance.title",insuranceTitle)
                .findAll();
    }


    public void delete(int id) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Objects.requireNonNull(realm.where(InsurancePayment.class).equalTo("id", id).findFirst()).deleteFromRealm();
            }
        });
    }


    public void update(InsurancePayment insurancePayment) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(insurancePayment);
            }

        });
    }

    public void close() {
        realm.close();
    }

    public void deleteAll() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(InsurancePayment.class).findAll().deleteAllFromRealm();
            }

        });
    }
}
