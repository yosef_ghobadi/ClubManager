package com.nikosoft.clubmanager.Data.DBRepository;

import com.nikosoft.clubmanager.Data.Model.Insurance;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import io.realm.Realm;

public class InsuranceRepository {

    private Realm realm;
    private int insuranceId;

    @Inject
    public InsuranceRepository()
    {
        realm=Realm.getDefaultInstance();
    }

    public int save(Insurance insurance) {

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                //    ایجاد مقدار فیلد ای دی به صورت افزایشی

                if(insurance.getId()==0) {

                    Number currentId = realm.where(Insurance.class).max("id");
                    if (currentId == null)
                        insuranceId = 1;
                    else
                        insuranceId = currentId.intValue() + 1;
                    insurance.setId(insuranceId);
                }


                realm.copyToRealmOrUpdate(insurance);

            }

        });
        return insuranceId;
    }

    public List<Insurance> getAll(){
        return realm.where(Insurance.class).findAll();
    }


    public Insurance get(int id)
    {
        return realm.where(Insurance.class).equalTo("id",id).findFirst();
    }

    public Insurance getInsurance(int insurance)
    {
        return (Insurance) realm.where(Insurance.class).equalTo("cost",insurance).findFirst();
    }

    public void delete(int id)
    {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Objects.requireNonNull(realm.where(Insurance.class).equalTo("id", id).findFirst()).deleteFromRealm();
            }
        });
    }


    public void update(Insurance insurance)
    {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(insurance);
            }

        });
    }

    public void close()
    {
        realm.close();
    }

    public void deleteAll() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(Insurance.class).findAll().deleteAllFromRealm();
            }

        });
    }
}
