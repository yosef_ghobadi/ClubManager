package com.nikosoft.clubmanager.Data.DBRepository;

import com.nikosoft.clubmanager.Data.Model.SMS;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.Sort;

public class SMSRepository {

    private Realm realm;
    int smsId=0;
    @Inject
    public SMSRepository()
    {
        realm=Realm.getDefaultInstance();
    }

    /**
     * get new id for set to new record
     * @return
     */
    public int getId()
    {
        //    ایجاد مقدار فیلد ای دی به صورت افزایشی

        Number currentId = realm.where(SMS.class).max("id");
        if (currentId == null)
            return 1;
        else

            return currentId.intValue() + 1;
    }


    public void save(SMS sms) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                SMS db_sms = realm.createObject(SMS.class, sms.getId());
                db_sms.setMessage(sms.getMessage());
                db_sms.setDate(sms.getDate());
                db_sms.setSent(sms.getSent());
                db_sms.setStudentId(sms.getStudentId());

            }

        });

    }
    public void saveAsync(SMS sms) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                SMS db_sms = realm.createObject(SMS.class, sms.getId());
                db_sms.setMessage(sms.getMessage());
                db_sms.setDate(sms.getDate());
                db_sms.setSent(sms.getSent());
                db_sms.setStudentId(sms.getStudentId());

            }

        });

    }

    public List<SMS> getAll(){
        return realm.where(SMS.class).sort("id", Sort.DESCENDING).findAll();
    }


    public SMS get(int id)
    {
        return realm.where(SMS.class).equalTo("id",id).findFirst();
    }



    public void delete(int id)
    {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Objects.requireNonNull(realm.where(SMS.class).equalTo("id", id).findFirst()).deleteFromRealm();
            }
        });
    }


    public void update(SMS sms)
    {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(sms);
            }

        });
    }

    public void close()
    {
        realm.close();
    }

    public void deleteAll() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(SMS.class).findAll().deleteAllFromRealm();
            }

        });
    }
}
