package com.nikosoft.clubmanager.Data.DBRepository;

import com.nikosoft.clubmanager.Data.Model.SalaryPayment;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.Sort;

public class SalaryPaymentRepository {

    private Realm realm;
    private int salaryPaymentId;

    @Inject
    public SalaryPaymentRepository()
    {
        realm=Realm.getDefaultInstance();
    }

    public int save(SalaryPayment salaryPayment) {

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                //    ایجاد مقدار فیلد ای دی به صورت افزایشی

                if(salaryPayment.getId()==0) {

                    Number currentId = realm.where(SalaryPayment.class).max("id");
                    if (currentId == null)
                        salaryPaymentId = 1;
                    else
                        salaryPaymentId = currentId.intValue() + 1;
                    salaryPayment.setId(salaryPaymentId);
                }

                /*Tuition db_tuition = realm.createObject(Tuition.class, salaryPaymentId);
                db_tuition.setTuitionCost(tuition.getTuitionCost());*/
                realm.copyToRealmOrUpdate(salaryPayment);

            }

        });
        return salaryPaymentId;
    }

    public List<SalaryPayment> getAll(){
        return realm.where(SalaryPayment.class).findAll();
    }


    public SalaryPayment get(int id)
    {
        return realm.where(SalaryPayment.class).equalTo("id",id).findFirst();
    }

    public List<SalaryPayment> getSalaryPayment(int coachId)
    {

        return realm.where(SalaryPayment.class).equalTo("coach.id",coachId).sort("date", Sort.DESCENDING).findAll();
    }





    public void delete(int id)
    {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Objects.requireNonNull(realm.where(SalaryPayment.class).equalTo("id", id).findFirst()).deleteFromRealm();
            }
        });
    }


    public void update(SalaryPayment salaryPayment)
    {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(salaryPayment);
            }

        });
    }

    public void close()
    {
        realm.close();
    }

    public void deleteAll() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(SalaryPayment.class).findAll().deleteAllFromRealm();
            }

        });
    }
}
