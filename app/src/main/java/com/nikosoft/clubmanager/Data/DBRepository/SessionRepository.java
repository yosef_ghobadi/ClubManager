package com.nikosoft.clubmanager.Data.DBRepository;

import android.util.Log;

import com.nikosoft.clubmanager.Data.Model.Club;
import com.nikosoft.clubmanager.Data.Model.Session;

import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class SessionRepository {

    private Realm realm;

    @Inject
    public SessionRepository()
    {
        realm=Realm.getDefaultInstance();
    }

    public void save(Session session) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                //    ایجاد مقدار فیلد ای دی به صورت افزایشی
                if (session.getId() == 0) {
                    int id;
                    Number currentId = realm.where(Session.class).max("id");
                    if (currentId == null)
                        id = 1;
                    else
                        id = currentId.intValue() + 1;
                    session.setId(id);
                }

                realm.copyToRealmOrUpdate(session);


            }
        });
    }

    public List<Session> getAll(){

                RealmResults<Session> res= realm.where(Session.class).findAll();
                res=res.sort("id", Sort.DESCENDING);
        return res;
    }

        public List<Session> getAllAsync(){
        return realm.where(Session.class).findAllAsync();
    }


    public Session get(int id)
    {
        return realm.where(Session.class).equalTo("id",id).findFirst();
    }
    public Session getAsync(int id)
    {
        return realm.where(Session.class).equalTo("id",id).findFirstAsync();
    }

    public Session get(String name)
    {
        return realm.where(Session.class).equalTo("name",name).findFirst();
    }

    /**
     * get session depends on club,start and end time and days
     * @param club
     * @param startTime
     * @param endTime
     * @param daysOfWeek
     * @return returns false if no result
     */
    public boolean get(Club club, String startTime, String endTime, String daysOfWeek) {
        RealmResults<Session> res= realm.where(Session.class)
                .equalTo("club.name",club.getName())
                .and()
                .equalTo("daysOfWeek",daysOfWeek)
                .findAll();

        if(res.size()==0)
            return false;
        for (Session session:res) {
            int start= Integer.parseInt(session.getStartTime().replace(":",""));
            int end= Integer.parseInt(session.getEndTime().replace(":",""));
            if( Integer.parseInt(startTime.replace(":",""))<start & Integer.parseInt(endTime.replace(":",""))<start)
                return false;
            if(Integer.parseInt(startTime.replace(":",""))>end & Integer.parseInt(endTime.replace(":",""))>end)
                return false;
        }
        return true;

    }

    public void delete(int id)
    {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(Session.class).equalTo("id",id).findFirst().deleteFromRealm();
            }
        });
    }


    public void update(Session session)
    {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(session);
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.i("DataBase_UpdateSession", error.getMessage()+"");
            }
        });
    }

    public void close()
    {
        realm.close();
    }

    public void deleteAll() {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(Session.class).findAll().deleteAllFromRealm();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.i("DB_DeleteAllSession", error.getMessage()+"");
            }
        });
    }


}
