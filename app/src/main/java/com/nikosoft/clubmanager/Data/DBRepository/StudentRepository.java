package com.nikosoft.clubmanager.Data.DBRepository;

import android.util.Log;

import com.nikosoft.clubmanager.Data.Model.Club;
import com.nikosoft.clubmanager.Data.Model.Coach;
import com.nikosoft.clubmanager.Data.Model.Field;
import com.nikosoft.clubmanager.Data.Model.Student;

import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;

public class StudentRepository {

    private Realm realm;

    @Inject
    public StudentRepository() {
        realm = Realm.getDefaultInstance();
    }

    public int save(Student student) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                //    ایجاد مقدار فیلد ای دی به صورت افزایشی
                if (student.getId() == 0) {
                    int id;
                    Number currentId = realm.where(Student.class).max("id");
                    if (currentId == null)
                        id = 1;
                    else
                        id = currentId.intValue() + 1;
                    student.setId(id);
                }

                realm.copyToRealmOrUpdate(student);


            }
        });
        return student.getId();
    }


    public List<Student> getAll() {
        return realm.where(Student.class).findAll();
    }


    public List<Student> getAllAsync() {
        return realm.where(Student.class).findAllAsync();
    }


    public Student get(int id) {
        return realm.where(Student.class).equalTo("id", id).findFirst();
    }

    public Student getAsync(int studentId) {
        return realm.where(Student.class).equalTo("id", studentId).findFirstAsync();
    }

    public Student get(String name) {
        return realm.where(Student.class).equalTo("name", name).findFirst();
    }

    public void delete(int id) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(Student.class).equalTo("id", id).findFirst().deleteFromRealm();
            }
        });
    }

    public List<Student> get(Field field, Club club, Coach coach) {
        /*RealmQuery<Student> query=realm.where(Student.class);
        query.equalTo("field",field.toString());
        query.and().contains("clubs",club.toString());
        query.and().contains("coaches",coach.toString());
                RealmResults<Student> results =query.findAll();
        return results;*/

        return realm.where(Student.class)
                .equalTo("field.title", field.getTitle())
                .and()
                .contains("clubs.name", club.getName())
                .and()
                .contains("coaches.name", coach.getName())
                .findAll();
    }

    public void update(Student student) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(student);
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.i("DataBase_UpdateStudent", error.getMessage() + "");
            }
        });
    }

    public void close() {
        realm.close();
    }

    public void deleteAll() {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(Student.class).findAll().deleteAllFromRealm();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.i("DB_DeleteAllStudent", error.getMessage() + "");
            }
        });
    }


}
