package com.nikosoft.clubmanager.Data.DBRepository;

import com.nikosoft.clubmanager.Data.Model.TuitionPayment;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.Sort;

public class TuitionPaymentRepository {

    private Realm realm;
    private int tuitionPaymentId;

    @Inject
    public TuitionPaymentRepository() {
        realm = Realm.getDefaultInstance();
    }

    public int save(TuitionPayment tuitionPayment) {

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                //    ایجاد مقدار فیلد ای دی به صورت افزایشی

                if (tuitionPayment.getId() == 0) {

                    Number currentId = realm.where(TuitionPayment.class).max("id");
                    if (currentId == null)
                        tuitionPaymentId = 1;
                    else
                        tuitionPaymentId = currentId.intValue() + 1;
                    tuitionPayment.setId(tuitionPaymentId);
                }

                /*Tuition db_tuition = realm.createObject(Tuition.class, tuitionPaymentId);
                db_tuition.setTuitionCost(tuition.getTuitionCost());*/
                realm.copyToRealmOrUpdate(tuitionPayment);

            }

        });
        return tuitionPaymentId;
    }

    public List<TuitionPayment> getAll() {
        return realm.where(TuitionPayment.class).findAll();
    }


    public TuitionPayment get(int id) {
        return realm.where(TuitionPayment.class).equalTo("id", id).findFirst();
    }

    public List<TuitionPayment> getTuitionPayments(int studentId) {
        /*//if (realm.isAutoRefresh())
            return Realm.getDefaultInstance().where(TuitionPayment.class).equalTo("student.id", studentId).sort("payDate", Sort.DESCENDING).findAllAsync().asFlowable();
        else*/
            return realm.where(TuitionPayment.class).equalTo("student.id", studentId).sort("payDate", Sort.DESCENDING).findAll();

    }

    public TuitionPayment getTuitionPayment(int studentId, int tuitionId, String clubName, long dueDate) {

        return realm.where(TuitionPayment.class)
                .equalTo("student.id", studentId)
                .equalTo("tuition.id", tuitionId)
                .equalTo("club.name", clubName)
                .equalTo("dueDate", dueDate).findFirst();
    }

    public TuitionPayment getStudentLastPay(int studentId)
    {
        return realm.where(TuitionPayment.class).equalTo("student.id", studentId).sort("dueDate", Sort.DESCENDING).findFirst();
    }

    public void delete(int id) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Objects.requireNonNull(realm.where(TuitionPayment.class).equalTo("id", id).findFirst()).deleteFromRealm();
            }
        });
    }

    public void deleteByStudent(int studentId) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Objects.requireNonNull(realm.where(TuitionPayment.class).equalTo("student.id", studentId).findAll()).deleteAllFromRealm();
            }
        });
    }


    public void update(TuitionPayment tuitionPayment) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(tuitionPayment);
            }

        });
    }

    public void close() {
        realm.close();
    }

    public void deleteAll() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(TuitionPayment.class).findAll().deleteAllFromRealm();
            }

        });
    }
}
