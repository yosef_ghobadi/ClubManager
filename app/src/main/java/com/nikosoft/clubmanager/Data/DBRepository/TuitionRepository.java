package com.nikosoft.clubmanager.Data.DBRepository;

import com.nikosoft.clubmanager.Data.Model.Tuition;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import io.realm.Realm;

public class TuitionRepository {

    private Realm realm;
    private int tuitionId;

    @Inject
    public TuitionRepository()
    {
        realm=Realm.getDefaultInstance();
    }

    public int save(Tuition tuition) {

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                //    ایجاد مقدار فیلد ای دی به صورت افزایشی

                if(tuition.getId()==0) {

                    Number currentId = realm.where(Tuition.class).max("id");
                    if (currentId == null)
                        tuitionId = 1;
                    else
                        tuitionId = currentId.intValue() + 1;
                    tuition.setId(tuitionId);
                }

                /*Tuition db_tuition = realm.createObject(Tuition.class, tuitionId);
                db_tuition.setTuitionCost(tuition.getTuitionCost());*/
                realm.copyToRealmOrUpdate(tuition);

            }

        });
        return tuitionId;
    }

    public List<Tuition> getAll(){
        return realm.where(Tuition.class).findAll();
    }


    public Tuition get(int id)
    {
        return realm.where(Tuition.class).equalTo("id",id).findFirst();
    }

    public Tuition getTuition(long startDate,long endDate)
    {

        return  realm.where(Tuition.class)
                .greaterThan("endDate",startDate)
                .and()
                .lessThan("startDate",endDate)
                .or()
                .lessThan("startDate",endDate)
                .and()
                .greaterThan("endDate",startDate)
                .findFirst();
    }

    public void delete(int id)
    {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                //realm.where(Club.class).equalTo("tuition.id",id).findFirst().settuition(null);
                Objects.requireNonNull(realm.where(Tuition.class).equalTo("id", id).findFirst()).deleteFromRealm();
            }
        });
    }


    public void update(Tuition tuition)
    {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(tuition);
            }

        });
    }

    public void close()
    {
        realm.close();
    }

    public void deleteAll() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(Tuition.class).findAll().deleteAllFromRealm();
            }

        });
    }
}
