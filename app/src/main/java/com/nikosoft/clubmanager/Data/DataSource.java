package com.nikosoft.clubmanager.Data;

import com.nikosoft.clubmanager.Data.Model.Club;
import com.nikosoft.clubmanager.Data.Model.Coach;
import com.nikosoft.clubmanager.Data.Model.Field;
import com.nikosoft.clubmanager.Data.Model.Insurance;
import com.nikosoft.clubmanager.Data.Model.InsurancePayment;
import com.nikosoft.clubmanager.Data.Model.SMS;
import com.nikosoft.clubmanager.Data.Model.SalaryPayment;
import com.nikosoft.clubmanager.Data.Model.Session;
import com.nikosoft.clubmanager.Data.Model.Student;
import com.nikosoft.clubmanager.Data.Model.Tuition;
import com.nikosoft.clubmanager.Data.Model.TuitionPayment;
import com.nikosoft.clubmanager.Models.Banner;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by Yosef on 08/02/2019.
 */

public interface DataSource {
    Single<List<Banner>> getSliderImages();
    List<Integer> getDefaltSliderImages();
    List<Club> getClubs();
    List<Coach> getCoaches();
    Coach getCoach(int id);
    List<Field> getFields();
    List<Tuition> getTuitions();
    List<Student> getStudents();
    Student getStudent(int id);
    List<Insurance> getInsurances();
    List<Session> getSessions();
    List<TuitionPayment> getTuitionPayments(int studentId);
    List<InsurancePayment> getInsurancePayment(int studentId);
    List<SalaryPayment> getSalaryPayments(int coachId);
    List<SMS> getSMSList();

    Club getClub(int clubId);
}
