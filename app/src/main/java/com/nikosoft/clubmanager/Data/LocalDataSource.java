package com.nikosoft.clubmanager.Data;

import com.nikosoft.clubmanager.Data.DBRepository.ClubRepository;
import com.nikosoft.clubmanager.Data.DBRepository.CoachRepository;
import com.nikosoft.clubmanager.Data.DBRepository.FieldRepository;
import com.nikosoft.clubmanager.Data.DBRepository.InsurancePaymentRepository;
import com.nikosoft.clubmanager.Data.DBRepository.InsuranceRepository;
import com.nikosoft.clubmanager.Data.DBRepository.SMSRepository;
import com.nikosoft.clubmanager.Data.DBRepository.SalaryPaymentRepository;
import com.nikosoft.clubmanager.Data.DBRepository.SessionRepository;
import com.nikosoft.clubmanager.Data.DBRepository.StudentRepository;
import com.nikosoft.clubmanager.Data.DBRepository.TuitionPaymentRepository;
import com.nikosoft.clubmanager.Data.DBRepository.TuitionRepository;
import com.nikosoft.clubmanager.Data.Model.Club;
import com.nikosoft.clubmanager.Data.Model.Coach;
import com.nikosoft.clubmanager.Data.Model.Field;
import com.nikosoft.clubmanager.Data.Model.Insurance;
import com.nikosoft.clubmanager.Data.Model.InsurancePayment;
import com.nikosoft.clubmanager.Data.Model.SMS;
import com.nikosoft.clubmanager.Data.Model.SalaryPayment;
import com.nikosoft.clubmanager.Data.Model.Session;
import com.nikosoft.clubmanager.Data.Model.Student;
import com.nikosoft.clubmanager.Data.Model.Tuition;
import com.nikosoft.clubmanager.Data.Model.TuitionPayment;
import com.nikosoft.clubmanager.Models.Banner;
import com.nikosoft.clubmanager.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * Created by Yosef on 08/02/2019.
 */

public class LocalDataSource implements DataSource {

    @Inject
    ClubRepository clubRepository;
    @Inject
    CoachRepository coachRepository;
    @Inject
    FieldRepository fieldRepository;
    @Inject
    TuitionRepository tuitionRepository;
    @Inject
    StudentRepository studentRepository;
    @Inject
    InsuranceRepository insuranceRepository;
    @Inject
    SessionRepository sessionRepository;
    @Inject
    TuitionPaymentRepository tuitionPaymentRepository;
    @Inject
    InsurancePaymentRepository insurancePaymentRepository;
    @Inject
    SalaryPaymentRepository salaryPaymentRepository;
    @Inject
    SMSRepository smsRepository;

    @Inject
    public LocalDataSource() {

    }

    @Override
    public Single<List<Banner>> getSliderImages() {
        return null;
    }

    @Override
    public List<Integer> getDefaltSliderImages() {
        List<Integer> imagesId = new ArrayList<>();
        imagesId.add(R.drawable.banner11);
        imagesId.add(R.drawable.banner22);
        imagesId.add(R.drawable.banner33);
        imagesId.add(R.drawable.banner44);
        return imagesId;
    }

    @Override
    public List<Club> getClubs() {
        List<Club> clubList = clubRepository.getAll();
        //clubRepository.close();
        return clubList;
    }

    @Override
    public List<Coach> getCoaches() {
        return coachRepository.getAll();
    }

    @Override
    public Coach getCoach(int id) {
        return coachRepository.get(id);
    }

    @Override
    public List<Field> getFields() {
        return fieldRepository.getAll();
    }

    @Override
    public List<Tuition> getTuitions() {
        return tuitionRepository.getAll();
    }

    @Override
    public List<Student> getStudents() {
        return studentRepository.getAll();
    }

    @Override
    public Student getStudent(int id) {
        return studentRepository.get(id);
    }

    @Override
    public List<Insurance> getInsurances() {
        return insuranceRepository.getAll();
    }

    @Override
    public List<Session> getSessions() {
        return sessionRepository.getAll();
    }

    @Override
    public List<TuitionPayment> getTuitionPayments(int studentId) {
        return tuitionPaymentRepository.getTuitionPayments(studentId);
    }

    @Override
    public List<InsurancePayment> getInsurancePayment(int studentId) {
        return insurancePaymentRepository.getInsurancePayment(studentId);
    }

    @Override
    public List<SalaryPayment> getSalaryPayments(int coachId) {
        return salaryPaymentRepository.getSalaryPayment(coachId);
    }

    @Override
    public List<SMS> getSMSList() {
        return smsRepository.getAll();
    }

    @Override
    public Club getClub(int clubId) {
        return clubRepository.getClub(clubId);
    }


}
