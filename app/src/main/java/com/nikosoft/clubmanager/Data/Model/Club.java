package com.nikosoft.clubmanager.Data.Model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.LinkingObjects;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Yosef on 02/02/2019.
 */

public class Club extends RealmObject {

    @PrimaryKey
    private int id;
    private String name;
    private int monthlyCost;
    private int tuitionDay;
    private String startTime;
    private String endTime;
    private String description;
    private Tuition tuition;
    private RealmList<Field> fields;
    @LinkingObjects("clubs")
    private final RealmResults<Coach> coaches=null;
    @LinkingObjects("club")
    private final RealmResults<SalaryPayment> coach_salaryPayments =null;
    @LinkingObjects("clubs")
    private final RealmResults<Student> students=null;
    @LinkingObjects("club")
    private final RealmResults<Session> sessions=null;

    public Club() {
    }


    public Club(int id, String name, int monthlyCost, int tuitionDay, String startTime, String endTime, String description, Tuition tuition, RealmList<Field> fields) {
        this.id = id;
        this.name = name;
        this.monthlyCost = monthlyCost;
        this.tuitionDay = tuitionDay;
        this.startTime = startTime;
        this.endTime = endTime;
        this.description = description;
        this.tuition = tuition;
        this.fields = fields;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMonthlyCost() {
        return monthlyCost;
    }

    public void setMonthlyCost(int monthlyCost) {
        this.monthlyCost = monthlyCost;
    }

    public int getTuitionDay() {
        return tuitionDay;
    }

    public void setTuitionDay(int tuitionDay) {
        this.tuitionDay = tuitionDay;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Tuition gettuition() {
        return tuition;
    }

    public void settuition(Tuition tuition) {
        this.tuition = tuition;
    }

    public RealmList<Field> getFields() {
        return fields;
    }

    public void setFields(RealmList<Field> fields) {
        this.fields = fields;
    }

    public RealmResults<Coach> getCoaches() {
        return coaches;
    }

    public RealmResults<SalaryPayment> getCoach_salaryPayments() {
        return coach_salaryPayments;
    }

    public RealmResults<Student> getStudents() {
        return students;
    }

    public RealmResults<Session> getSessions() {
        return sessions;
    }
}
