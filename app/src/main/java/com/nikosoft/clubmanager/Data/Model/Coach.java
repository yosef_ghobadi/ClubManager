package com.nikosoft.clubmanager.Data.Model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.LinkingObjects;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Yosef on 02/02/2019.
 */

public class Coach extends RealmObject {

    @PrimaryKey
    private int     id;
    private String  name;
    private long    birthDate;
    private String  image;
    private long    date;
    private int     contractType;
    private long    salary;
    private String  phone;
    private RealmList<Club> clubs;
    private Field field;
    private boolean activityStatus;
    @LinkingObjects("coach")
    private final RealmResults<SalaryPayment> coach_salaryPayments =null;
    @LinkingObjects("coaches")
    private final RealmResults<Student> students=null;
    @LinkingObjects("coach")
    private final RealmResults<Session> sessions=null;


    public Coach() {
    }


    public Coach(int id, String name, long birthDate, String image, long date, int contractType, long salary, String phone, RealmList<Club> clubs, Field field,boolean activityStatus) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.image = image;
        this.date = date;
        this.contractType = contractType;
        this.salary = salary;
        this.phone = phone;
        this.clubs = clubs;
        this.field = field;
        this.activityStatus=activityStatus;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public int getContractType() {
        return contractType;
    }

    public void setContractType(int contractType) {
        this.contractType = contractType;
    }

    public long getSalary() {
        return salary;
    }

    public void setSalary(long salary) {
        this.salary = salary;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public RealmList<Club> getClubs() {
        return clubs;
    }

    public void setClubs(RealmList<Club> clubs) {
        this.clubs = clubs;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public boolean isActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(boolean activityStatus) {
        this.activityStatus = activityStatus;
    }

    public RealmResults<SalaryPayment> getCoach_salaryPayments() {
        return coach_salaryPayments;
    }

    public RealmResults<Student> getStudents() {
        return students;
    }

    public RealmResults<Session> getSessions() {
        return sessions;
    }


}
