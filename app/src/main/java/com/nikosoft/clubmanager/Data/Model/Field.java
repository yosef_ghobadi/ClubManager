package com.nikosoft.clubmanager.Data.Model;

import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.LinkingObjects;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Yosef on 02/02/2019.
 */

public class Field extends RealmObject {

    @PrimaryKey
    private int     id;
    private String  title;

    @LinkingObjects("fields")
    private final RealmResults<Club> clubs=null;
    @LinkingObjects("field")
    private final RealmResults<Coach> coaches=null;
    @LinkingObjects("field")
    private final RealmResults<Student> students=null;
    @LinkingObjects("field")
    private final RealmResults<Session> sessions=null;


    public Field() {
    }

    public Field( int id, String title) {

        this.id = id;
        this.title = title;


    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }




    public RealmResults<Club> getClubs() {
        return clubs;
    }

    public RealmResults<Coach> getCoaches() {
        return coaches;
    }

    public RealmResults<Student> getStudents() {
        return students;
    }

    public RealmResults<Session> getSessions() {
        return sessions;
    }
}
