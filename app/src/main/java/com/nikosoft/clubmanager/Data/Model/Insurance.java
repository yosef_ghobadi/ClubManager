package com.nikosoft.clubmanager.Data.Model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Yosef on 02/02/2019.
 */

public class Insurance extends RealmObject {

    @PrimaryKey
    private int     id;
    private int     cost;
    private String title;


    public Insurance() {
    }

    public Insurance( int id, int cost, String title) {

        this.id = id;
        this.cost = cost;
        this.title = title;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
