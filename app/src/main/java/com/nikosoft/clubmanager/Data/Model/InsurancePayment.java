package com.nikosoft.clubmanager.Data.Model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class InsurancePayment extends RealmObject {

    @PrimaryKey
    private int id;
    private Student student;
    private Insurance insurance;
    private long date;

    public InsurancePayment() {
    }

    public InsurancePayment(int id, Student student, Insurance insurance, long date) {
        this.id = id;
        this.student = student;
        this.insurance = insurance;
        this.date = date;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Insurance getInsurance() {
        return insurance;
    }

    public void setInsurance(Insurance insurance) {
        this.insurance = insurance;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }
}
