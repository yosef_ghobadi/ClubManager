package com.nikosoft.clubmanager.Data.Model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class SMS extends RealmObject {
    @PrimaryKey
    private int id;
    private String message;
    private long date;
    private int sent;
    private int studentId;

    public SMS() {
    }

    public SMS(int id, String message, long date, int sent, int studentId) {
        this.id = id;
        this.message = message;
        this.date = date;
        this.sent = sent;
        this.studentId = studentId;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }


    public int getSent() {
        return sent;
    }

    public void setSent(int sent) {
        this.sent = sent;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }
}
