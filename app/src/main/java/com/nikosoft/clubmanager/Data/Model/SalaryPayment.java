package com.nikosoft.clubmanager.Data.Model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class SalaryPayment extends RealmObject {

    @PrimaryKey
    private int id;
    private long date;
    private Club club;
    private Coach coach;
    private long paidAmount;

    public SalaryPayment() {
    }

    public SalaryPayment(int id, long date, Club club, Coach coach, long paidAmount) {
        this.id = id;
        this.date = date;
        this.club = club;
        this.coach = coach;
        this.paidAmount = paidAmount;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public Coach getCoach() {
        return coach;
    }

    public void setCoach(Coach coach) {
        this.coach = coach;
    }

    public long getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(long paidAmount) {
        this.paidAmount = paidAmount;
    }
}
