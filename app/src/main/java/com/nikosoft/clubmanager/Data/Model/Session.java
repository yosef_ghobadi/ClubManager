package com.nikosoft.clubmanager.Data.Model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Session extends RealmObject {

    @PrimaryKey
    private int id;
    private Club club;
    private Field field;
    private Coach coach;
    private RealmList<Student> students;
    private String startTime;
    private String endTime;
    private int gender;
    private String daysOfWeek;

    public Session()
    {

    }

    public Session(int id, Club club, Field field, Coach coach, RealmList<Student> students, String startTime, String endTime, int gender, String daysOfWeek) {
        this.id = id;
        this.club = club;
        this.field = field;
        this.coach = coach;
        this.students = students;
        this.startTime = startTime;
        this.endTime = endTime;
        this.gender = gender;
        this.daysOfWeek = daysOfWeek;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public Coach getCoach() {
        return coach;
    }

    public void setCoach(Coach coach) {
        this.coach = coach;
    }

    public RealmList<Student> getStudents() {
        return students;
    }

    public void setStudents(RealmList<Student> students) {
        this.students = students;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(String daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }
}
