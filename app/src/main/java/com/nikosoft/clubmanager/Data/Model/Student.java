package com.nikosoft.clubmanager.Data.Model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.LinkingObjects;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Yosef on 02/02/2019.
 */

public class Student extends RealmObject   {

    @PrimaryKey
    private int                 id;
    private String              name;
    private long                birthDate;
    private long                joinDate;
    private String              phone;
    private String              image;
    private String              description;
    private RealmList<Club>     clubs;
    private RealmList<Coach>    coaches;
    private Field               field;
    private boolean             activityStatus;
     @LinkingObjects("students")
     final RealmResults<Session> sessions=null;




    public Student() {
    }
 public Student(Student student) {
     this.id = student.getId();
     this.name = student.getName();
     this.birthDate = student.getBirthDate();
     this.joinDate = student.getJoinDate();
     this.phone = student.getPhone();
     this.image = student.getImage();
     this.description = student.getDescription();
     this.clubs = student.getClubs();
     this.coaches = student.getCoaches();
     this.field = student.getField();
     this.activityStatus=student.isActivityStatus();
    }

    public Student( int id, String name, long birthDate, long joinDate,  String phone, String image, String description, RealmList<Club> clubs, RealmList<Coach> coaches, Field field,boolean activityStatus) {

        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.joinDate = joinDate;
        this.phone = phone;
        this.image = image;
        this.description = description;
        this.clubs = clubs;
        this.coaches = coaches;
        this.field = field;
        this.activityStatus=activityStatus;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(long joinDate) {
        this.joinDate = joinDate;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }



    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RealmList<Club> getClubs() {
        return clubs;
    }

    public void setClubs(RealmList<Club> clubs) {
        this.clubs = clubs;
    }

    public RealmList<Coach> getCoaches() {
        return coaches;
    }

    public void setCoaches(RealmList<Coach> coaches) {
        this.coaches = coaches;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public boolean isActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(boolean activityStatus) {
        this.activityStatus = activityStatus;
    }

    public RealmResults<Session> getSessions() {
        return sessions;
    }




}
