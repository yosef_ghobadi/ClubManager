package com.nikosoft.clubmanager.Data.Model;

import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.LinkingObjects;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Yosef on 03/02/2019.
 */

public class Tuition extends RealmObject {

    @PrimaryKey
    private int id;
    private int tuitionCost;
    private long startDate;
    private long endDate;
    @LinkingObjects("tuition")
    private final RealmResults<Club> clubs=null;

    public Tuition() {
    }

    public Tuition(int id, int tuitionCost, long startDate, long endDate) {
        this.id = id;
        this.tuitionCost = tuitionCost;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTuitionCost() {
        return tuitionCost;
    }

    public void setTuitionCost(int tuitionCost) {
        this.tuitionCost = tuitionCost;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    public RealmResults<Club> getClubs() {
        return clubs;
    }
}
