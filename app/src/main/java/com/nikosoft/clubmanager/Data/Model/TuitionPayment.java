package com.nikosoft.clubmanager.Data.Model;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class TuitionPayment extends RealmObject implements Parcelable {

    @PrimaryKey
    private int id;
    private Student student;
    private Tuition tuition;
    private long payDate;
    private Club club;
    private long dueDate;

    public TuitionPayment() {
    }

    public TuitionPayment(int id, Student student, Tuition tuition, long payDate, Club club, long dueDate) {
        this.id = id;
        this.student = student;
        this.tuition = tuition;
        this.payDate = payDate;
        this.club = club;
        this.dueDate = dueDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Tuition getTuition() {
        return tuition;
    }

    public void setTuition(Tuition tuition) {
        this.tuition = tuition;
    }

    public long getPayDate() {
        return payDate;
    }

    public void setPayDate(long payDate) {
        this.payDate = payDate;
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public long getDueDate() {
        return dueDate;
    }

    public void setDueDate(long dueDate) {
        this.dueDate = dueDate;
    }


    protected TuitionPayment(Parcel in) {
        id = in.readInt();
        tuition.setTuitionCost(in.readInt());
        club.setName(in.readString());
        payDate=in.readLong();
        dueDate=in.readLong();
    }

    public static final Creator<TuitionPayment> CREATOR = new Creator<TuitionPayment>() {
        @Override
        public TuitionPayment createFromParcel(Parcel in) {
            return new TuitionPayment(in);
        }

        @Override
        public TuitionPayment[] newArray(int size) {
            return new TuitionPayment[size];
        }
    };



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }
}
