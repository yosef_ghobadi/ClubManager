package com.nikosoft.clubmanager.Data;

import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.Data.Model.Club;
import com.nikosoft.clubmanager.Data.Model.Coach;
import com.nikosoft.clubmanager.Data.Model.Field;
import com.nikosoft.clubmanager.Data.Model.Insurance;
import com.nikosoft.clubmanager.Data.Model.InsurancePayment;
import com.nikosoft.clubmanager.Data.Model.SMS;
import com.nikosoft.clubmanager.Data.Model.SalaryPayment;
import com.nikosoft.clubmanager.Data.Model.Session;
import com.nikosoft.clubmanager.Data.Model.Student;
import com.nikosoft.clubmanager.Data.Model.Tuition;
import com.nikosoft.clubmanager.Data.Model.TuitionPayment;
import com.nikosoft.clubmanager.Models.Banner;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

/**
 * Created by Yosef on 08/02/2019.
 */

public class Repository implements DataSource {

    @Inject
     LocalDataSource localDataSource ;
    @Inject
     ServerDataSource serverDataSource ;

    public Repository(){
        ClubManager.getApplicationComponent().inject(this);
    }

    @Override
    public Single<List<Banner>> getSliderImages() {
        /*if (Utility.isInternetAvailable()) {

        }else
            return null;*/
        return serverDataSource.getSliderImages();
    }

    @Override
    public List<Integer> getDefaltSliderImages() {
        return localDataSource.getDefaltSliderImages();
    }

    @Override
    public List<Club> getClubs() {
        return localDataSource.getClubs();
    }

    @Override
    public List<Coach> getCoaches() {
        return localDataSource.getCoaches();
    }

    @Override
    public Coach getCoach(int id) {
        return localDataSource.getCoach(id);
    }

    @Override
    public List<Field> getFields() {
        return localDataSource.getFields();
    }

    @Override
    public List<Tuition> getTuitions() {
        return localDataSource.getTuitions();
    }

    @Override
    public List<Student> getStudents() {
        return localDataSource.getStudents();
    }

    @Override
    public Student getStudent(int id) {
        return localDataSource.getStudent(id);
    }

    @Override
    public List<Insurance> getInsurances() {
        return localDataSource.getInsurances();
    }

    @Override
    public List<Session> getSessions() {
        return localDataSource.getSessions();
    }

    @Override
    public List<TuitionPayment> getTuitionPayments(int studentId) {
        return localDataSource.getTuitionPayments(studentId);
    }

    @Override
    public List<InsurancePayment> getInsurancePayment(int studentId) {
        return localDataSource.getInsurancePayment(studentId);
    }

    @Override
    public List<SalaryPayment> getSalaryPayments(int coachId) {
        return localDataSource.getSalaryPayments(coachId);
    }

    @Override
    public List<SMS> getSMSList() {
        return localDataSource.getSMSList();
    }

    @Override
    public Club getClub(int clubId) {
        return localDataSource.getClub(clubId);
    }


}
