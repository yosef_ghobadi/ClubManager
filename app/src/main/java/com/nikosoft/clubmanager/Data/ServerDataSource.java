package com.nikosoft.clubmanager.Data;

import com.nikosoft.clubmanager.Data.Model.Club;
import com.nikosoft.clubmanager.Data.Model.Coach;
import com.nikosoft.clubmanager.Data.Model.Field;
import com.nikosoft.clubmanager.Data.Model.Insurance;
import com.nikosoft.clubmanager.Data.Model.InsurancePayment;
import com.nikosoft.clubmanager.Data.Model.SMS;
import com.nikosoft.clubmanager.Data.Model.SalaryPayment;
import com.nikosoft.clubmanager.Data.Model.Session;
import com.nikosoft.clubmanager.Data.Model.Student;
import com.nikosoft.clubmanager.Data.Model.Tuition;
import com.nikosoft.clubmanager.Data.Model.TuitionPayment;
import com.nikosoft.clubmanager.Models.Banner;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Yosef on 08/02/2019.
 */

public class ServerDataSource implements DataSource {

    public ApiService apiService;

    @Inject
    public ServerDataSource()
    {
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl("http://192.168.1.102:7777/api/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiService=retrofit.create(ApiService.class);
    }

    @Override
    public Single<List<Banner>> getSliderImages() {
        return apiService.getSliderImages();
    }

    @Override
    public List<Integer> getDefaltSliderImages() {
        return null;
    }

    @Override
    public List<Club> getClubs() {
        return null;
    }

    @Override
    public List<Coach> getCoaches() {
        return null;
    }

    @Override
    public Coach getCoach(int id) {
        return null;
    }

    @Override
    public List<Field> getFields() {
        return null;
    }

    @Override
    public List<Tuition> getTuitions() {
        return null;
    }

    @Override
    public List<Student> getStudents() {
        return null;
    }

    @Override
    public Student getStudent(int id) {
        return null;
    }

    @Override
    public List<Insurance> getInsurances() {
        return null;
    }

    @Override
    public List<Session> getSessions() {
        return null;
    }

    @Override
    public List<TuitionPayment> getTuitionPayments(int studentId) {
        return null;
    }

    @Override
    public List<InsurancePayment> getInsurancePayment(int studentId) {
        return null;
    }

    @Override
    public List<SalaryPayment> getSalaryPayments(int coachId) {
        return null;
    }

    @Override
    public List<SMS> getSMSList() {
        return null;
    }

    @Override
    public Club getClub(int clubId) {
        return null;
    }


}
