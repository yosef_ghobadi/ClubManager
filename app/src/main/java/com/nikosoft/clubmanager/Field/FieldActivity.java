package com.nikosoft.clubmanager.Field;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.nikosoft.clubmanager.Adapters.Fields_Adapter;
import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.Data.DBRepository.FieldRepository;
import com.nikosoft.clubmanager.Data.Model.Field;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.Utiles.Utility;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FieldActivity extends AppCompatActivity implements FieldContract.View,OnFieldEditMenuClickListener {



    @BindView(R.id.txt_field_name)
    EditText txt_name;
    @BindView(R.id.lay_add_field)
    MaterialRippleLayout add_field;
    @BindView(R.id.recy_fields)
    RecyclerView recycler_fields;
    @BindView(R.id.toolbar_field)
    Toolbar toolbar;
    FieldContract.Presenter presenter;
    @Inject
    Utility utility;
    @Inject
    FieldRepository fieldRepository;

    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;
    List<Field> fieldList;
    public static boolean EDITMODE = false;
    public static int fieldID = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //dependency injection by dagger2
        ClubManager.getApplicationComponent().inject(this);
        //set app theme
        AppCompatDelegate.setDefaultNightMode(utility.getThemeNight());

        setContentView(R.layout.activity_field);

        ButterKnife.bind(this);

        presenter.attachView(this);

        setupViews();

    }




    /**
     * setup activitys views
     */
    private void setupViews() {
        setSupportActionBar(toolbar);
        //fields recycler
        layoutManager = new LinearLayoutManager(getViewContext());
        recycler_fields.setLayoutManager(layoutManager);
        adapter = new Fields_Adapter(getViewContext(), fieldList,this::onEditMenuClick);
        recycler_fields.setAdapter(adapter);

        //add or update field
        add_field.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //check empty title
                if (utility.isEmpty(txt_name)) {
                    utility.setError(txt_name, getString(R.string.emtpy_value_error));
                    return;
                }

                //check exist value

                    for (Field item : fieldList) {
                        if (item.getTitle().equals(txt_name.getText().toString())) {
                            utility.setError(txt_name, getString(R.string.exist_value_error));
                            return;
                        }
                    }


                fieldRepository.save(new Field(fieldID, txt_name.getText().toString()));
                adapter.notifyDataSetChanged();
                txt_name.setText("");
                fieldID = 0;

            }
        });

    }

    @Inject
    public void setPresenter(FieldPresenter presenter) {
        this.presenter = presenter;
    }





    @Override
    public Context getViewContext() {
        return this;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void FieldsList(List<Field> fieldList) {
        this.fieldList = fieldList;
    }

    @Override
    public void onEditMenuClick(Field field) {
        txt_name.setText(field.getTitle());
        EDITMODE = true;
        fieldID = field.getId();
    }

}
