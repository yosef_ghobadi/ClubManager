package com.nikosoft.clubmanager.Field;

import com.nikosoft.clubmanager.Base.BasePresenter;
import com.nikosoft.clubmanager.Base.BaseView;
import com.nikosoft.clubmanager.Data.Model.Field;

import java.util.List;

public interface FieldContract  {

    public interface View extends BaseView
    {
         void FieldsList(List<Field> fieldList);
    }

    public interface Presenter extends BasePresenter<View>{
        void getFieldsList();

    }
}
