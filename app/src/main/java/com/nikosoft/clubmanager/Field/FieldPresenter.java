package com.nikosoft.clubmanager.Field;

import com.nikosoft.clubmanager.Data.DataSource;

import javax.inject.Inject;

public class FieldPresenter implements FieldContract.Presenter {


    private FieldContract.View view;
    private DataSource dataSource;

    @Inject
    public FieldPresenter(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void getFieldsList() {
         view.FieldsList(dataSource.getFields());
    }

    @Override
    public void attachView(FieldContract.View view) {
        this.view=view;
        getFieldsList();
    }

    @Override
    public void detachView() {
        this.view=null;
    }
}
