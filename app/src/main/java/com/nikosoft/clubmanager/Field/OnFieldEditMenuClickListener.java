package com.nikosoft.clubmanager.Field;

import com.nikosoft.clubmanager.Data.Model.Field;

public interface OnFieldEditMenuClickListener {
    void onEditMenuClick(Field field);

}
