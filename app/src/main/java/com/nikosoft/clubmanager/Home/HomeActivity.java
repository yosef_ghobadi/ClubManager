package com.nikosoft.clubmanager.Home;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.farsitel.bazaar.IUpdateCheckService;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.ClubModify.ClubModifyActivity;
import com.nikosoft.clubmanager.Club_Fragment_Dialog.Fragment_dialog_modal;
import com.nikosoft.clubmanager.Coach.CoachActivity;
import com.nikosoft.clubmanager.Data.DBRepository.FieldRepository;
import com.nikosoft.clubmanager.Data.DBRepository.StudentRepository;
import com.nikosoft.clubmanager.Data.Model.Field;
import com.nikosoft.clubmanager.Field.FieldActivity;
import com.nikosoft.clubmanager.Insurance.InsuranceActivity;
import com.nikosoft.clubmanager.Models.Banner;
import com.nikosoft.clubmanager.MyAccount.MyAccountActivity;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.SMS.SMSActivity;
import com.nikosoft.clubmanager.Session.SessionActivity;
import com.nikosoft.clubmanager.Student.StudentActivity;
import com.nikosoft.clubmanager.Tuition.TuitionActivity;
import com.nikosoft.clubmanager.Utiles.SliderView;
import com.nikosoft.clubmanager.Utiles.Utility;
import com.nikosoft.clubmanager.Utiles.util.IabHelper;
import com.nikosoft.clubmanager.Utiles.util.IabResult;
import com.nikosoft.clubmanager.Utiles.util.Inventory;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.tapsell.sdk.nativeads.TapsellNativeVideoAd;
import ir.tapsell.sdk.nativeads.TapsellNativeVideoAdCompletionListener;
import ir.tapsell.sdk.nativeads.TapsellNativeVideoAdLoadListener;
import ir.tapsell.sdk.nativeads.TapsellNativeVideoAdLoader;

public class HomeActivity extends AppCompatActivity implements HomeContract.View {


    private static final String TAG = "HOME_BAZAR";
    @BindView(R.id.slider)
    SliderLayout slider;
    @BindView(R.id.item_club)
    View item_gym;
    @BindView(R.id.item_coach_ripple)
    View item_coach;
    @BindView(R.id.item_field_ripple)
    View item_field;
    @BindView(R.id.item_athlete_ripple)
    View item_athlete;
    @BindView(R.id.item_tuition_ripple)
    View item_tuition;
    @BindView(R.id.item_insurance_ripple)
    View item_insurance;
    @BindView(R.id.item_session_ripple)
    View item_session;
    @BindView(R.id.item_sms_ripple)
    View item_sms;
    @BindView(R.id.item_account_ripple)
    View item_account;
    @BindView(R.id.main_lay_bootom_sheet)
    LinearLayout bottomsheet_club;
    @BindView(R.id.recy_list_view_club)
    RecyclerView recyclerView;
    @BindView(R.id.lay_add_coach)
    View add_club;
    //ad parent layout
    @BindView(R.id.adParent)
    RelativeLayout adParent;
    @BindView(R.id.lay_slider)
    LinearLayout lay_slider;

    @BindView(R.id.lay_home_items)
    LinearLayout lay_items;
    //ad loader
    TapsellNativeVideoAdLoader.Builder ad_builder;
    TapsellNativeVideoAd tapsellNativeVideoAd;
    View.OnClickListener onClickListener;

    private HomeContract.Presenter presenter;
    //bazar helper
    IabHelper iabHelper;


    IUpdateCheckService service;
    UpdateServiceConnection connection;

    @Inject
    Utility utility;
    @Inject
    StudentRepository studentRepository;
    @Inject
    FieldRepository fieldRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ClubManager.getApplicationComponent().inject(this);
        AppCompatDelegate.setDefaultNightMode(utility.getThemeNight());
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        presenter.attachView(this);

        setupViews();

        //set first run flag
        if (utility.retrievePref(utility.FIRST_RUN, "-1", getViewContext()).equals("-1")) {
            utility.savePref(utility.FIRST_RUN, "1", getViewContext());
            //تعریف رشته های پیش فرض در اولین اجرا
            setDefaultFieldValues();
        } else
            utility.savePref(utility.FIRST_RUN, "0", getViewContext());


        // مقدار دهی اولیه مقدار اعتبار برنامه
        if (utility.retrievePref(utility.IS_VALID_APP, "-1", getViewContext()).equals("-1"))
            utility.savePref(utility.IS_VALID_APP, "0", getViewContext());

        //مقدار دهی اولیه تعداد دفعات عدم اتصال به اینترنت
        if (utility.retrievePref(utility.OFFLINE_LIMIT, "", getViewContext()).equals(""))
            utility.savePref(utility.OFFLINE_LIMIT, String.valueOf(utility.OFFLINE_LIMIT_COUNT), getViewContext());

        //اگر نسخه جدیدی از برنامه وجود داشت
        if (Integer.parseInt(utility.retrievePref(utility.NEW_VERSION, "0", getViewContext())) > utility.getAppVersionNumber())
            showUpdateDialog();

        //چک کردن معتبر بودن برنامه
        checkAppValidity();


        //show case view
        try {
            //show case view
            startShowCaseView(item_gym, this, "مدیریت باشگاه", "قدم اول \n یک باشگاه تعریف کنید", new TapTargetView.Listener() {

                @Override
                public void onTargetDismissed(TapTargetView view, boolean userInitiated) {

                    startShowCaseView(item_coach, getThisActivity(), "مدیریت مربیان",
                            "قدم دوم \nمربیان باشگاه خود را تعریف کنید\nاگر خودتان مربی هستید اطلاعات خودتان را ثبت کنید", new TapTargetView.Listener() {
                                @Override
                                public void onTargetDismissed(TapTargetView view, boolean userInitiated) {
                                    startShowCaseView(item_field, getThisActivity(), "مدیریت رشته ها",
                                            "قدم سوم \nاگر رشته های باشگاه شما در لیست رشته ها وجود ندارد\nیا رشته خود شما وجود ندارد, میتوانید تعریف کنید", new TapTargetView.Listener() {
                                                @Override
                                                public void onTargetDismissed(TapTargetView view, boolean userInitiated) {
                                                    startShowCaseView(item_athlete, getThisActivity(), "مدیریت شاگردان",
                                                            "قدم چهارم \nاطلاعات شاگردان باشگاه یا کلاس خود را وارد کنید\nدر این قسمت میتوانید شهریه ها و بیمه های منقضی را ببینید و برای آنها پیامک بفرستید", new TapTargetView.Listener() {
                                                                @Override
                                                                public void onTargetDismissed(TapTargetView view, boolean userInitiated) {
                                                                    startShowCaseView(item_tuition, getThisActivity(), "مدیریت شهریه ها",
                                                                            "قدم پنجم \nمیتوانید شهریه هر سال را تعریف کنید", new TapTargetView.Listener() {
                                                                                @Override
                                                                                public void onTargetDismissed(TapTargetView view, boolean userInitiated) {
                                                                                    startShowCaseView(item_insurance, getThisActivity(), "مدیریت بیمه ها",
                                                                                            "قدم ششم \nمیتوانید بیمه هر سال را نیز تعریف کنید", new TapTargetView.Listener() {
                                                                                                @Override
                                                                                                public void onTargetDismissed(TapTargetView view, boolean userInitiated) {
                                                                                                    startShowCaseView(item_session, getThisActivity(), "مدیریت سانس ها",
                                                                                                            "قدم هفتم \nمیتوانید سانسهای باشگاه را تعریف کنید", new TapTargetView.Listener() {
                                                                                                                @Override
                                                                                                                public void onTargetDismissed(TapTargetView view, boolean userInitiated) {
                                                                                                                    startShowCaseView(item_sms, getThisActivity(), "مدیریت پیامک ها",
                                                                                                                            "قدم هشتم \nمیتوانید پیامکهای ارسال شده به شاگردان را ببینید\n و از وضعیت ارسال آنها مطلع شوید", new TapTargetView.Listener() {
                                                                                                                                @Override
                                                                                                                                public void onTargetDismissed(TapTargetView view, boolean userInitiated) {
                                                                                                                                    startShowCaseView(item_account, getThisActivity(), "مدیریت حساب و شماره تلفن",
                                                                                                                                            "قدم آخر \nمیتوانید شماره تلفن خود را برای ارسال پیامک به شاگردان تنظیم نمایید\n و همچنین اشتراک های برنامه را تهیه کنید", new TapTargetView.Listener() {
                                                                                                                                                @Override
                                                                                                                                                public void onTargetDismissed(TapTargetView view, boolean userInitiated) {


                                                                                                                                                }
                                                                                                                                            });

                                                                                                                                }
                                                                                                                            });

                                                                                                                }
                                                                                                            });
                                                                                                }
                                                                                            });
                                                                                }
                                                                            });
                                                                }
                                                            });
                                                }
                                            });
                                }
                            });
                }
            });

        } catch (
                Exception e) {
            e.printStackTrace();
        }


        //چک کردن نسخه برنامه
        if (utility.isConnectedInternet(getViewContext()))
            initService();

        //نمایش تبلیغ
        showAds();

    }


    private void showAds() {

        ad_builder = new TapsellNativeVideoAdLoader.Builder();
        ad_builder.setContentViewTemplate(R.layout.tapsell_ad);
        ad_builder.setAppInstallationViewTemplate(R.layout.tabsell_ad_app);
        ad_builder.setAutoStartVideoOnScreenEnabled(false);
        ad_builder.setFullscreenBtnEnabled(true);
        ad_builder.setMuteVideoBtnEnabled(false);
        ad_builder.setMuteVideo(false);

        ad_builder.loadAd(getViewContext(), "5d444a67b334d30001c50a8b", new TapsellNativeVideoAdLoadListener() {

            @Override
            public void onNoNetwork() {
                Log.e("Tapsell", "No Network Available");
                //Toast.makeText(getViewContext(), "No Network Available", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNoAdAvailable() {
                Log.e("Tapsell", "No Native Video Ad Available");
                //Toast.makeText(getViewContext(), "No Native Video Ad Available", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(String error) {
                Log.e("Tapsell", "Error: " + error);
                //Toast.makeText(getViewContext(), "Error: " + error, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRequestFilled(TapsellNativeVideoAd videoAd) {
                tapsellNativeVideoAd = videoAd;
                tapsellNativeVideoAd.setCompletionListener(new TapsellNativeVideoAdCompletionListener() {
                    @Override
                    public void onAdShowFinished(String adId) {
                        Log.e("Tapsell", "onAdShowFinished: " + adId);

                        setADLayoutVisibility(false);
                    }
                });
                tapsellNativeVideoAd.setOnClickListener(onClickListener);

                setADLayoutVisibility(true);
                tapsellNativeVideoAd.addToParentView((RelativeLayout) findViewById(R.id.adParent));
            }

        });
    }

    private void setADLayoutVisibility(boolean visibility) {
        if(visibility)
        {
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            lay_items.setLayoutParams(param);

            adParent.setVisibility(View.VISIBLE);
            lay_slider.setVisibility(View.GONE);
        }
        else
        {
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    0,
                    0.65f
            );
            lay_items.setLayoutParams(param);

            /*LinearLayout.LayoutParams param1 = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    0,
                    0.35f
            );
            lay_slider.setLayoutParams(param1);*/


            adParent.setVisibility(View.GONE);
            lay_slider.setVisibility(View.VISIBLE);
        }

    }



    private void checkAppValidity() {
        //اگر اشتراک برنامه به اتمام رسیده بود
        if (utility.retrievePref(utility.IS_VALID_APP, "-1", getViewContext()).equals("0")) {

            //اگر تعداد دانش آموزان بیشتر از تعداد محدود باشد
            if (studentRepository.getAll().size() > utility.LIMIT_COUNT) {
                //اگر اتصال اینترنت برقرار باشد
                if (utility.isConnectedInternet(getViewContext())) {

                    //اتصال به بازار و چک کردن وضعیت اشتراک
                    connectToBazar();
                    //اگر اشتراک برنامه به اتمام رسیده بود
                    if (utility.retrievePref(utility.IS_VALID_APP, "-1", getViewContext()).equals("0")) {
                        Toast.makeText(getViewContext(), getString(R.string.subs_expired), Toast.LENGTH_LONG).show();
                        getAccount();
                    } else {
                        return;
                    }

                } else {
                    int limit = Integer.parseInt(utility.retrievePref(utility.OFFLINE_LIMIT, "-1", getViewContext()));
                    Log.i("OFFLINE_LIMIT", limit + "");

                    if (limit > 0) {
                        //کم کردن از تعداد محدودیت عدم اتصال به اینترنت
                        utility.savePref(utility.OFFLINE_LIMIT, String.valueOf(limit - 1), getViewContext());
                        return;
                    } else {
                        showInternetConnectionDialog();
                    }

                }
            }

        }
    }

    private void getAccount() {
        Intent intent = new Intent(HomeActivity.this, MyAccountActivity.class);
        startActivity(intent);
    }

    private void connectToBazar() {
        try {
            //اتصال به بازار با IabHelper
            iabHelper = new IabHelper(getViewContext(), utility.getRSAKey(getViewContext()));
            Log.d(TAG, "Starting setup.");

            iabHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
                @Override
                public void onIabSetupFinished(IabResult result) {
                    if (!result.isSuccess()) {
                        Toast.makeText(getViewContext(), result.getMessage() + "خطا در بر قراری ارتباط \nوارد حساب خود در کافه بازار شوید", Toast.LENGTH_LONG).show();

                    } else {
                        Log.d(TAG, "Query inventory start.");

                        //ارسال درخواست برای دریافت محصولات
                        iabHelper.queryInventoryAsync(true, new IabHelper.QueryInventoryFinishedListener() {
                            @Override
                            public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
                                Log.d(TAG, "Query inventory finished.");
                                if (result.isFailure()) {
                                    Log.d(TAG, "Failed to query inventory: " + result);
                                    return;
                                } else {
                                    Log.d(TAG, "Query inventory was successful.");

                                    //-----------------------------------------دریافت اشتراک خریده شده توسط کاربر و اعمال آن
                                    if (inventory.hasPurchase(utility.MONTHLY_SUB)) {
                                        Log.d(TAG, "اشتراک ماهانه" + inventory.getPurchase(utility.MONTHLY_SUB).getOriginalJson());
                                        //update internet connection limit
                                        utility.setAppValidity(true, getViewContext());
                                    } else if (inventory.hasPurchase(utility.YEARLY_SUB)) {
                                        Log.d(TAG, "اشتراک سالانه" + inventory.getPurchase(utility.YEARLY_SUB).getOriginalJson());

                                        //update internet connection limit
                                        utility.setAppValidity(true, getViewContext());
                                    } else {

                                        utility.setAppValidity(false, getViewContext());
                                    }

                                }

                                Log.d(TAG, "Initial inventory query finished; enabling main UI.");
                            }
                        });


                    }

                }
            });


        } catch (Exception ex) {
            Toast.makeText(getViewContext(), "برنامه بازار روی تلفن شما نصب نیست\nاگر این برنامه را از کافه بازار دریافت نکرده اید ،برنامه را از بازار دریافت کنید", Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

    private void showInternetConnectionDialog() {
        //show dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getViewContext());
        builder.setTitle(getViewContext().getString(R.string.internet_connection));
        builder.setMessage(getViewContext().getString(R.string.please_connect_to_internet));
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //close app
                utility.closeApp();
            }
        });

        builder.show();
    }



    private void showAppComments()
    {
        //show dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getViewContext());
        builder.setTitle(getViewContext().getString(R.string.exit));
        builder.setMessage(getViewContext().getString(R.string.please_leave_comment));
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.commenting, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(utility.isConnectedInternet(getViewContext())) {
                    Intent intent = new Intent(Intent.ACTION_EDIT);
                    intent.setData(Uri.parse("bazaar://details?id=" + getPackageName()));
                    intent.setPackage("com.farsitel.bazaar");
                    startActivity(intent);
                }
                else
                    Toast.makeText(getViewContext(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
            }
        });

        builder.setNegativeButton(R.string.exit, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //close app
                utility.closeApp();
            }
        });
        builder.show();
    }

    private void setDefaultFieldValues() {
        try {
            int n = fieldRepository.getAll().size();
            if (fieldRepository.getAll().size() == 0) {
                fieldRepository.save(new Field(0, "کیک بوکسینگ"));
                fieldRepository.save(new Field(0, "کونگ فو"));
                fieldRepository.save(new Field(0, "کاراته"));
                fieldRepository.save(new Field(0, "ووشو"));
                fieldRepository.save(new Field(0, "ژیمناستیک"));
                fieldRepository.save(new Field(0, "کشتی"));
                fieldRepository.save(new Field(0, "جودو"));
                fieldRepository.save(new Field(0, "موی تای"));
                fieldRepository.save(new Field(0, "تکواندو"));
                fieldRepository.save(new Field(0, "کیوکوشین"));
                fieldRepository.save(new Field(0, "ایروبیک"));
                fieldRepository.save(new Field(0, "دفاع شخصی"));
                fieldRepository.save(new Field(0, "آیکیدو"));
                fieldRepository.save(new Field(0, "بوکس"));
                fieldRepository.save(new Field(0, "بدنسازی"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Inject
    public void setPresenter(HomePresenter presenter) {
        this.presenter = presenter;
    }

    /**
     * setup all view on HomeActivity
     */
    private void setupViews() {

        //setSupportActionBar(toolbar);


        item_gym.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Fragment_dialog_modal modal = new Fragment_dialog_modal();
                modal.show(getSupportFragmentManager(), modal.getTag());
            }
        });

        item_coach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, CoachActivity.class));
                //Crashlytics.getInstance().crash();
            }
        });

        add_club.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, ClubModifyActivity.class));
            }
        });

        item_field.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, FieldActivity.class));
            }
        });

        item_athlete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, StudentActivity.class));
            }
        });


        item_tuition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, TuitionActivity.class));
            }
        });


        item_insurance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, InsuranceActivity.class));
            }
        });

        item_session.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, SessionActivity.class));
            }
        });

        item_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, SMSActivity.class));
            }
        });

        item_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, MyAccountActivity.class));
            }
        });

    }


    @Override
    public void onBackPressed() {
        showAppComments();
    }


    @Override
    public Context getViewContext() {
        return HomeActivity.this;
    }

    public HomeActivity getThisActivity() {
        return HomeActivity.this;
    }

    /**
     * @param images load slider images from server and show them if exist
     */
    @Override
    public void showSlider(List<Banner> images) {
        //مقدار دهی اسلایدر
        //از اینترنت

        for (Banner banner : images) {
            SliderView sliderView = new SliderView(this);
            sliderView.image(banner.getUrl())
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.nikosoft.ir"));
                            startActivity(browserIntent);
                        }
                    });
            slider.addSlider(sliderView);
        }

    }

    /**
     * show error on loading slider images
     *
     * @param error error message
     */
    @Override
    public void showError(String error) {
    }

    /**
     * load default slider images from resource
     *
     * @param localImages
     */
    @Override
    public void showDefaltSliderImages(List<Integer> localImages) {
        //مقدار دهی اسلایدر
        //از داخل پوشه drawable
        for (final int id : localImages) {
            SliderView sliderView1 = new SliderView(this);
            sliderView1
                    .image(id)
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {

                        }
                    });
            slider.addSlider(sliderView1);
        }

        slider.setPresetTransformer(SliderLayout.Transformer.ZoomOut);
        slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        slider.setCustomAnimation(new DescriptionAnimation());
        slider.setDuration(4000);
        slider.addOnPageChangeListener(new ViewPagerEx.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
        studentRepository.close();
        fieldRepository.close();
        if (iabHelper != null) iabHelper.dispose();
        iabHelper = null;
        releaseService();
    }

    /**
     * show target view in first app run
     *
     * @param view     view
     * @param activity activity
     * @param title    title
     * @param text     text
     */
    public void startShowCaseView(View view, AppCompatActivity activity, String title, String text, TapTargetView.Listener listener) {
        if (utility.retrievePref(utility.FIRST_RUN, "-1", getViewContext()).equals("1")) {

            TapTargetView.showFor(activity, TapTarget.forView(view, title, text)
                    .transparentTarget(true)
                    .targetRadius(70)
                    .tintTarget(false), listener);


        }
    }


    class UpdateServiceConnection implements ServiceConnection {
        public void onServiceConnected(ComponentName name, IBinder boundService) {
            service = IUpdateCheckService.Stub
                    .asInterface((IBinder) boundService);
            try {
                long vCode = service.getVersionCode(getPackageName());
                Log.i("VERSION","new version code: "+vCode);
                utility.savePref(utility.NEW_VERSION, String.valueOf(vCode), getViewContext());
                Log.i("VERSION","app version code: "+utility.getAppVersionNumber());
                if (vCode > utility.getAppVersionNumber())
                    showUpdateDialog();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d(TAG, "onServiceConnected(): Connected");
        }

        public void onServiceDisconnected(ComponentName name) {
            service = null;
            Log.d(TAG, "onServiceDisconnected(): Disconnected");
        }
    }

    private void showUpdateDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getViewContext());
        builder.setTitle(getViewContext().getString(R.string.update_app));
        builder.setMessage(getViewContext().getString(R.string.please_update_app));
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.update_app, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("bazaar://details?id=" + getPackageName()));
                    intent.setPackage("com.farsitel.bazaar");
                    startActivity(intent);
                    utility.closeApp();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getViewContext(), getString(R.string.bazar_is_not_installed), Toast.LENGTH_SHORT).show();
                    utility.closeApp();
                }
            }
        });
        builder.setNegativeButton(R.string.exit, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                utility.closeApp();
            }
        });

        builder.show();
    }


    private void initService() {
        Log.i(TAG, "initService()");
        connection = new UpdateServiceConnection();

        Intent i = new Intent(
                "com.farsitel.bazaar.service.UpdateCheckService.BIND");
        i.setPackage("com.farsitel.bazaar");
        boolean ret = bindService(i, connection, Context.BIND_AUTO_CREATE);
        Log.d(TAG, "initService() bound value: " + ret);
    }

    /**
     * This is our function to un-binds this activity from our service.
     */
    private void releaseService() {
        if (connection != null)
            unbindService(connection);
        connection = null;
        Log.d(TAG, "releaseService(): unbound.");
    }
}
