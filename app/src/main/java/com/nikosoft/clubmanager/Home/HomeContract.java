package com.nikosoft.clubmanager.Home;

import com.nikosoft.clubmanager.Base.BasePresenter;
import com.nikosoft.clubmanager.Base.BaseView;
import com.nikosoft.clubmanager.Models.Banner;

import java.util.List;

/**
 * Created by Yosef on 08/02/2019.
 */

public interface HomeContract {
    interface View extends BaseView{
        void showSlider(List<Banner> images);
        void showError(String error);
        void showDefaltSliderImages(List<Integer> localImages);

    }

    interface Presenter extends BasePresenter<View>{
        void getSliderImages();
        void getDefaltSliderImages();

    }
}
