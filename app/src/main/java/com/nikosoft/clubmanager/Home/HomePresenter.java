package com.nikosoft.clubmanager.Home;

import com.nikosoft.clubmanager.Data.DataSource;
import com.nikosoft.clubmanager.Models.Banner;
import com.nikosoft.clubmanager.Utiles.Utility;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Yosef on 08/02/2019.
 */

public class HomePresenter implements HomeContract.Presenter {

    private HomeContract.View view;
    private DataSource dataSource;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    Utility utility=new Utility();

    @Inject
    public HomePresenter(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void attachView(HomeContract.View view) {
        this.view = view;
        if (utility.isConnectedInternet(view.getViewContext())) {
            getSliderImages();
        }
        getDefaltSliderImages();
    }

    @Override
    public void detachView() {
        this.view = null;
        if (compositeDisposable != null & compositeDisposable.size() > 0) {
            compositeDisposable.clear();
        }
    }

    @Override
    public void getSliderImages() {
        dataSource.getSliderImages().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<Banner>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(List<Banner> sliderImages) {
                        view.showSlider(sliderImages);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showError(e.toString());
                    }
                });
    }

    @Override
    public void getDefaltSliderImages() {

        view.showDefaltSliderImages(dataSource.getDefaltSliderImages());
    }


}
