package com.nikosoft.clubmanager.Insurance;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.nikosoft.clubmanager.Adapters.Insurance_Adapter;
import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.Data.DBRepository.InsuranceRepository;
import com.nikosoft.clubmanager.Data.Model.Insurance;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.Utiles.Utility;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InsuranceActivity extends AppCompatActivity implements InsuranceContract.View,OnInsuranceEditMenuClickListener {

    @BindView(R.id.txt_insurance_cost)
    EditText txt_cost;
    @BindView(R.id.txt_insurance_title)
    EditText txt_title;
    @BindView(R.id.lay_add_insurance)
    MaterialRippleLayout add_insurance;
    @BindView(R.id.recy_insurances)
    RecyclerView recycler_insurance;

    InsuranceContract.Presenter presenter;
    @Inject
    Utility utility;
    @Inject
    InsuranceRepository insuranceRepository;


    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;
    List<Insurance> insuranceList;
    public static int insuranceID = 0;
    public boolean EDITMODE=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //dependency injection by dagger2
        ClubManager.getApplicationComponent().inject(this);
        //set app theme
        AppCompatDelegate.setDefaultNightMode(utility.getThemeNight());

        setContentView(R.layout.activity_insurance);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        presenter.attachView(this);

        setupViews();

    }

    /**
     * setup activitys views
     */
    private void setupViews() {

        txt_cost.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                utility.handleNumberInput(txt_cost,this);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        //fields recycler
        layoutManager = new LinearLayoutManager(getViewContext());

        recycler_insurance.setLayoutManager(layoutManager);

        adapter = new Insurance_Adapter(getViewContext(), insuranceList,this::onEditMenuClick);

        recycler_insurance.setAdapter(adapter);




        //add or update field
        add_insurance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //check empty title
                if (utility.isEmpty(txt_title)) {
                    utility.setError(txt_title, getString(R.string.emtpy_value_error));
                    return;
                }

                //check exist title
                if(!EDITMODE) {
                    for (Insurance item : insuranceList) {
                        if (item.getTitle().equals(txt_title.getText().toString())) {
                            utility.setError(txt_title, getString(R.string.exist_value_error));
                            return;
                        }
                    }
                }
                //check empty cost
                if (utility.isEmpty(txt_cost)) {
                    utility.setError(txt_cost, getString(R.string.emtpy_value_error));
                    return;
                }


                insuranceRepository.save(new Insurance(
                        insuranceID,
                        Integer.parseInt(utility.removeSpaces(txt_cost.getText().toString())),
                        txt_title.getText().toString()));

                adapter.notifyDataSetChanged();
                txt_cost.setText("");
                txt_title.setText("");
                txt_title.requestFocus();
                insuranceID = 0;
                EDITMODE=false;

            }
        });

    }





    @Inject
    public void setPresenter(InsurancePresenter presenter)
    {
        this.presenter=presenter;
    }


    @Override
    public void onEditMenuClick(Insurance insurance) {
        txt_title.setText(insurance.getTitle());
        txt_cost.setText(insurance.getCost()+"");
        EDITMODE = true;
        insuranceID = insurance.getId();
    }

    @Override
    public void insuranceList(List<Insurance> insuranceList) {
        this.insuranceList=insuranceList;
    }

    @Override
    public Context getViewContext() {
        return this;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }
}
