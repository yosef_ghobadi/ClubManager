package com.nikosoft.clubmanager.Insurance;

import com.nikosoft.clubmanager.Base.BasePresenter;
import com.nikosoft.clubmanager.Base.BaseView;
import com.nikosoft.clubmanager.Data.Model.Insurance;

import java.util.List;

public interface InsuranceContract {

    public interface View extends BaseView{
        void insuranceList(List<Insurance> insuranceList);
    }

    public interface Presenter extends BasePresenter<View>{

        void getInsuranceList();
    }
}
