package com.nikosoft.clubmanager.Insurance;

import com.nikosoft.clubmanager.Data.DataSource;

import javax.inject.Inject;

public class InsurancePresenter implements InsuranceContract.Presenter {

    private InsuranceContract.View view;
    private DataSource dataSource;

    @Inject
    public InsurancePresenter(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void getInsuranceList() {
        view.insuranceList(dataSource.getInsurances());
    }

    @Override
    public void attachView(InsuranceContract.View view) {
        this.view = view;
        getInsuranceList();
    }

    @Override
    public void detachView() {
        this.view = null;
    }
}
