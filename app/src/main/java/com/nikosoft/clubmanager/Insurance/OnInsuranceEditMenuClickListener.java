package com.nikosoft.clubmanager.Insurance;

import com.nikosoft.clubmanager.Data.Model.Insurance;

public interface OnInsuranceEditMenuClickListener {
    void onEditMenuClick(Insurance insurance);

}
