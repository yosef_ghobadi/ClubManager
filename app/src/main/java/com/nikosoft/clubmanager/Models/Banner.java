package com.nikosoft.clubmanager.Models;

/**
 * Created by Yosef on 10/02/2019.
 */

public class Banner  {
    private int id;
    private String name;
    private String url;
    private String link;

    public Banner() {
    }

    public Banner(int id, String name, String url, String link) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.link = link;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
