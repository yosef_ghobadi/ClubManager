package com.nikosoft.clubmanager.MyAccount;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.balysv.materialripple.MaterialRippleLayout;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.Data.DBRepository.StudentRepository;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.Utiles.Utility;
import com.nikosoft.clubmanager.Utiles.util.IabHelper;
import com.nikosoft.clubmanager.Utiles.util.IabResult;
import com.nikosoft.clubmanager.Utiles.util.Inventory;
import com.nikosoft.clubmanager.Utiles.util.Purchase;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyAccountActivity extends AppCompatActivity implements MyAccountContract.View {

    private static final String TAG = "BAZAR";
    @BindView(R.id.toolbar_account)
    Toolbar toolbar;
    @BindView(R.id.txt_account_phone)
    MaterialEditText txt_phone;
    @BindView(R.id.lay_click_expan_account_phone)
    LinearLayout btn_expand_phone;
    @BindView(R.id.img_expan_account_phone_arrow)
    ImageView expand_phone_img;
    @BindView(R.id.lay_expan_account_phone)
    ExpandableLinearLayout expandableLayout;
    @BindView(R.id.lay_register_phone)
    MaterialRippleLayout btn_register_phone;
    @BindView(R.id.txt_account_monthly_subc_price)
    TextView monthly_subsc_price;
    @BindView(R.id.txt_account_yearly_subs_price)
    TextView yearly_subsc_price;
    @BindView(R.id.txt_account_validity)
    TextView txt_validity;
    @BindView(R.id.lay_ripple_annual_subs)
    MaterialRippleLayout yearly_subsc;
    @BindView(R.id.lay_ripple_monthly_subs)
    MaterialRippleLayout monthly_subsc;
    @BindView(R.id.discounted_price)
    TextView txt_discount;

    @BindView(R.id.account_collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;

    MyAccountContract.Presenter presenter;

    IabHelper iabHelper;


    ProgressDialog progressDialog;

    @Inject
    Utility utility;
    @Inject
    StudentRepository studentRepository;
    private String phoneNumber;
    private boolean BAZAR_IS_READY = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ClubManager.getApplicationComponent().inject(this);

        AppCompatDelegate.setDefaultNightMode(utility.getThemeNight());

        setContentView(R.layout.activity_my_account);


        ButterKnife.bind(this);
        presenter.attachView(this);

        setupViews();


        //نمایش دیالوگ برای اتصال به بازار

        showProgress();
        prepareBazarWithHelper();


    }

    private void showProgress() {
        progressDialog = new ProgressDialog(getViewContext());
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.connectiong));
        progressDialog.show();
    }


    private void prepareBazarWithHelper() {
        //bazar buy helper
        if (utility.isConnectedInternet(getViewContext())) {
            try {


                //اتصال به بازار با IabHelper
                iabHelper = new IabHelper(getViewContext(), utility.getRSAKey(getViewContext()));
                Log.d(TAG, "Starting setup.");


                iabHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
                    @Override
                    public void onIabSetupFinished(IabResult result) {


                        if (!result.isSuccess()) {
                            Toast.makeText(getViewContext(), result.getMessage() + "خطا در بر قراری ارتباط \nوارد حساب خود در کافه بازار شوید", Toast.LENGTH_LONG).show();
                            progressDialog.dismiss();
                        } else {
                            Log.d(TAG, "Query inventory start.");
                            BAZAR_IS_READY = true;

                            //لیست محصولات قابل خریداری
                            ArrayList skuList = new ArrayList();
                            skuList.add(utility.YEARLY_SUB);
                            skuList.add(utility.MONTHLY_SUB);

                            //ارسال درخواست برای دریافت محصولات
                            iabHelper.queryInventoryAsync(true, skuList, new IabHelper.QueryInventoryFinishedListener() {
                                @Override
                                public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
                                    Log.d(TAG, "Query inventory finished.");
                                    if (result.isFailure()) {
                                        Toast.makeText(getViewContext(), getString(R.string.please_login), Toast.LENGTH_SHORT).show();

                                        //صفحه ورود به بازار
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent.setData(Uri.parse("bazaar://login"));
                                        intent.setPackage("com.farsitel.bazaar");
                                        startActivity(intent);


                                        Log.d(TAG, "Failed to query inventory: " + result);
                                        if (progressDialog.isShowing())
                                            progressDialog.dismiss();
                                        return;
                                    } else {
                                        Log.d(TAG, "Query inventory was successful.");

                                        long time = 0;

                                        //-----------------------------------------دریافت اشتراک خریده شده توسط کاربر و اعمال آن
                                        if (inventory.hasPurchase(utility.MONTHLY_SUB)) {
                                            Log.d(TAG, "اشتراک ماهانه" + inventory.getPurchase(utility.MONTHLY_SUB).getOriginalJson());
                                            //get buy date as milli second
                                            time = inventory.getPurchase(utility.MONTHLY_SUB).getPurchaseTime();
                                            //convert buy date persian date and update ui
                                            txt_validity.setText(getString(R.string.subs_expire_date) + utility.farsinumber(getPersianDate(time, 1)));
                                            //update internet connection limit
                                            utility.setAppValidity(true, getViewContext());
                                        } else if (inventory.hasPurchase(utility.YEARLY_SUB)) {
                                            Log.d(TAG, "اشتراک سالانه" + inventory.getPurchase(utility.YEARLY_SUB).getOriginalJson());
                                            //get buy date as milli second
                                            time = inventory.getPurchase(utility.YEARLY_SUB).getPurchaseTime();
                                            //convert buy date persian date and update ui
                                            txt_validity.setText(getString(R.string.subs_expire_date) + utility.farsinumber(getPersianDate(time, 12)));
                                            //update internet connection limit

                                            utility.setAppValidity(true, getViewContext());
                                        } else {
                                            txt_validity.setText(R.string.no_subscription);
                                            utility.setAppValidity(false, getViewContext());
                                        }

                                        //get subscriptions prices and set in ui
                                        String yearlyPrice = convertRialToToman(inventory.getSkuDetails(utility.YEARLY_SUB).getPrice(), true);
                                        String monthlyPrice = convertRialToToman(inventory.getSkuDetails(utility.MONTHLY_SUB).getPrice(), true);
                                        monthly_subsc_price.setText(monthlyPrice);
                                        yearly_subsc_price.setText(yearlyPrice);

                                        int discount = Integer.parseInt(convertRialToToman(inventory.getSkuDetails(utility.YEARLY_SUB).getPrice(), false)) * 2;
                                        txt_discount.setText(utility.farsinumber(utility.threeDigit(String.valueOf(discount), ',')) + getString(R.string.toman));

                                        //update internet unconnection limit
                                        utility.savePref(utility.OFFLINE_LIMIT, String.valueOf(utility.OFFLINE_LIMIT_COUNT), getViewContext());
                                        progressDialog.dismiss();
                                    }

                                    Log.d(TAG, "Initial inventory query finished; enabling main UI.");
                                    if (progressDialog.isShowing())
                                        progressDialog.dismiss();
                                }

                            });


                        }

                    }
                });


            } catch (Exception ex) {
                Toast.makeText(getViewContext(), "برنامه بازار روی تلفن شما نصب نیست\nاگر این برنامه را از کافه بازار دریافت نکرده اید ،برنامه را از بازار دریافت کنید", Toast.LENGTH_LONG).show();
                ex.printStackTrace();
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        } else {
            Toast.makeText(getViewContext(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
            BAZAR_IS_READY = false;
            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }
    }


    @Inject
    public void setPresenter(MyAccountPresenter presenter) {
        this.presenter = presenter;
    }

    private void setupViews() {

        //set title
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.title_activity_my_account));

        collapsingToolbarLayout.setExpandedTitleTextColor(Objects.requireNonNull(ContextCompat.getColorStateList(getViewContext(), android.R.color.transparent)));

        txt_validity.setText(R.string.no_subscription);
        txt_validity.requestFocus();
        //get saved phone number to edittext-----------------------------------------
        txt_phone.setText(phoneNumber);


        //set expandable layout----------------------------------------------
        btn_expand_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expandableLayout.toggle();
                if (expandableLayout.isExpanded())
                    expand_phone_img.setImageResource(R.drawable.ic_arrow_down);
                else
                    expand_phone_img.setImageResource(R.drawable.ic_arrow_up);
                expandableLayout.initLayout();
            }
        });


        //save phone number----------------------------------------------------
        btn_register_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txt_phone.getText().length() == 11) {
                    utility.savePref(utility.PHONE_NUMBER, txt_phone.getText().toString(), getViewContext());
                    Toast.makeText(getViewContext(), getString(R.string.save_successfull), Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(getViewContext(), getString(R.string.phone_digit_length_error), Toast.LENGTH_LONG).show();
            }
        });


        //go to buy subscription------------------------------------------------
        if (BAZAR_IS_READY) {

            //add subscriptions SKU to list
            List subscriptions = new ArrayList();
            subscriptions.add(utility.MONTHLY_SUB);
            subscriptions.add(utility.YEARLY_SUB);

            //get subscription details
            iabHelper.queryInventoryAsync(true, subscriptions, new IabHelper.QueryInventoryFinishedListener() {
                @Override
                public void onQueryInventoryFinished(IabResult result, Inventory inv) {

                    if (result.isFailure()) {
                        Toast.makeText(getViewContext(), getString(R.string.error_get_subscription_bazar), Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        String monthly_price = inv.getSkuDetails(utility.MONTHLY_SUB).getPrice();
                        String yearly_price = inv.getSkuDetails(utility.YEARLY_SUB).getPrice();

                        Log.i(TAG, yearly_price);
                        monthly_subsc_price.setText(monthly_price + getString(R.string.toman));
                        yearly_subsc_price.setText(yearly_price + getString(R.string.toman));


                    }
                }
            });
        }


        //monthly subscription
        monthly_subsc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (BAZAR_IS_READY) {
                    if (utility.retrievePref(utility.IS_VALID_APP, "-1", getViewContext()).equals("0")) {
                        if (iabHelper != null)
                            iabHelper.flagEndAsync();
                        iabHelper.launchPurchaseFlow(MyAccountActivity.this, utility.MONTHLY_SUB, 1, new IabHelper.OnIabPurchaseFinishedListener() {
                            @Override
                            public void onIabPurchaseFinished(IabResult result, Purchase info) {
                                if (result.isFailure()) {
                                    Toast.makeText(getViewContext(), getString(R.string.paymentـerror), Toast.LENGTH_LONG).show();
                                    return;
                                }
                                if (info.getSku().equals(utility.MONTHLY_SUB) & info.getDeveloperPayload().equals(utility.retrievePref(utility.PHONE_NUMBER, "", getViewContext()))) {
                                    Toast.makeText(getViewContext(), getString(R.string.monthly_subscription_successful_payment), Toast.LENGTH_LONG).show();
                                    txt_validity.setText(getPersianDate(info.getPurchaseTime(), 1));
                                    utility.setAppValidity(true, getViewContext());

                                } else
                                    Toast.makeText(getViewContext(), getString(R.string.invalid_payment), Toast.LENGTH_LONG).show();
                            }
                        }, utility.retrievePref(utility.PHONE_NUMBER, "", getViewContext()));
                    } else
                        Toast.makeText(getViewContext(), getString(R.string.valid_subscription), Toast.LENGTH_LONG).show();

                } else
                    Toast.makeText(getViewContext(), getString(R.string.error_get_subscription_bazar), Toast.LENGTH_SHORT).show();
            }
        });


        //yearly subscription
        yearly_subsc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (BAZAR_IS_READY) {
                    if (utility.retrievePref(utility.IS_VALID_APP, "-1", getViewContext()).equals("0")) {
                        if (iabHelper != null)
                            iabHelper.flagEndAsync();
                        iabHelper.launchPurchaseFlow(MyAccountActivity.this, utility.YEARLY_SUB, 1, new IabHelper.OnIabPurchaseFinishedListener() {
                            @Override
                            public void onIabPurchaseFinished(IabResult result, Purchase info) {
                                if (result.isFailure()) {
                                    Toast.makeText(getViewContext(), getString(R.string.paymentـerror), Toast.LENGTH_LONG).show();
                                    return;
                                }
                                if (info.getSku().equals(utility.YEARLY_SUB) & info.getDeveloperPayload().equals(utility.retrievePref(utility.PHONE_NUMBER, "", getViewContext()))) {
                                    Toast.makeText(getViewContext(), getString(R.string.annual_subscription_successful_payment), Toast.LENGTH_LONG).show();
                                    txt_validity.setText(getPersianDate(info.getPurchaseTime(), 12));
                                    utility.setAppValidity(true, getViewContext());

                                } else
                                    Toast.makeText(getViewContext(), getString(R.string.invalid_payment), Toast.LENGTH_LONG).show();
                            }
                        }, utility.retrievePref(utility.PHONE_NUMBER, "", getViewContext()));
                    } else
                        Toast.makeText(getViewContext(), getString(R.string.valid_subscription), Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(getViewContext(), getString(R.string.error_get_subscription_bazar), Toast.LENGTH_LONG).show();
            }
        });

    }


    /**
     * back arrow action
     *
     * @return
     */
    @Override
    public boolean onSupportNavigateUp() {
        Toast.makeText(this, "pressed", Toast.LENGTH_SHORT).show();
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        if (utility.retrievePref(utility.IS_VALID_APP, "-1", getViewContext()).equals("0"))
            //اگر تعداد دانش آموزان بیشتر از تعداد محدود باشد
            if (studentRepository.getAll().size() > utility.LIMIT_COUNT)
                Toast.makeText(getViewContext(), getString(R.string.subs_expired), Toast.LENGTH_SHORT).show();

            else
                super.onBackPressed();
        else
            super.onBackPressed();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        /*if (requestCode == 1001) {
            int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
            String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
            String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");

            if (resultCode == RESULT_OK) {
                try {
                    JSONObject jo = new JSONObject(purchaseData);
                    String sku = jo.getString("productId");
                    alert("You have bought the " + sku + ". Excellent choice, adventurer!");
                }
                catch (JSONException e) {
                    alert("Failed to parse purchase data.");
                    e.printStackTrace();
                }
            }
        }*/
        super.onActivityResult(requestCode, resultCode, data);

    }

    /**
     * get date from millisecond
     *
     * @param millisecond millisecond from 1/1/1970
     * @return return date
     */
    public String getPersianDate(long millisecond, int month) {
        Date date = new Date(Long.parseLong(String.valueOf(millisecond)));

        return utility.getPersianDateFromMiladiDate(date, month);
    }

    public String convertRialToToman(String priceRial, boolean formated) {
        String toman = "";
        if (priceRial.contains("صفر ریال")) {
            if (formated)
                return "صفر تومان";
            else
                return "0";
        } else {
            ArrayList<String> digits = new ArrayList<>(Arrays.asList("۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹", "۰"));

            for (int i = 0; i < priceRial.length(); i++) {
                Log.i(TAG, "i= " + i + "\n cahr= " + priceRial.substring(i, i + 1) + "\n length= " + priceRial.length());
                if (digits.contains(priceRial.substring(i, i + 1))) {
                    toman += priceRial.substring(i, i + 1);
                }
            }
        }
        toman = toman.substring(0, toman.length() - 1);
        if (formated)
            return utility.farsinumber(utility.threeDigit(toman, ',')) + getString(R.string.toman);
        else
            return toman;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_my_account, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.my_account_reconnect) {
            showProgress();
            prepareBazarWithHelper();
        } else if (item.getItemId() == android.R.id.home)
            onBackPressed();
        return true;
    }

    @Override
    public void userPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public Context getViewContext() {
        return this;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
        if (iabHelper != null) iabHelper.dispose();
        iabHelper = null;
        studentRepository.close();
    }


}
