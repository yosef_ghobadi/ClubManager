package com.nikosoft.clubmanager.MyAccount;

import com.nikosoft.clubmanager.Base.BasePresenter;
import com.nikosoft.clubmanager.Base.BaseView;

public interface MyAccountContract {

    public interface View extends BaseView{
        void userPhoneNumber(String phoneNumber);
    }

    public interface Presenter extends BasePresenter<View>{
        void getUserPhoneNumber();
    }
}
