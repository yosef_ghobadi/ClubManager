package com.nikosoft.clubmanager.MyAccount;

import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.Data.DataSource;
import com.nikosoft.clubmanager.Utiles.Utility;

import javax.inject.Inject;

public class MyAccountPresenter implements MyAccountContract.Presenter {


    MyAccountContract.View view;
    DataSource dataSource;

    @Inject
    Utility utility;

    @Inject
    public MyAccountPresenter(DataSource dataSource) {
        this.dataSource = dataSource;
        ClubManager.getApplicationComponent().inject(this);
    }

    @Override
    public void getUserPhoneNumber() {
        view.userPhoneNumber(utility.retrievePref(utility.PHONE_NUMBER,"",utility.context));
    }

    @Override
    public void attachView(MyAccountContract.View view) {

        this.view=view;
        getUserPhoneNumber();
    }

    @Override
    public void detachView() {
        this.view=null;
    }
}
