package com.nikosoft.clubmanager.SMS;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nikosoft.clubmanager.Adapters.SMS_Adapter;
import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.Data.DBRepository.SMSRepository;
import com.nikosoft.clubmanager.Data.DBRepository.StudentRepository;
import com.nikosoft.clubmanager.Data.Model.SMS;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.Utiles.Utility;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SMSActivity extends AppCompatActivity implements SMSContract.View, SMS_Adapter.ClickAdapterListener {


    @BindView(R.id.sms_toolbar)
    Toolbar toolbar;
    @BindView(R.id.sms_recycler)
    RecyclerView recyclerView;


    @Inject
    Utility utility;
    @Inject
    SMSRepository smsRepository;
    @Inject
    StudentRepository studentRepository;

    SMSContract.Presenter presenter;

    private SMSActivity.ActionModeCallback actionModeCallback;
    private ActionMode actionMode;
    //detect when long press is enabled
    private boolean isLongselectionEnabled = false;
    private SMS_Adapter sms_adapter;
    private AlertDialog.Builder alert_builder;
    private List<SMS> smsList=new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ClubManager.getApplicationComponent().inject(this);

        AppCompatDelegate.setDefaultNightMode(utility.getThemeNight());

        setContentView(R.layout.activity_sms);

        ButterKnife.bind(this);

        presenter.attachView(this);

        setupViews();

        setSupportActionBar(toolbar);

        //setTempData();

    }

    private void setTempData() {
       /* if (smsRepository.getAll().size() == 0) {
            smsRepository.save(new SMS(
                    0
                    , "با سلام لطفا جلسه بعد شهریه خود را پرداخت نمایید.با تشکر مدیریت باشگاه فجر"
                    , 13980425
                    , true
                    , studentRepository.get(1)
            ));
            smsRepository.save(new SMS(
                    0
                    , "با سلام لطفا جلسه بعد شهریه خود را پرداخت نمایید.با تشکر مدیریت باشگاه فجر"
                    , 13980425
                    , false
                    , studentRepository.get(1)
            ));
            smsRepository.save(new SMS(
                    0
                    , "با سلام لطفا جلسه بعد شهریه خود را پرداخت نمایید.با تشکر مدیریت باشگاه فجر"
                    , 13980425
                    , true
                    , studentRepository.get(1)
            ));
            smsRepository.save(new SMS(
                    0
                    , "با سلام لطفا جلسه بعد شهریه خود را پرداخت نمایید.با تشکر مدیریت باشگاه فجر"
                    , 13980425
                    , true
                    , studentRepository.get(1)
            ));
            smsRepository.save(new SMS(
                    0
                    , "با سلام لطفا جلسه بعد شهریه خود را پرداخت نمایید.با تشکر مدیریت باشگاه فجر"
                    , 13980425
                    , false
                    , studentRepository.get(1)
            ));

        }*/
    }

    private void setupViews() {
        alert_builder = new AlertDialog.Builder(this);

        sms_adapter = new SMS_Adapter(smsList, getViewContext(), this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getViewContext()));
        recyclerView.setAdapter(sms_adapter);
        actionModeCallback = new ActionModeCallback();
    }


    /**
     * inject coach presenter
     *
     * @param presenter
     */
    @Inject
    public void setPresenter(SMSPresenter presenter) {
        this.presenter = presenter;
    }


    @Override
    public void smsList(List<SMS> smsList) {
        this.smsList=smsList;
        sms_adapter = new SMS_Adapter(smsList, getViewContext(), this);
        sms_adapter.notifyDataSetChanged();
    }

    @Override
    public Context getViewContext() {
        return this;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        sms_adapter.notifyDataSetChanged();
    }

    @Override
    public void onRowClicked(int position) {
        if (isLongselectionEnabled)
            enableActionMode(position);
        /*else
            sms_adapter.selectItem(position);*/
    }

    @Override
    public void onRowLongClicked(int position) {
        isLongselectionEnabled = true;
        enableActionMode(position);
    }

    private void enableActionMode(int position) {
        if (actionMode == null) {
            actionMode = startSupportActionMode(actionModeCallback);
        }
        toggleSelection(position);
    }

    private void toggleSelection(int position) {
        sms_adapter.toggleSelection(position);
        int count = sms_adapter.getSelectedItemCount();

        if (count == 0) {
            actionMode.finish();
            actionMode = null;
            isLongselectionEnabled = false;
        } else {
            actionMode.setTitle(String.valueOf(count));
            actionMode.invalidate();
        }
    }

    private void selectAll() {
        try {
            sms_adapter.selectAll();
            int count = sms_adapter.getSelectedItemCount();

            if (count == 0) {
                actionMode.finish();
            } else {
                actionMode.setTitle(String.valueOf(count));
                actionMode.invalidate();
            }

            actionMode = null;
        } catch (Exception ex) {
        }
    }

    private void deleteRows() {
        List selectedItemPositions =
                sms_adapter.getSelectedItems();
        for (int i = selectedItemPositions.size() - 1; i >= 0; i--) {
            sms_adapter.removeData((Integer) selectedItemPositions.get(i));
        }
        isLongselectionEnabled = false;
        sms_adapter.notifyDataSetChanged();


        actionMode = null;
    }

    private class ActionModeCallback implements ActionMode.Callback {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.menu_sms_list_option, menu);

            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            //Log.d("API123", "here");
            switch (item.getItemId()) {


                case R.id.coach_menu_delete:
                    // delete all the selected rows
                    alert_builder.setTitle(getViewContext().getString(R.string.warning));
                    alert_builder.setMessage(getViewContext().getString(R.string.are_you_sure));
                    alert_builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            deleteRows();
                            mode.finish();
                        }
                    });
                    alert_builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });
                    alert_builder.show();

                    return true;


                case R.id.coach_menu_select_all:
                    selectAll();
                    return true;


                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            sms_adapter.clearSelections();
            actionMode = null;
            isLongselectionEnabled = false;
        }
    }

/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_student,menu);

        MenuItem search= menu.findItem(R.id.search);
        SearchView searchView= (SearchView) MenuItemCompat.getActionView(search);

        int searchIconId = searchView.getContext().getResources().getIdentifier("android:id/search_button",null, null);
        ImageView searchIcon = (ImageView) searchView.findViewById(searchIconId);
        searchIcon.setImageResource(R.drawable.ic_search_white);

        search(searchView);
        return super.onCreateOptionsMenu(menu);
    }*/

   /* private void search(SearchView searchView) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                sms_adapter.getFilter().filter(newText);
                return true;
            }
        });
    }*/

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case R.id.order_all:
                ordered_studentList=getStudents(studentList,0);
                toolbar.setTitle(getString(R.string.title_activity_student));
                break;

            case R.id.order_exp_tuition:
                ordered_studentList=getStudents(studentList,1);
                toolbar.setTitle(getString(R.string.out_of_date_tuitions));
                break;

            case R.id.order_exp_insurance:
                ordered_studentList=getStudents(studentList,2);
                toolbar.setTitle(getString(R.string.out_of_date_insurance));
                break;

        }
        sms_adapter=new sms_adapter(ordered_studentList,getViewContext(),this);
        recyclerView.setAdapter(sms_adapter);
        sms_adapter.notifyDataSetChanged();
        return super.onOptionsItemSelected(item);
    }*/
}
