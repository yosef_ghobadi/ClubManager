package com.nikosoft.clubmanager.SMS;

import com.nikosoft.clubmanager.Base.BasePresenter;
import com.nikosoft.clubmanager.Base.BaseView;
import com.nikosoft.clubmanager.Data.Model.SMS;

import java.util.List;

public interface SMSContract {

    public interface View extends BaseView{
        void smsList(List<SMS> smsList);
    }

    public interface Presenter extends BasePresenter<View>{
        void getSmsList();
    }
}
