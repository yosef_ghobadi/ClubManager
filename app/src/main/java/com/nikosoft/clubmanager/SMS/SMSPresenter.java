package com.nikosoft.clubmanager.SMS;

import com.nikosoft.clubmanager.Data.DataSource;

import javax.inject.Inject;

public class SMSPresenter implements SMSContract.Presenter {

    SMSContract.View view;
    DataSource dataSource;

    @Inject
    public SMSPresenter(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void getSmsList() {
        view.smsList(dataSource.getSMSList());
    }

    @Override
    public void attachView(SMSContract.View view) {
        this.view=view;
        getSmsList();
    }

    @Override
    public void detachView() {
        this.view=null;
    }
}
