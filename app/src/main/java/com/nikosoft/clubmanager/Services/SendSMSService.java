package com.nikosoft.clubmanager.Services;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;
import android.telephony.SmsManager;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.Data.DBRepository.InsurancePaymentRepository;
import com.nikosoft.clubmanager.Data.DBRepository.SMSRepository;
import com.nikosoft.clubmanager.Data.DBRepository.StudentRepository;
import com.nikosoft.clubmanager.Data.DBRepository.TuitionPaymentRepository;
import com.nikosoft.clubmanager.Data.Model.InsurancePayment;
import com.nikosoft.clubmanager.Data.Model.SMS;
import com.nikosoft.clubmanager.Data.Model.Student;
import com.nikosoft.clubmanager.Data.Model.TuitionPayment;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.Utiles.Utility;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import saman.zamani.persiandate.PersianDate;

public class SendSMSService extends Service {

    private final IBinder mBinder = new MyBinder();
    public static final String ORDER = "order";
    private SMS sms=new SMS();

    @Inject
    Utility utility;
    @Inject
    StudentRepository studentRepository;
    @Inject
    TuitionPaymentRepository tuitionPaymentRepository;
    @Inject
    InsurancePaymentRepository insurancePaymentRepository;
    @Inject
    SMSRepository smsRepository;

    @Override
    public void onCreate() {
        super.onCreate();
        ClubManager.getApplicationComponent().inject(this);
        Log.i("Service Started","");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String phone = utility.retrievePref(utility.PHONE_NUMBER,"", this);
        String message=utility.retrievePref(utility.DEFAULT_MESSAGE,"",this);
        int smsId=-1;

        int order = -1;
        if (intent != null) {
            order = intent.getIntExtra(ORDER, -1);
        }

        List<Student> students=getStudents(order);

        NotificationManager mNotificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

        final NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_account_circle)
                .setContentTitle(this.getString(R.string.sending))
                .setTicker(this.getString(R.string.sending))
                .setContentText(this.getString(R.string.sending)+utility.farsinumber(0+"")+this.getString(R.string.of)+students.size())
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setProgress(students.size(),0,false)
                .setOngoing(true)
                .setOnlyAlertOnce(true)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        //Notification notification = notification.build();
        //notification.flags = Notification.FLAG_NO_CLEAR|Notification.FLAG_ONGOING_EVENT;
        mNotificationManager.notify(1, notification.build());


        if (order != -1) {

            for (int i=0;i< students.size();i++) {

                smsId=smsRepository.getId();
                //Toast.makeText(this, "smsId ="+smsId, Toast.LENGTH_SHORT).show();
                smsRepository.save(new SMS(smsId,message , utility.getLongFromPersianDate(PersianDate.today()), 0, students.get(i).getId()));

                sendSMS(phone,message, students.get(i).getPhone(),smsId);
                //Toast.makeText(this, i+"", Toast.LENGTH_SHORT).show();

                notification.setProgress(students.size(),i,false)
                        .setContentTitle(this.getString(R.string.sending)+utility.farsinumber(i+"")+" از "+utility.farsinumber(students.size()+""));
                mNotificationManager.notify(1, notification.build());

            }
            notification.setContentTitle(this.getString(R.string.sms_send_done))
                    .setContentText(utility.farsinumber(students.size()+" ")+this.getString(R.string.sms_sent))
                    .setOngoing(false)
                    .setProgress(0,0,false);
            mNotificationManager.notify(1, notification.build());
        }
        return Service.START_NOT_STICKY;

    }

    @Nullable
    @Override
    public IBinder onBind(Intent arg0) {
        return mBinder;
    }
    public class MyBinder extends Binder {
        SendSMSService getService() {
            return SendSMSService.this;
        }
    }

    /**
     * get students list based on order
     *
     * @param order 0=all 1=expired tuitions 2=expired insurance
     * @return list of selected students
     */
    public List<Student> getStudents(int order) {

        List<Student> temp_students = new ArrayList<>();
        List<Student> studentList = studentRepository.getAll();

        switch (order) {
            case 0:
                return studentList;

            case 1:
                for (Student student : studentList) {
                    TuitionPayment payment = tuitionPaymentRepository.getStudentLastPay(student.getId());
                    //اگر ماه شهریه کوچکتر از امروز باشد
                    if (utility.getPersianDate(payment.getDueDate()).compareTo(PersianDate.today()) == -1) {
                        if (utility.getPersianDate(payment.getPayDate()).addMonth(1).compareTo(PersianDate.today()) == -1) {
                            temp_students.add(student);
                        }
                    }
                }
                return temp_students;

            case 2:
                for (Student student : studentList) {
                    InsurancePayment payment = insurancePaymentRepository.getLastInsurance(student.getId());
                    //اگر ماه شهریه کوچکتر از امروز باشد
                    if (utility.getPersianDate(payment.getDate()).addYear(1).compareTo(PersianDate.today()) == -1) {
                        temp_students.add(student);
                    }
                }
                return temp_students;
        }
        return null;
    }


    /**
     * send sms
     *
     * @param phone_number scNumber
     * @param message      message
     * @return if sms not sent returns 0
     * sms sent return 1
     * sms delivered 2
     */
    public int sendSMS(String phone_number, String message, String dest_phone,int smsId) {
        try {
            String SENT = "SMS_SENT";
            String DELIVERED = "SMS_DELIVERED";

            PendingIntent sentPI = PendingIntent.getBroadcast(this, 0,
                    new Intent(SENT), 0);

            PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                    new Intent(DELIVERED), 0);

            //---when the SMS has been sent---
            registerReceiver(new BroadcastReceiver(){
                @Override
                public void onReceive(Context arg0, Intent arg1) {
                    switch (getResultCode())
                    {
                        case Activity.RESULT_OK:
                            sms=smsRepository.get(smsId);
                            sms.getRealm().executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    sms.setSent(1);
                                }
                            });

                            smsRepository.update(sms);
                            //Toast.makeText(SendSMSService.this, "status= "+sms_status, Toast.LENGTH_SHORT).show();
                            break;
                        case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                            break;
                        case SmsManager.RESULT_ERROR_NO_SERVICE:
                            break;
                        case SmsManager.RESULT_ERROR_NULL_PDU:
                            break;
                        case SmsManager.RESULT_ERROR_RADIO_OFF:
                            break;
                    }
                }
            }, new IntentFilter(SENT));

            //---when the SMS has been delivered---
            registerReceiver(new BroadcastReceiver(){
                @Override
                public void onReceive(Context arg0, Intent arg1) {
                    switch (getResultCode())
                    {
                        case Activity.RESULT_OK:
                            sms=smsRepository.get(smsId);
                            sms.getRealm().executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    sms.setSent(2);
                                }
                            });
                            smsRepository.update(sms);
                            break;
                        case Activity.RESULT_CANCELED:
                            sms=smsRepository.get(smsId);
                            sms.getRealm().executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    sms.setSent(1);
                                }
                            });
                            smsRepository.update(sms);
                            break;
                    }
                }
            }, new IntentFilter(DELIVERED));

            //for divide message to multi message if characters more than 160
            //ArrayList<String> messageList = SmsManager.getDefault().divideMessage(message);
            SmsManager smsManager = SmsManager.getDefault();

            smsManager.sendTextMessage(dest_phone, phone_number, message, sentPI, deliveredPI);
            //Toast.makeText(this, "status= "+sms_status, Toast.LENGTH_SHORT).show();
            return 0;


        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        smsRepository.close();
        insurancePaymentRepository.close();
        studentRepository.close();
        tuitionPaymentRepository.close();
    }
}
