package com.nikosoft.clubmanager.Session;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nikosoft.clubmanager.Adapters.Sessions_Adapter;
import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.Data.DBRepository.ClubRepository;
import com.nikosoft.clubmanager.Data.DBRepository.CoachRepository;
import com.nikosoft.clubmanager.Data.DBRepository.FieldRepository;
import com.nikosoft.clubmanager.Data.DBRepository.SessionRepository;
import com.nikosoft.clubmanager.Data.DBRepository.StudentRepository;
import com.nikosoft.clubmanager.Data.Model.Session;
import com.nikosoft.clubmanager.Data.Model.Student;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.SessionModify.SessionModifyActivity;
import com.nikosoft.clubmanager.Utiles.Utility;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmList;

public class SessionActivity extends AppCompatActivity implements SessionContract.View,Sessions_Adapter.ClickAdapterListener {

    @BindView(R.id.session_recycler)
    RecyclerView recyclerView;
    @BindView(R.id.fab_session)
    FloatingActionButton fab_addSession;
    Sessions_Adapter sessions_adapter;
    SessionContract.Presenter presenter;

    private SessionActivity.ActionModeCallback actionModeCallback;
    private ActionMode actionMode;
    private AlertDialog.Builder alert_builder;
    //detect when long press is enabled
    private boolean isLongselectionEnabled = false;

    @Inject
    Utility utility;
    @Inject
    SessionRepository sessionRepository;
    @Inject
    ClubRepository clubRepository;
    @Inject
    FieldRepository fieldRepository;
    @Inject
    CoachRepository coachRepository;
    @Inject
    StudentRepository studentRepository;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ClubManager.getApplicationComponent().inject(this);

        AppCompatDelegate.setDefaultNightMode(utility.getThemeNight());

        setContentView(R.layout.activity_session);
        Toolbar toolbar = findViewById(R.id.toolbar_session);
        setSupportActionBar(toolbar);

        //addDefaultValues();
        ButterKnife.bind(this);

        presenter.attachView(this);

        setupViews();
    }

    private void addDefaultValues() {
        RealmList<Student> students=new RealmList<>();
        students.addAll(studentRepository.getAll());
        sessionRepository.save(new Session(0,clubRepository.getClub(1),fieldRepository.get(1),coachRepository.get(1), students,"21:00","22:30",1,"024"));
    }


    /**
     * inject coach presenter
     * @param presenter
     */
    @Inject
    public void setPresenter(SessionPresenter presenter)
    {
        this.presenter=presenter;
    }

    /**
     * setup activity views
     */
    private void setupViews() {

        alert_builder = new AlertDialog.Builder(getViewContext());
        fab_addSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getViewContext(), SessionModifyActivity.class));
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(getViewContext()));
        recyclerView.setAdapter(sessions_adapter);
        actionModeCallback = new ActionModeCallback();

    }


    @Override
    public void sessionList(List<Session> sessionList) {
        sessions_adapter=new Sessions_Adapter(sessionList,getViewContext(),this);
    }

    @Override
    public Context getViewContext() {
        return this;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void onRowClicked(int position) {
        if (isLongselectionEnabled)
            enableActionMode(position);
    }

    @Override
    public void onRowLongClicked(int position) {
        isLongselectionEnabled=true;
        enableActionMode(position);
    }

    private void enableActionMode(int position) {
        if (actionMode == null) {
            actionMode = startSupportActionMode(actionModeCallback);
        }
        toggleSelection(position);
    }

    private void toggleSelection(int position) {
        sessions_adapter.toggleSelection(position);
        int count = sessions_adapter.getSelectedItemCount();

        if (count == 0) {
            actionMode.finish();
            actionMode = null;
            isLongselectionEnabled=false;
        } else {
            actionMode.setTitle(String.valueOf(count));
            actionMode.invalidate();
        }
    }

    private void selectAll() {
        try {
            sessions_adapter.selectAll();
            int count = sessions_adapter.getSelectedItemCount();

            if (count == 0) {
                actionMode.finish();
            } else {
                actionMode.setTitle(String.valueOf(count));
                actionMode.invalidate();
            }

            actionMode = null;
        } catch (Exception ex) {
        }
    }

    private void deleteRows() {
        List selectedItemPositions =
                sessions_adapter.getSelectedItems();
        for (int i = selectedItemPositions.size() - 1; i >= 0; i--) {
            sessions_adapter.removeData((Integer) selectedItemPositions.get(i));
        }
        isLongselectionEnabled=false;
        sessions_adapter.notifyDataSetChanged();


        actionMode = null;
    }

    private class ActionModeCallback implements ActionMode.Callback {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.menu_coach_list_option, menu);

            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            //Log.d("API123", "here");
            switch (item.getItemId()) {


                case R.id.coach_menu_delete:
                    // delete all the selected rows
                    alert_builder.setTitle(getViewContext().getString(R.string.warning));
                    alert_builder.setMessage(getViewContext().getString(R.string.are_you_sure));
                    alert_builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            deleteRows();
                            mode.finish();
                        }
                    });
                    alert_builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });
                    alert_builder.show();

                    return true;


                case R.id.coach_menu_select_all:
                    selectAll();
                    return true;

                case R.id.coach_menu_edit:
                    sessions_adapter.updateData();
                    mode.finish();
                    return true;

                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            sessions_adapter.clearSelections();
            actionMode = null;
            isLongselectionEnabled=false;

        }
    }

}
