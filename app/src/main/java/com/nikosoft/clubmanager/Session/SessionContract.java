package com.nikosoft.clubmanager.Session;

import com.nikosoft.clubmanager.Base.BasePresenter;
import com.nikosoft.clubmanager.Base.BaseView;
import com.nikosoft.clubmanager.Data.Model.Session;

import java.util.List;

public interface SessionContract {

    public interface View extends BaseView{
        void sessionList(List<Session> sessionList);
    }

    public interface Presenter extends BasePresenter<View>{
        void getSessionList();
    }
}
