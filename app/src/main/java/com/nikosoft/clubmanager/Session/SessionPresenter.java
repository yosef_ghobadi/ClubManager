package com.nikosoft.clubmanager.Session;

import com.nikosoft.clubmanager.Data.DataSource;

import javax.inject.Inject;

public class SessionPresenter implements SessionContract.Presenter {

    SessionContract.View view;
    DataSource dataSource;

    @Inject
    public SessionPresenter(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void getSessionList() {
        view.sessionList(dataSource.getSessions());
    }

    @Override
    public void attachView(SessionContract.View view) {
        this.view = view;
        getSessionList();

    }

    @Override
    public void detachView() {
        this.view = null;
    }
}
