package com.nikosoft.clubmanager.SessionModify;

import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;

import com.balysv.materialripple.MaterialRippleLayout;
import com.mcsoft.timerangepickerdialog.RangeTimePickerDialog;
import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.Data.DBRepository.SessionRepository;
import com.nikosoft.clubmanager.Data.DBRepository.StudentRepository;
import com.nikosoft.clubmanager.Data.Model.Club;
import com.nikosoft.clubmanager.Data.Model.Coach;
import com.nikosoft.clubmanager.Data.Model.Field;
import com.nikosoft.clubmanager.Data.Model.Session;
import com.nikosoft.clubmanager.Data.Model.Student;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.Session.SessionActivity;
import com.nikosoft.clubmanager.StudentModify.StudentModifyActivity;
import com.nikosoft.clubmanager.Utiles.Utility;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.ganfra.materialspinner.MaterialSpinner;
import io.realm.RealmList;

public class SessionModifyActivity extends AppCompatActivity implements SessionModifyContract.View, RangeTimePickerDialog.ISelectedTime {

    @BindView(R.id.sp_session_modify_club)
    MaterialSpinner sp_club;
    @BindView(R.id.sp_session_modify_coach)
    MaterialSpinner sp_coach;
    @BindView(R.id.sp_session_modify_field)
    MaterialSpinner sp_field;
    @BindView(R.id.sp_session_modify_gender)
    MaterialSpinner sp_gender;

    @BindView(R.id.txt_session_modify_students_caption)
    TextView students_caption;
    @BindView(R.id.txt_session_modify_time_caption)
    TextView time_caption;
    @BindView(R.id.txt_session_modify_days_caption)
    TextView days_caption;

    @BindView(R.id.session_modify_students_choose)
    TextView students_choose;
    @BindView(R.id.session_modify_time_choose)
    TextView time_choose;

    @BindView(R.id.session_modify_sat)
    TextView day_sat;
    @BindView(R.id.session_modify_sun)
    TextView day_sun;
    @BindView(R.id.session_modify_mon)
    TextView day_mon;
    @BindView(R.id.session_modify_tue)
    TextView day_tue;
    @BindView(R.id.session_modify_wed)
    TextView day_wed;
    @BindView(R.id.session_modify_thu)
    TextView day_thu;

    @BindView(R.id.lay_add_session)
    MaterialRippleLayout add_session;


    private ArrayAdapter<String> arr_field;
    private ArrayAdapter<String> arr_club;
    private ArrayAdapter<String> arr_coach;
    private ArrayAdapter<String> arr_gender;

    private String[] field_items = new String[0];
    private String[] club_items = new String[0];
    private String[] coach_items = new String[0];

    List<Field> FieldList = new ArrayList<>();
    List<Club> ClubList = new ArrayList<>();
    List<Coach> CoachList = new ArrayList<>();

    //for detecting edit coach or add new
    private boolean EDIT_MODE = false;

    public int SELECTED_FIELD = -1;
    public int SELECTED_CLUB = -1;
    public int SELECTED_COACH = -1;
    public int SELECTED_GENDER = -1;

    private Session session;
    private int sessionId;

    private boolean satureDay = false;
    private boolean sunDay = false;
    private boolean monDay = false;
    private boolean tuesDay = false;
    private boolean wednesDay = false;
    private boolean thursDay = false;

    @Inject
    Utility utility;
    @Inject
    SessionRepository sessionRepository;
    @Inject
    StudentRepository studentRepository;

    SessionModifyContract.Presenter presenter;
    private List<Student> StudentList;
    private boolean[] checked_students;
    private int startHour = 0;
    private int startMinute = 0;
    private int endHour = 0;
    private int endMinute = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ClubManager.getApplicationComponent().inject(this);

        AppCompatDelegate.setDefaultNightMode(utility.getThemeNight());

        setContentView(R.layout.activity_session_modify);

        Toolbar toolbar = findViewById(R.id.toolbar_session_modify);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        presenter.attachView(this);

        //get the session id for edit
        Bundle bundle = getIntent().getExtras();

        try {
            if (bundle != null) {
                sessionId = bundle.getInt("sessionId");
                EDIT_MODE = true;
                session = sessionRepository.get(sessionId);

            } else {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        setupViews();

        //set drawable to edittexts and textviews
        setViewsDrawable();

    }

    private void setValues(Session session) {
        //set club
        if (session.getClub() != null)
            sp_club.setSelection(arr_club.getPosition(session.getClub().getName()) + 1);


        //set time
        if (!session.getStartTime().equals("0:00")) {
            time_choose.setText(getString(R.string.start_time_caption) + utility.farsinumber(session.getStartTime())
                    + "\n" + getString(R.string.end_time_caption) + utility.farsinumber(session.getEndTime()));
            String[] start_time = session.getStartTime().split(":");
            String[] end_time = session.getEndTime().split(":");
            startHour = Integer.parseInt(start_time[0]);
            startMinute = Integer.parseInt(start_time[1]);
            endHour = Integer.parseInt(end_time[0]);
            endMinute = Integer.parseInt(end_time[1]);
        } else
            time_choose.setText(getString(R.string.choose));

        //set gender
        sp_gender.setSelection(session.getGender() + 1);

        //set days
        boolean[] days = setDays(session.getDaysOfWeek());

        if (days[0])
            day_sat.setSelected(true);
        if (days[1])
            day_sun.setSelected(true);
        if (days[2])
            day_mon.setSelected(true);
        if (days[3])
            day_tue.setSelected(true);
        if (days[4])
            day_wed.setSelected(true);
        if (days[5])
            day_thu.setSelected(true);


    }

    private void setupViews() {

        //set spinners adapter
        arr_club = new ArrayAdapter<>(getViewContext(), android.R.layout.simple_list_item_1, club_items);
        arr_gender = new ArrayAdapter<>(getViewContext(), android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.genders));

        sp_club.setAdapter(arr_club);
        sp_gender.setAdapter(arr_gender);


        //set spinners hint direction
        if (!utility.deviceLanguageDirection()) {
            sp_gender.setRtl();
            sp_coach.setRtl();
            sp_field.setRtl();
            sp_club.setRtl();
        }


        //set club selection if editmode was true
        if (EDIT_MODE & session != null)
            if (session.getClub() != null)
                sp_club.setSelection(arr_club.getPosition(session.getClub().getName()) + 1);

        sp_club.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SELECTED_CLUB = i;
                if (i != -1)
                    setFileds(ClubList.get(i));
                if (!(SELECTED_CLUB == -1 | SELECTED_FIELD == -1 | SELECTED_COACH == -1))
                    try {
                        StudentList = setStudents(ClubList, SELECTED_CLUB, FieldList, SELECTED_FIELD, CoachList, SELECTED_COACH, false);
                        students_choose.setText(R.string.choose);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                //set field selection if editmode was true
                if (EDIT_MODE & session != null)
                    if (session.getField() != null)
                        sp_field.setSelection(arr_field.getPosition(session.getField().getTitle()) + 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        sp_field.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SELECTED_FIELD = i;
                if (i != -1) {
                    setCoaches(FieldList.get(i));
                    if (!(SELECTED_CLUB == -1 | SELECTED_FIELD == -1 | SELECTED_COACH == -1))
                        try {

                            StudentList = setStudents(ClubList, SELECTED_CLUB, FieldList, SELECTED_FIELD, CoachList, SELECTED_COACH, false);
                            students_choose.setText(R.string.choose);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    //set coach selection if editmode was true
                    if (EDIT_MODE & session != null)
                        if(session.getCoach()!=null)
                        sp_coach.setSelection(arr_coach.getPosition(session.getCoach().getName()) + 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        sp_coach.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SELECTED_COACH = i;

                if (!(SELECTED_CLUB == -1 | SELECTED_FIELD == -1 | SELECTED_COACH == -1)) {
                    try {

                        StudentList = setStudents(ClubList, SELECTED_CLUB, FieldList, SELECTED_FIELD, CoachList, SELECTED_COACH, false);
                        students_choose.setText(R.string.choose);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    //set students selection if editmode was true
                    if (EDIT_MODE & session != null)
                        StudentList = setStudents(ClubList, SELECTED_CLUB, FieldList, SELECTED_FIELD, CoachList, SELECTED_COACH, true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        students_choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SessionModifyActivity.this, StudentModifyActivity.class);
                AlertDialog d = showStudentMultiChoiceDialog(getViewContext(), getString(R.string.select_students), StudentList, students_choose, checked_students, intent).create();
                d.show();
            }
        });


        //set time selection if editmode was true
        if (EDIT_MODE & session != null) {
            if (!session.getStartTime().equals("0:00")) {
                time_choose.setText(getString(R.string.start_time_caption) + utility.farsinumber(session.getStartTime())
                        + "\n" + getString(R.string.end_time_caption) + utility.farsinumber(session.getEndTime()));
                String[] start_time = session.getStartTime().split(":");
                String[] end_time = session.getEndTime().split(":");
                startHour = Integer.parseInt(start_time[0]);
                startMinute = Integer.parseInt(start_time[1]);
                endHour = Integer.parseInt(end_time[0]);
                endMinute = Integer.parseInt(end_time[1]);
            } else
                time_choose.setText(getString(R.string.choose));
        }

        time_choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //set start and end time
                startHour = (startHour == 0) ? 21 : startHour;

                endHour = (endHour == 0) ? 22 : endHour;
                endMinute = (endMinute == 0) ? 30 : endMinute;

                // Create an instance of the dialog fragment and show it
                RangeTimePickerDialog dialog = new RangeTimePickerDialog();
                dialog.newInstance(R.color.colorPrimary, R.color.gray_overlay, R.color.white, R.color.colorAccent, true);
                dialog.setRadiusDialog(20); // Set radius of dialog (default is 50)
                dialog.setIs24HourView(true); // Indicates if the format should be 24 hours
                dialog.setTextBtnPositive(getString(R.string.ok));
                dialog.setTextBtnNegative(getString(R.string.cancel));
                dialog.setTextTabStart(getString(R.string.start_time_caption));
                dialog.setTextTabEnd(getString(R.string.end_time_caption));
                dialog.setInitialStartClock(startHour, startMinute);
                dialog.setInitialEndClock(endHour, endMinute);
                dialog.setValidateRange(true);
                dialog.setMessageErrorRangeTime(getString(R.string.time_start_end_error));
                FragmentManager fragmentManager = getFragmentManager();
                dialog.show(fragmentManager, "");
            }
        });


        //set gender  selection if editmode was true
        if (EDIT_MODE & session != null)
            sp_gender.setSelection(session.getGender() + 1);

        sp_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SELECTED_GENDER = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        //set days selection if editmode was true
        if (EDIT_MODE & session != null) {
            boolean[] days = setDays(session.getDaysOfWeek());

            if (days[0]) {
                day_sat.setSelected(true);
                satureDay = true;
            }
            if (days[1]) {
                day_sun.setSelected(true);
                sunDay = true;
            }
            if (days[2]) {
                day_mon.setSelected(true);
                monDay = true;
            }
            if (days[3]) {
                day_tue.setSelected(true);
                tuesDay = true;
            }
            if (days[4]) {
                day_wed.setSelected(true);
                wednesDay = true;
            }
            if (days[5]) {
                day_thu.setSelected(true);
                thursDay = true;
            }
        }

        //week days
        day_sat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isSelected = !view.isSelected();
                view.setSelected(isSelected);
                satureDay = view.isSelected();
            }
        });

        day_sun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isSelected = !view.isSelected();
                view.setSelected(isSelected);
                sunDay = view.isSelected();
            }
        });

        day_mon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isSelected = !view.isSelected();
                view.setSelected(isSelected);
                monDay = view.isSelected();
            }
        });

        day_tue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isSelected = !view.isSelected();
                view.setSelected(isSelected);
                tuesDay = view.isSelected();
            }
        });

        day_wed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isSelected = !view.isSelected();
                view.setSelected(isSelected);
                wednesDay = view.isSelected();
            }
        });

        day_thu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isSelected = !view.isSelected();
                view.setSelected(isSelected);
                thursDay = view.isSelected();
            }
        });


        add_session.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //check club
                if (SELECTED_CLUB == -1) {
                    utility.setError(sp_club, getString(R.string.unselected_value_error));
                    return;
                }

                //check field
                if (SELECTED_FIELD == -1) {
                    utility.setError(sp_field, getString(R.string.unselected_value_error));
                    return;
                }

                //check coach
                if (SELECTED_COACH == -1) {
                    utility.setError(sp_coach, getString(R.string.unselected_value_error));
                    return;
                }

                //check students
                if (students_choose.getText().equals(getString(R.string.choose))) {
                    utility.setError(students_choose, getString(R.string.unselected_value_error));
                    return;
                }

                //check time
                if (time_choose.getText().equals(getString(R.string.choose))) {
                    utility.setError(time_choose, getString(R.string.unselected_value_error));
                    return;
                }

                //check gender
                if (SELECTED_GENDER == -1) {
                    utility.setError(sp_gender, getString(R.string.unselected_value_error));
                    return;
                }

                if (!satureDay & !sunDay & !monDay & !tuesDay & !wednesDay & !thursDay) {
                    utility.setError(days_caption, getString(R.string.unselected_value_error));
                    return;
                }


                RealmList<Student> students = getSelectedStudents(StudentList, checked_students);


                String sminute, eminute;
                if (startMinute == 0)
                    sminute = "00";
                else
                    sminute = startMinute + "";
                if (endMinute == 0)
                    eminute = "00";
                else
                    eminute = endMinute + "";

                Session session = new Session();
                session.setId(sessionId);
                session.setClub(ClubList.get(SELECTED_CLUB));
                session.setField(FieldList.get(SELECTED_FIELD));
                session.setCoach(CoachList.get(SELECTED_COACH));
                session.setStudents(students);
                session.setStartTime(startHour + ":" + sminute);
                session.setEndTime(endHour + ":" + eminute);
                session.setGender(SELECTED_GENDER);
                session.setDaysOfWeek(getDays(satureDay, sunDay, monDay, tuesDay, wednesDay, thursDay));

                //check exist session
                if (!EDIT_MODE)
                    if (sessionRepository.get(session.getClub(), session.getStartTime(), session.getEndTime(), session.getDaysOfWeek())) {
                        Toast.makeText(SessionModifyActivity.this, getString(R.string.exist_session), Toast.LENGTH_LONG).show();
                        return;
                    }

                sessionRepository.save(session);

                Intent intent = new Intent(getViewContext(), SessionActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();

            }
        });
        //session=null;
    }


    /**
     * get selected students
     *
     * @param studentList      all students
     * @param checked_students students check values
     * @return checked students
     */
    private RealmList<Student> getSelectedStudents(List<Student> studentList, boolean[] checked_students) {

        RealmList<Student> students = new RealmList<>();

        for (int i = 0; i < checked_students.length; i++) {
            if (checked_students[i])
                students.add(studentList.get(i));
        }

        return students;
    }

    /**
     * create a string from selected days
     *
     * @param satureDay   =0
     * @param sunDay      =1
     * @param monDay      =2
     * @param tuesDay     =3
     * @param wednesDay=4
     * @param thursDay=5
     * @return a string contains days numbers
     */
    private String getDays(boolean satureDay, boolean sunDay, boolean monDay, boolean tuesDay, boolean wednesDay, boolean thursDay) {
        String days = "";

        if (satureDay)
            days += "0";
        if (sunDay)
            days += "1";
        if (monDay)
            days += "2";
        if (tuesDay)
            days += "3";
        if (wednesDay)
            days += "4";
        if (thursDay)
            days += "5";

        return days;
    }


    /**
     * set week days from string
     *
     * @param days string days
     * @return bool array of days value
     */
    private boolean[] setDays(String days) {
        boolean[] array_days = new boolean[6];
        for (int i = 0; i < days.length(); i++) {
            switch (days.charAt(i)) {
                case '0':
                    array_days[0] = true;
                    break;
                case '1':
                    array_days[1] = true;
                    break;
                case '2':
                    array_days[2] = true;
                    break;
                case '3':
                    array_days[3] = true;
                    break;
                case '4':
                    array_days[4] = true;
                    break;
                case '5':
                    array_days[5] = true;
                    break;

            }
        }
        return array_days;
    }


    /**
     * get students depends on club,field and coach that selected for students
     *
     * @param clubList         selected clubs list
     * @param selectedClubPos  selected club position in selected clubs list
     * @param fieldList        selected fields list
     * @param selectedFieldPos selected field position in selected fields list
     * @param coachList        selected coaches list
     * @param selectedCoachPos selected coach position in selected coaches list
     * @return students list
     */
    private List<Student> setStudents(List<Club> clubList, int selectedClubPos, List<Field> fieldList, int selectedFieldPos, List<Coach> coachList, int selectedCoachPos, boolean checked) {

        Club club = new Club();
        Field field = new Field();
        Coach coach = new Coach();
        List<Student> students = new ArrayList<>();

        if (clubList != null & clubList.size() > 0)
            club = clubList.get(selectedClubPos);

        if (fieldList != null & fieldList.size() > 0)
            field = fieldList.get(selectedFieldPos);

        if (coachList != null & coachList.size() > 0)
            coach = coachList.get(selectedCoachPos);

        students = studentRepository.get(field, club, coach);
        if (students != null) {
            checked_students = new boolean[students.size()];
            if (checked) {
                int counter = 0;
                for (int i = 0; i < checked_students.length; i++) {
                    for (Student item : session.getStudents()) {

                        if (students.get(i).equals(item)) {
                            checked_students[i] = true;
                            counter++;
                        }
                    }

                }
                students_choose.setText(utility.farsinumber(counter + "") + getString(R.string.people));
            }
        } else
            checked_students = new boolean[0];
        return students;
    }


    /**
     * get field coaches
     *
     * @param field
     */
    private void setCoaches(Field field) {
        CoachList = field.getCoaches();
        coach_items = new String[CoachList.size()];

        int i = 0;
        for (Coach coach : CoachList) {
            coach_items[i] = coach.getName();
            i++;
        }
        arr_coach = new ArrayAdapter<>(getViewContext(), android.R.layout.simple_list_item_1, coach_items);
        sp_coach.setAdapter(arr_coach);
    }


    /**
     * get club fields
     *
     * @param club
     */
    private void setFileds(Club club) {
        FieldList = club.getFields();
        field_items = new String[FieldList.size()];

        int i = 0;
        for (Field field : FieldList) {
            field_items[i] = field.getTitle();
            i++;
        }
        arr_field = new ArrayAdapter<>(getViewContext(), android.R.layout.simple_list_item_1, field_items);
        sp_field.setAdapter(arr_field);

    }


    @Inject
    public void setPresenter(SessionModifyPresenter presenter) {
        this.presenter = presenter;
    }


    /**
     * set edittext and textview drawable
     */
    private void setViewsDrawable() {
        utility.setViewDrawable(days_caption, getViewContext(), R.drawable.ic_tuition_day, 4, 10);
        utility.setViewDrawable(students_caption, getViewContext(), R.drawable.ic_athlete, 4, 10);
        utility.setViewDrawable(time_caption, getViewContext(), R.drawable.ic_activity_time, 4, 10);

    }


    /**
     * show dialog for choose students
     *
     * @param context
     * @param title        dialog title
     * @param itemsName    fields name
     * @param textView     textview that shows fields name after selection
     * @param checkedItems save checked fields
     * @param intent       start coaches activity for insert fields
     * @return
     */
    private AlertDialog.Builder showStudentMultiChoiceDialog(Context context, String title,
                                                             List<Student> itemsName,
                                                             TextView textView,
                                                             boolean[] checkedItems,
                                                             Intent intent) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setCancelable(false);

        //ارایه ای برای نمایش نام ایتم ها
        int itemsSize = 0;

        if (itemsName != null)
            itemsSize = itemsName.size();

        String[] filds = new String[itemsSize];
        boolean[] checkedItems_temp =new boolean[itemsSize];

        if (itemsName != null) {
            for (int i = 0; i < itemsSize; i++) {
                filds[i] = itemsName.get(i).getName();
                checkedItems_temp[i] = checkedItems[i];
            }
        }


        builder.setMultiChoiceItems(filds, checkedItems_temp, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i, boolean b) {
                checkedItems_temp[i] = b;
            }
        });

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {


                int counter = 0;
                if (itemsName != null) {
                    for (int j = 0; j < checkedItems_temp.length; j++) {
                        if (checkedItems_temp[j])
                            counter++;

                        checkedItems[j] = checkedItems_temp[j];
                    }
                }
                if (counter == 0)
                    textView.setText(R.string.choose);
                else
                    textView.setText(utility.farsinumber(String.valueOf(counter)) + getString(R.string.people));

                utility.setError(textView, null);

            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.setNeutralButton(R.string.add, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivity(intent);
            }
        });
        builder.setCancelable(true);
        return builder;
    }


    /**
     * get selected start and end time
     *
     * @param hourStart
     * @param minuteStart
     * @param hourEnd
     * @param minuteEnd
     */
    @Override
    public void onSelectedTime(int hourStart, int minuteStart, int hourEnd, int minuteEnd) {
        String s_hour = "", s_minute = "", e_hour = "", e_minute = "";
        this.startHour = hourStart;
        this.startMinute = minuteStart;
        this.endHour = hourEnd;
        this.endMinute = minuteEnd;

        s_hour = String.format("%02d", startHour);
        s_minute = String.format("%02d", startMinute);
        e_hour = String.format("%02d", endHour);
        e_minute = String.format("%02d", endMinute);


        String startTime = getString(R.string.start_caption) + utility.farsinumber(s_hour + ":" + s_minute);
        String endTime = getString(R.string.end_caption) + utility.farsinumber(e_hour + ":" + e_minute);
        time_choose.setText(startTime + "\n" + endTime);

        utility.setError(time_choose, null);
    }


    @Override
    public void clubList(List<Club> clubList) {
        ClubList = clubList;
        club_items = new String[clubList.size()];

        int i = 0;
        for (Club club : clubList) {
            club_items[i] = club.getName();
            i++;
        }
    }


    @Override
    public Context getViewContext() {
        return this;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }
}
