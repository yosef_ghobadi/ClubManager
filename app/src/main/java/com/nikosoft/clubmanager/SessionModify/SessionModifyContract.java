package com.nikosoft.clubmanager.SessionModify;

import com.nikosoft.clubmanager.Base.BasePresenter;
import com.nikosoft.clubmanager.Base.BaseView;
import com.nikosoft.clubmanager.Data.Model.Club;

import java.util.List;

public interface SessionModifyContract {

    public interface View extends BaseView{
        void clubList(List<Club> clubList);


    }

    public interface Presenter extends BasePresenter<View>
    {
        void getClubs();

    }
}
