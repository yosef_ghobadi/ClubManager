package com.nikosoft.clubmanager.SessionModify;

import com.nikosoft.clubmanager.Data.DataSource;

import javax.inject.Inject;

public class SessionModifyPresenter implements SessionModifyContract.Presenter {

    SessionModifyContract.View view;
    DataSource dataSource;

    @Inject
    public SessionModifyPresenter(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void getClubs() {
        view.clubList(dataSource.getClubs());

    }



    @Override
    public void attachView(SessionModifyContract.View view) {
        this.view = view;
        getClubs();

    }

    @Override
    public void detachView() {
        this.view = null;
    }
}
