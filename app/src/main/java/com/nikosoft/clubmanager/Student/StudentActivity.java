package com.nikosoft.clubmanager.Student;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nikosoft.clubmanager.Adapters.Students_Adapter;
import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.Data.DBRepository.InsurancePaymentRepository;
import com.nikosoft.clubmanager.Data.DBRepository.SMSRepository;
import com.nikosoft.clubmanager.Data.DBRepository.StudentRepository;
import com.nikosoft.clubmanager.Data.DBRepository.TuitionPaymentRepository;
import com.nikosoft.clubmanager.Data.Model.InsurancePayment;
import com.nikosoft.clubmanager.Data.Model.Student;
import com.nikosoft.clubmanager.Data.Model.TuitionPayment;
import com.nikosoft.clubmanager.MyAccount.MyAccountActivity;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.Services.SendSMSService;
import com.nikosoft.clubmanager.StudentModify.StudentModifyActivity;
import com.nikosoft.clubmanager.Utiles.Utility;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import saman.zamani.persiandate.PersianDate;

public class StudentActivity extends AppCompatActivity implements StudentContract.View, Students_Adapter.ClickAdapterListener {

    @BindView(R.id.student_recycler)
    RecyclerView recyclerView;
    @BindView(R.id.fab_student)
    FloatingActionButton fab_addStudent;
    @BindView(R.id.toolbar_student)
    Toolbar toolbar;


    Students_Adapter students_adapter;
    StudentContract.Presenter presenter;

    private StudentActivity.ActionModeCallback actionModeCallback;
    private ActionMode actionMode;
    //detect when long press is enabled
    private boolean isLongselectionEnabled = false;
    private AlertDialog.Builder alert_builder;

    @Inject
    Utility utility;
    @Inject
    TuitionPaymentRepository tuitionPaymentRepository;
    @Inject
    InsurancePaymentRepository insurancePaymentRepository;
    @Inject
    SMSRepository smsRepository;
    @Inject
    public StudentRepository studentRepository;

    private int order = 0;
    private List<Student> studentList = new ArrayList<>();
    private List<Student> ordered_studentList = new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ClubManager.getApplicationComponent().inject(this);

        AppCompatDelegate.setDefaultNightMode(utility.getThemeNight());

        setContentView(R.layout.activity_student);


        //addDefaultValues();
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        presenter.attachView(this);

        getStudentsOrder();
        setupViews();


    }

    /**
     * get students load order
     */
    private void getStudentsOrder() {
        Bundle bundle = getIntent().getExtras();

        try {
            if (bundle != null) {
                order = bundle.getInt("order");

            } else {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * inject coach presenter
     *
     * @param presenter
     */
    @Inject
    public void setPresenter(StudentPresenter presenter) {
        this.presenter = presenter;
    }

    /**
     * setup activity views
     */
    private void setupViews() {

        alert_builder = new AlertDialog.Builder(getViewContext());
        fab_addStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //اگر تعداد دانش آموزان بیشتر از تعداد محدود باشد
                if(studentRepository.getAll().size()>utility.LIMIT_COUNT)
                     {
                    //اگر اشتراک منقضی شده باشد
                    if(utility.retrievePref(utility.IS_VALID_APP,"-1",getViewContext()).equals("0")) {
                        //نمایش دیالوگ برای خرید اشتراک
                        AlertDialog.Builder builder = new AlertDialog.Builder(getViewContext());
                        builder.setTitle(getViewContext().getString(R.string.upgrate));
                        builder.setMessage(getViewContext().getString(R.string.upgrate_app_desc));
                        builder.setPositiveButton(R.string.purchase_sub, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                startActivity(new Intent(StudentActivity.this, MyAccountActivity.class));
                            }
                        });
                        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
                        builder.show();
                    }
                    else {
                           startActivity(new Intent(getViewContext(), StudentModifyActivity.class));

                    }

                }
                else
                    startActivity(new Intent(getViewContext(), StudentModifyActivity.class));

            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(getViewContext()));
        recyclerView.setAdapter(students_adapter);
        actionModeCallback = new ActionModeCallback();


    }

    @Override
    public void studentList(List<Student> studentList) {
        this.studentList = studentList;

        ordered_studentList = getStudents(studentList, order);

        students_adapter = new Students_Adapter(ordered_studentList, getViewContext(), this);
        students_adapter.notifyDataSetChanged();
    }

    /**
     * get ordered students
     *
     * @param studentList all students list
     * @param order       0=all , 1=expired tuition , 2=expired insurance
     */
    public List<Student> getStudents(List<Student> studentList, int order) {

        List<Student> temp_students = new ArrayList<>();
        switch (order) {
            case 0:
                return studentList;

            case 1:
                for (Student student : studentList) {
                    TuitionPayment payment = tuitionPaymentRepository.getStudentLastPay(student.getId());

                    if (payment != null) {
                        //اگر ماه شهریه کوچکتر از امروز باشد
                        if (utility.getPersianDate(payment.getDueDate()).compareTo(PersianDate.today()) == -1) {
                            if (utility.getPersianDate(payment.getPayDate()).addMonth(1).compareTo(PersianDate.today()) == -1) {
                                temp_students.add(student);
                            }
                        }
                    }
                    //اگر تا کنون شهریه پرداخت نکرده باشد
                    else
                        temp_students.add(student);
                }
                return temp_students;

            case 2:
                for (Student student : studentList) {
                    InsurancePayment payment = insurancePaymentRepository.getLastInsurance(student.getId());
                    if (payment != null) {
                        //اگر ماه شهریه کوچکتر از امروز باشد
                        if (utility.getPersianDate(payment.getDate()).addYear(1).compareTo(PersianDate.today()) == -1) {
                            temp_students.add(student);
                        }
                    }
                    //اگر تا کنون بیمه پرداخت نکرده باشد
                    else
                        temp_students.add(student);
                }
                return temp_students;
        }
        return null;
    }

    /**
     * get students list based on order
     *
     * @param order 0=all 1=expired tuitions 2=expired insurance
     * @return list of selected students
     */
    public List<Student> getStudents(int order) {

        List<Student> temp_students = new ArrayList<>();
        List<Student> studentList = studentRepository.getAll();

        switch (order) {
            case 0:
                return studentList;

            case 1:
                for (Student student : studentList) {
                    TuitionPayment payment = tuitionPaymentRepository.getStudentLastPay(student.getId());
                    //اگر ماه شهریه کوچکتر از امروز باشد
                    if (utility.getPersianDate(payment.getDueDate()).compareTo(PersianDate.today()) == -1) {
                        if (utility.getPersianDate(payment.getPayDate()).addMonth(1).compareTo(PersianDate.today()) == -1) {
                            temp_students.add(student);
                        }
                    }
                }
                return temp_students;

            case 2:
                for (Student student : studentList) {
                    InsurancePayment payment = insurancePaymentRepository.getLastInsurance(student.getId());
                    //اگر ماه شهریه کوچکتر از امروز باشد
                    if (utility.getPersianDate(payment.getDate()).addYear(1).compareTo(PersianDate.today()) == -1) {
                        temp_students.add(student);
                    }
                }
                return temp_students;
        }
        return null;
    }

    @Override
    public Context getViewContext() {
        return this;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();

        insurancePaymentRepository.close();
        smsRepository.close();
        studentRepository.close();
        tuitionPaymentRepository.close();
    }

    @Override
    protected void onResume() {
        super.onResume();
        students_adapter.notifyDataSetChanged();
    }

    @Override
    public void onRowClicked(int position) {
        if (isLongselectionEnabled)
            enableActionMode(position);
        else
            students_adapter.selectItem(position);
    }

    @Override
    public void onRowLongClicked(int position) {
        isLongselectionEnabled = true;
        enableActionMode(position);
    }

    private void enableActionMode(int position) {
        if (actionMode == null) {
            actionMode = startSupportActionMode(actionModeCallback);
        }
        toggleSelection(position);
    }

    private void toggleSelection(int position) {
        students_adapter.toggleSelection(position);
        int count = students_adapter.getSelectedItemCount();

        if (count == 0) {
            actionMode.finish();
            actionMode = null;
            isLongselectionEnabled = false;
        } else {
            actionMode.setTitle(String.valueOf(count));
            actionMode.invalidate();
        }
    }

    private void selectAll() {
        try {
            students_adapter.selectAll();
            int count = students_adapter.getSelectedItemCount();

            if (count == 0) {
                actionMode.finish();
            } else {
                actionMode.setTitle(String.valueOf(count));
                actionMode.invalidate();
            }

            actionMode = null;
        } catch (Exception ex) {
        }
    }

    private void deleteRows() {
        List selectedItemPositions =
                students_adapter.getSelectedItems();
        for (int i = selectedItemPositions.size() - 1; i >= 0; i--) {
            students_adapter.removeData((Integer) selectedItemPositions.get(i));
        }
        isLongselectionEnabled = false;
        students_adapter.notifyDataSetChanged();


        actionMode = null;
    }

    private class ActionModeCallback implements ActionMode.Callback {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.menu_coach_list_option, menu);

            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            //Log.d("API123", "here");
            switch (item.getItemId()) {


                case R.id.coach_menu_delete:
                    // delete all the selected rows
                    alert_builder.setTitle(getViewContext().getString(R.string.warning));
                    alert_builder.setMessage(getViewContext().getString(R.string.are_you_sure));
                    alert_builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            deleteRows();
                            mode.finish();
                        }
                    });
                    alert_builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });
                    alert_builder.show();

                    return true;


                case R.id.coach_menu_select_all:
                    selectAll();
                    return true;

                case R.id.coach_menu_edit:
                    students_adapter.updateData();
                    mode.finish();
                    return true;

                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            students_adapter.clearSelections();
            actionMode = null;
            isLongselectionEnabled = false;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_student, menu);

        MenuItem search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);

        int searchIconId = searchView.getContext().getResources().getIdentifier("android:id/search_button", null, null);
        ImageView searchIcon = (ImageView) searchView.findViewById(searchIconId);
        searchIcon.setImageResource(R.drawable.ic_search_white);

        search(searchView);
        return super.onCreateOptionsMenu(menu);
    }

    private void search(SearchView searchView) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                students_adapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.order_all:
                ordered_studentList = getStudents(studentList, 0);
                toolbar.setTitle(getString(R.string.title_activity_student));
                break;

            case R.id.order_exp_tuition:
                ordered_studentList = getStudents(studentList, 1);
                toolbar.setTitle(getString(R.string.out_of_date_tuitions));
                break;

            case R.id.order_exp_insurance:
                ordered_studentList = getStudents(studentList, 2);
                toolbar.setTitle(getString(R.string.out_of_date_insurance));
                break;

            case R.id.send_sms:
                //send sms to students of list
                if(ordered_studentList.size()==0) {
                    Toast.makeText(getViewContext(), getString(R.string.students_list_is_empty), Toast.LENGTH_LONG).show();
                    break;
                }
                AlertDialog.Builder alert = new AlertDialog.Builder(getViewContext());
                final MaterialEditText edittext = new MaterialEditText(getViewContext());
                edittext.setHint(R.string.message_title);
                edittext.setSingleLine(false);
                edittext.setFloatingLabel(MaterialEditText.FLOATING_LABEL_HIGHLIGHT);
                edittext.setFloatingLabelText(getString(R.string.message_title));
                edittext.setText(utility.retrievePref(utility.DEFAULT_MESSAGE,"", getViewContext()));
                edittext.setMaxCharacters(70);
                edittext.setMinCharacters(10);
                edittext.setBaseColor(R.color.colorAccent);

                edittext.setPaddings(10,10,10,10);

                alert.setTitle(this.getString(R.string.send_from)+utility.farsinumber(utility.retrievePref(utility.PHONE_NUMBER,"",getViewContext())));
                alert.setView(edittext);
                alert.setCancelable(false);

                alert.setPositiveButton(getString(R.string.send_message), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //What ever you want to do with the value

                        if(edittext.getText().length()>=10 & edittext.getText().length()<=160) {
                            String message = edittext.getText().toString();
                            utility.savePref(utility.DEFAULT_MESSAGE, message, getViewContext());
                            startService();
                        }
                        else
                            Toast.makeText(getViewContext(), getString(R.string.min_max_sms_character_error), Toast.LENGTH_SHORT).show();
                    }
                });

                alert.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // what ever you want to do with No option.
                    }
                });

                alert.show();

                break;

        }
        students_adapter = new Students_Adapter(ordered_studentList, getViewContext(), this);
        recyclerView.setAdapter(students_adapter);
        students_adapter.notifyDataSetChanged();
        return super.onOptionsItemSelected(item);
    }

    private void startService() {
        Intent intent = new Intent(getViewContext(), SendSMSService.class);
        intent.putExtra("order", order);
        startService(intent);


    }





}
