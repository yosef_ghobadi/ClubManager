package com.nikosoft.clubmanager.Student;

import com.nikosoft.clubmanager.Base.BasePresenter;
import com.nikosoft.clubmanager.Base.BaseView;
import com.nikosoft.clubmanager.Data.Model.Student;

import java.util.List;

public interface StudentContract {

    public interface View extends BaseView {
        void studentList(List<Student> studentList);
    }

    public interface Presenter extends BasePresenter<View>
    {
        void getStudents();
    }
}
