package com.nikosoft.clubmanager.Student;

import com.nikosoft.clubmanager.Data.DataSource;

import javax.inject.Inject;

public class StudentPresenter implements StudentContract.Presenter {

    private StudentContract.View view;
    private DataSource dataSource;

    @Inject
    public StudentPresenter(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void getStudents() {
        view.studentList(dataSource.getStudents());
    }

    @Override
    public void attachView(StudentContract.View view) {
        this.view = view;
        getStudents();
    }

    @Override
    public void detachView() {
        this.view = null;
    }
}
