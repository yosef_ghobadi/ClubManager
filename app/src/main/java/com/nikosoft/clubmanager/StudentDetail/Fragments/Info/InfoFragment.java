package com.nikosoft.clubmanager.StudentDetail.Fragments.Info;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;

import com.nikosoft.clubmanager.Base.BaseFragment;
import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.Data.DBRepository.StudentRepository;
import com.nikosoft.clubmanager.Data.Model.Club;
import com.nikosoft.clubmanager.Data.Model.Coach;
import com.nikosoft.clubmanager.Data.Model.Student;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.Utiles.Utility;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class InfoFragment extends BaseFragment implements InfoFragmentContract.View {

    private static final String STUDENT_ID = "coachId";
    public int studentId;
    private Student student = null;
    @Inject
    Utility utility;
    @Inject
    StudentRepository studentRepository;


    InfoFragmentContract.Presenter presenter;

    @BindView(R.id.txt_frag_info_phone)
    TextView txt_phone;
    @BindView(R.id.txt_frag_info_club)
    TextView txt_club;
    @BindView(R.id.txt_frag_info_birthdate)
    TextView txt_birthdate;
    @BindView(R.id.txt_frag_info_coach)
    TextView txt_coach;
    @BindView(R.id.txt_frag_info_desc)
    TextView txt_desc;
    @BindView(R.id.txt_frag_info_field)
    TextView txt_field;
    /*@BindView(R.id.txt_frag_info_activity_status)
    TextView txt_activity;
    @BindView(R.id.switch_activity)
    SwitchButton switch_activity;*/


    public InfoFragment() {
        // Required empty public constructor
    }


    public static InfoFragment newInstance(int studentId) {
        InfoFragment fragment = new InfoFragment();
        Bundle args = new Bundle();
        args.putInt(STUDENT_ID, studentId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            studentId = getArguments().getInt(STUDENT_ID);

            ClubManager.getApplicationComponent().inject(this);
            AppCompatDelegate.setDefaultNightMode(utility.getThemeNight());

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(getLayout(), container, false);


        presenter.attachView(this);
        ButterKnife.bind(this, view);

        setupViews();
        return view;
    }

    @Inject
    public void setPresenter(InfoFragmentPresenter presenter) {
        presenter.studentId = studentId;
        this.presenter = presenter;
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_info;
    }

    @Override
    public void setupViews() {


        txt_phone.setText(utility.farsinumber(student.getPhone()));
        txt_birthdate.setText(utility.farsinumber(utility.getFormattedDate(student.getBirthDate(), 0)));
        txt_desc.setText(student.getDescription());

        if (student.getField() != null)
            txt_field.setText(student.getField().getTitle());

        if (student.getClubs().size() > 0) {
            for (Club item : student.getClubs()) {
                txt_club.setText(txt_club.getText() + item.getName() + " ,");
            }
            txt_club.setText(txt_club.getText().toString().substring(0, txt_club.getText().length() - 1));
        }

        if (student.getCoaches().size() > 0) {
            for (Coach item : student.getCoaches()) {
                txt_coach.setText(txt_coach.getText() + item.getName() + " ,");
            }
            txt_coach.setText(txt_coach.getText().toString().substring(0, txt_coach.getText().length() - 1));
        }

        /*if (student.isActivityStatus()) {
            txt_activity.setText(getString(R.string.active));
            txt_activity.setTextColor(getResources().getColor(R.color.green));
        }
        else {
            txt_activity.setText(getString(R.string.inactive));
            txt_activity.setTextColor(getResources().getColor(R.color.red_light));
        }

        switch_activity.setChecked(student.isActivityStatus());
        switch_activity.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {


                if(isChecked) {

                    setActivityChanges(new StudentActivityHistory(
                            0,
                            Long.valueOf(utility.getTodaysDate(false)),
                            0,
                            studentId),isChecked);
                }
                else
                {

                    setActivityChanges(new StudentActivityHistory(
                            0,
                            0,
                            Long.valueOf(utility.getTodaysDate(false)),
                            studentId),isChecked);
                }
            }
        });*/
    }

   /* private void setActivityChanges(StudentActivityHistory studentActivityHistory, boolean isChecked) {

        //save status in db return db changes res for update ui
        if(historyRepository.save(studentActivityHistory,isChecked))
        {

            //change activity value
            student.getRealm().executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    student.setActivityStatus(isChecked);
                }
            });
            //update student status in db
            studentRepository.save(student);

            //update ui
            if(isChecked) {
                txt_activity.setText(getString(R.string.active));
                txt_activity.setTextColor(getResources().getColor(R.color.green));
            }
            else{
                txt_activity.setText(getString(R.string.inactive));
                txt_activity.setTextColor(getResources().getColor(R.color.red_light));
            }
            //update profile image border in activity
            ((StudentDetailActivity) Objects.requireNonNull(getActivity())).setActivityColor(isChecked);


        }
    }*/


    @Override
    public void student(Student student) {
        this.student = student;
    }


    @Override
    public Context getViewContext() {
        return getContext();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
        studentRepository.close();
    }
}


