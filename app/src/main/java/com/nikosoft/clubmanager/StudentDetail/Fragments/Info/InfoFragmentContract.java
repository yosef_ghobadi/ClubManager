package com.nikosoft.clubmanager.StudentDetail.Fragments.Info;

import com.nikosoft.clubmanager.Base.BasePresenter;
import com.nikosoft.clubmanager.Base.BaseView;
import com.nikosoft.clubmanager.Data.Model.Student;

public interface InfoFragmentContract  {

    interface View extends BaseView{
        void student(Student student);

    }

    interface Presenter extends BasePresenter<View>{

        void getStudent(int id);

    }
}
