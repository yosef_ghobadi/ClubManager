package com.nikosoft.clubmanager.StudentDetail.Fragments.Info;

import com.nikosoft.clubmanager.Data.DataSource;

import javax.inject.Inject;

public class InfoFragmentPresenter implements InfoFragmentContract.Presenter {

    InfoFragmentContract.View view;
    DataSource dataSource;

    public int studentId=-1;



    @Inject
    public InfoFragmentPresenter(DataSource dataSource) {
        this.dataSource = dataSource;

    }

    @Override
    public void attachView(InfoFragmentContract.View view) {
        this.view=view;
        getStudent(studentId);
    }

    @Override
    public void detachView() {
        this.view=null;
    }

    @Override
    public void getStudent(int id) {
        view.student(dataSource.getStudent(id));
    }


}
