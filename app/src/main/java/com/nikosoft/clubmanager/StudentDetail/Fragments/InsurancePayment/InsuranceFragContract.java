package com.nikosoft.clubmanager.StudentDetail.Fragments.InsurancePayment;

import com.nikosoft.clubmanager.Base.BasePresenter;
import com.nikosoft.clubmanager.Base.BaseView;
import com.nikosoft.clubmanager.Data.Model.Insurance;
import com.nikosoft.clubmanager.Data.Model.InsurancePayment;

import java.util.List;

public interface InsuranceFragContract {

    public interface View extends BaseView
    {
        void insuranceList(List<Insurance> insuranceList);
        void insurancePaymentList(List<InsurancePayment> insurancePaymentList);
    }

    public interface Presenter extends BasePresenter<View>
    {
        void getInsuranceList();
        void getInsurancePaymentList(int studentId);
    }
}
