package com.nikosoft.clubmanager.StudentDetail.Fragments.InsurancePayment;

import com.nikosoft.clubmanager.Data.DataSource;

import javax.inject.Inject;

public class InsuranceFragPresenter implements InsuranceFragContract.Presenter {

    InsuranceFragContract.View view;
    DataSource dataSource;

    public int studentId=-1;

    @Inject
    public InsuranceFragPresenter(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void getInsuranceList() {
        view.insuranceList(dataSource.getInsurances());
    }

    @Override
    public void getInsurancePaymentList(int studentId) {
        view.insurancePaymentList(dataSource.getInsurancePayment(studentId));
    }

    @Override
    public void attachView(InsuranceFragContract.View view) {
        this.view=view;
        getInsuranceList();
        getInsurancePaymentList(studentId);
    }

    @Override
    public void detachView() {
        this.view=null;
    }
}
