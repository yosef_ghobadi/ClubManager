package com.nikosoft.clubmanager.StudentDetail.Fragments.InsurancePayment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.nikosoft.clubmanager.Adapters.InsurancePayment_Adapter;
import com.nikosoft.clubmanager.Base.BaseFragment;
import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.Data.DBRepository.InsurancePaymentRepository;
import com.nikosoft.clubmanager.Data.DBRepository.StudentRepository;
import com.nikosoft.clubmanager.Data.Model.Insurance;
import com.nikosoft.clubmanager.Data.Model.InsurancePayment;
import com.nikosoft.clubmanager.Data.Model.Student;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.Utiles.Utility;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.ganfra.materialspinner.MaterialSpinner;
import ir.hamsaa.persiandatepicker.Listener;
import ir.hamsaa.persiandatepicker.PersianDatePickerDialog;
import ir.hamsaa.persiandatepicker.util.PersianCalendar;
import saman.zamani.persiandate.PersianDate;


public class InsuranceFragment extends BaseFragment implements InsuranceFragContract.View {

    private static final String STUDENT_ID = "coachId";

    private String[] insuranceItems;
    private List<Insurance> insuranceList;
    private List<InsurancePayment> insurance_payment;
    private int studentId=0;


    @BindView(R.id.sp_frag_insur_insurance)
    MaterialSpinner sp_insur;
    @BindView(R.id.lay_frag_insur_ripple)
    MaterialRippleLayout select_date;
    @BindView(R.id.lay_add_insurance_payment)
    MaterialRippleLayout add_insur_pay;
    @BindView(R.id.txt_frag_insur_pay_date)
    TextView txt_date;
    @BindView(R.id.recy_frag_insur)
    RecyclerView recyclerView;
    @BindView(R.id.lay_expan_insurance)
    ExpandableLinearLayout expandableLinearLayout;
    @BindView(R.id.lay_click_expan_insurance)
    LinearLayout btn_expan;
    @BindView(R.id.img_expan_insurance_arrow)
    ImageView img_expan;


    InsurancePayment_Adapter adapter;
    ArrayAdapter arrayAdapter;
    public int SELECTED_INSURANCE=-1;
    private long selected_date=0;

    @Inject
    Utility utility;
    @Inject
    InsurancePaymentRepository repository;
    @Inject
    StudentRepository studentRepository;

    InsuranceFragContract.Presenter presenter;

    public InsuranceFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *

     * @return A new instance of fragment SalaryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InsuranceFragment newInstance(int studentId) {
        InsuranceFragment fragment = new InsuranceFragment();
        Bundle args = new Bundle();

        args.putInt(STUDENT_ID, studentId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            studentId=getArguments().getInt(STUDENT_ID);
        }

        ClubManager.getApplicationComponent().inject(this);
        AppCompatDelegate.setDefaultNightMode(utility.getThemeNight());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(getLayout(), container, false);

        ButterKnife.bind(this, view);
        presenter.attachView(this);
        setupViews();


        return view;
    }

    @Inject
    public void setPresenter(InsuranceFragPresenter presenter)
    {
        presenter.studentId=studentId;
        this.presenter=presenter;
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_insurance;
    }

    @Override
    public void setupViews() {
        //set recycler adapter
        insurance_payment=  repository.getInsurancePayment(studentId);

        adapter=new InsurancePayment_Adapter(getContext(),insurance_payment);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);


        //set expandable layout
        btn_expan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expandableLinearLayout.toggle();
                if(expandableLinearLayout.isExpanded())
                    img_expan.setImageResource(R.drawable.ic_arrow_down);
                else
                    img_expan.setImageResource(R.drawable.ic_arrow_up);
                expandableLinearLayout.initLayout();
            }
        });



        //set spinner adapter
        insuranceItems=new String[insuranceList.size()];
        for(int i = 0; i< insuranceList.size(); i++)
        {
            insuranceItems[i]= insuranceList.get(i).getTitle()+" ( "+utility.farsinumber(utility.threeDigit(String.valueOf(insuranceList.get(i).getCost()),','))+" )";
        }
        arrayAdapter=new ArrayAdapter(getContext(),android.R.layout.simple_list_item_1,insuranceItems);
        sp_insur.setAdapter(arrayAdapter);

        sp_insur.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SELECTED_INSURANCE=i;
                expandableLinearLayout.initLayout();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if(utility.deviceLanguageDirection()==false)
            sp_insur.setRtl();

        //set pay date
        selected_date= Long.parseLong(utility.getTodaysDate(false));
        txt_date.setText(utility.farsinumber(utility.getTodaysDate(true)));
        select_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //get today's date
                PersianCalendar persianCalendar=new PersianCalendar();
                persianCalendar.setPersianDate(persianCalendar.getPersianYear(),persianCalendar.getPersianMonth(),persianCalendar.getPersianDay());

                PersianDatePickerDialog picker = new PersianDatePickerDialog(getContext())
                        .setPositiveButtonString(getString(R.string.ok))
                        .setNegativeButton(getString(R.string.cancel))
                        .setTodayButton(getString(R.string.today))
                        .setTodayButtonVisible(true)
                        .setInitDate(persianCalendar)
                        .setMaxYear(PersianDatePickerDialog.THIS_YEAR)
                        .setMinYear(1390)
                        .setListener(new Listener() {
                            @Override
                            public void onDateSelected(PersianCalendar persianCalendar) {

                                String date= utility.getDateFromPersianCalendar(persianCalendar,false);
                                selected_date= Long.parseLong(date);
                                date=utility.farsinumber(utility.getFormattedDate(Long.parseLong(date),0));
                                txt_date.setText(date);
                            }

                            @Override
                            public void onDismissed() {

                            }
                        });

                picker.show();
            }
        });


        //add insuranceList payment
        add_insur_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SELECTED_INSURANCE==-1)
                    utility.setError(sp_insur,getString(R.string.unselected_value_error));

                else
                {
                    Student student=studentRepository.get(studentId);
                    InsurancePayment insurancePayment=new InsurancePayment(0,student, insuranceList.get(SELECTED_INSURANCE),selected_date);
                    if(repository.getInsurancePayment(student.getPhone(), insuranceList.get(SELECTED_INSURANCE).getTitle()).isEmpty())
                    {
                        repository.save(insurancePayment);
                        adapter.notifyDataSetChanged();
                    }
                    else
                        Toast.makeText(getContext(), getString(R.string.duplicate_insurance_register_error), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
        repository.close();

    }

    @Override
    public void insuranceList(List<Insurance> insuranceList) {
        this.insuranceList=insuranceList;
    }

    @Override
    public void insurancePaymentList(List<InsurancePayment> insurancePaymentList) {
        insurance_payment=insurancePaymentList;
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }
}
