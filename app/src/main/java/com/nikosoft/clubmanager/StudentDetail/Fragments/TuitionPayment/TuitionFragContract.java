package com.nikosoft.clubmanager.StudentDetail.Fragments.TuitionPayment;

import com.nikosoft.clubmanager.Base.BasePresenter;
import com.nikosoft.clubmanager.Base.BaseView;
import com.nikosoft.clubmanager.Data.Model.Student;
import com.nikosoft.clubmanager.Data.Model.Tuition;
import com.nikosoft.clubmanager.Data.Model.TuitionPayment;

import java.util.List;

public interface TuitionFragContract {

    public interface View extends BaseView
    {
        void yearAccountingList(List<ActivityHistoryAccounting> yearAccountants);
        void tuitionList(List<Tuition> tuitionList);
        void student(Student student);
    }

    public interface Presenter extends BasePresenter<View>
    {
        void getTuitionList();
        void getTuitionPaymentList(int studentId);
        void getStudentAH(int studentId);
        void getYearAccountants(List<TuitionPayment> tuitionPayments);
        void refreshData();
        void getStudent(int studentId);
    }
}
