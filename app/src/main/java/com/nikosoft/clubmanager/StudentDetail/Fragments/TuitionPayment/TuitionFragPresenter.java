package com.nikosoft.clubmanager.StudentDetail.Fragments.TuitionPayment;

import android.content.Context;

import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.Data.DataSource;
import com.nikosoft.clubmanager.Data.Model.Student;
import com.nikosoft.clubmanager.Data.Model.TuitionPayment;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.Utiles.Utility;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import saman.zamani.persiandate.PersianDate;

public class TuitionFragPresenter implements TuitionFragContract.Presenter {

    TuitionFragContract.View view;
    DataSource dataSource;

    private List<TuitionPayment> tuitionPayments;



    public int studentId = -1;
    private Student student;

    @Inject
    Context context;

    @Inject
    Utility utility;

    @Inject
    public TuitionFragPresenter(DataSource dataSource) {
        this.dataSource = dataSource;
        ClubManager.getApplicationComponent().inject(this);
    }


    @Override
    public void attachView(TuitionFragContract.View view) {
        this.view = view;

        getStudent(studentId);
        getTuitionList();
        getTuitionPaymentList(studentId);
        //getStudentAH(studentId);
        getYearAccountants(tuitionPayments);
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public void getTuitionList() {
        view.tuitionList(dataSource.getTuitions());
    }

    @Override
    public void getTuitionPaymentList(int studentId) {
        tuitionPayments = dataSource.getTuitionPayments(studentId);
       /* dataSource.getTuitionPayments(studentId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new FlowableSubscriber<RealmResults<TuitionPayment>>() {
                    @Override
                    public void onSubscribe(Subscription s) {

                    }

                    @Override
                    public void onNext(RealmResults<TuitionPayment> tuition_Payments) {
                    tuitionPayments=tuition_Payments;
                        Toast.makeText(context, "Tuition Payment well done ", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        //t.getCause().getStackTrace();
                        Toast.makeText(context, "Tuition Payment : "+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {
                        //گرفتن سابقه فعالیت شاگرد
                        getStudentAH(studentId);
                    }
                });*/
    }

    @Override
    public void getStudentAH(int studentId) {
        //histories = dataSource.getStudentActivityHistory(studentId);
        /*dataSource.getStudentActivityHistory(studentId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new FlowableSubscriber<RealmResults<StudentActivityHistory>>() {
                    @Override
                    public void onSubscribe(Subscription s) {

                    }

                    @Override
                    public void onNext(RealmResults<StudentActivityHistory> studentActivityHistories) {
                    histories=studentActivityHistories;
                        Toast.makeText(context, "History well done", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        t.getCause();
                        Toast.makeText(context, "History error : "+t.getMessage(), Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onComplete() {
                        getYearAccountants(tuitionPayments, histories);
                    }
                });*/
    }

    @Override
    public void getYearAccountants(List<TuitionPayment> tuitionPayments) {

        view.yearAccountingList(loadData(tuitionPayments));

    }

    private List<ActivityHistoryAccounting> loadData(List<TuitionPayment> tuitionPayments) {

        //list of activity accounting
        List<ActivityHistoryAccounting> activityHistoryAccountings = new ArrayList<>();

        //list of activity row
        List<TuitionPayment> formatted_payments = new ArrayList<>();

        int year_title=0;
        if(tuitionPayments.size()>0)
            year_title=utility.getPersianDate(tuitionPayments.get(0).getPayDate()).getShYear();



        //while activity start date less than activity end date
        for (TuitionPayment payment : tuitionPayments) {



                //گروه بندی پرداختها بر اساس سال پرداخت
                if (year_title < utility.getPersianDate(payment.getPayDate()).getShYear())
                {
                    //activity accounting
                    ActivityHistoryAccounting accounting;
                    if(year_title>0) {
                        accounting = new ActivityHistoryAccounting(utility.farsinumber(context.getString(R.string.year_title) + year_title), formatted_payments);

                        //add activity history accounting
                        activityHistoryAccountings.add(accounting);

                        year_title = utility.getPersianDate(payment.getPayDate()).getShYear();

                        formatted_payments.clear();
                        formatted_payments.add(payment);
                    }
                }
                else
                {
                    formatted_payments.add(payment);
                }
            }
        if(year_title>0) {
            ActivityHistoryAccounting accounting = new ActivityHistoryAccounting(utility.farsinumber(context.getString(R.string.year_title) + year_title), formatted_payments);
            activityHistoryAccountings.add(accounting);
        }

        return activityHistoryAccountings;
        }

        @Override
        public void refreshData () {
            getTuitionPaymentList(studentId);
            getYearAccountants(tuitionPayments);
        }


        @Override
        public void getStudent ( int studentId){
            student = dataSource.getStudent(studentId);
            view.student(student);
        }


        /**
         * get tuition payment between two date
         *
         * @param startDate       start date
         * @param endDate         end date
         * @param tuitionPayments list of tuittion payments
         * @return return tuition payment
         */
        private TuitionPayment isPaidDuringDates (PersianDate startDate, PersianDate
        endDate, List < TuitionPayment > tuitionPayments){

            PersianDate start_date = startDate;
            PersianDate end_date = utility.addPersianDate(startDate, 0, 1, 0);

            if (tuitionPayments != null) {
                for (TuitionPayment payment : tuitionPayments) {
                    if (payment.getDueDate() > 0 & endDate.compareTo(end_date) == 1) {
                        //compare dates by year and month
                        if ((utility.compareYearAndMonth(utility.getPersianDate(payment.getDueDate()), start_date) == 1 | utility.compareYearAndMonth(utility.getPersianDate(payment.getDueDate()), start_date) == 0)
                                & utility.compareYearAndMonth(utility.getPersianDate(payment.getDueDate()), end_date) == -1) {
                            return payment;
                        }

                    }
                }
            /*start_date=utility.addPersianDate(startDate,0,1,0);
            end_date=utility.addPersianDate(startDate,0,1,0);*/
            }
            return null;
        }


    }
