package com.nikosoft.clubmanager.StudentDetail.Fragments.TuitionPayment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.nikosoft.clubmanager.Adapters.TuitionPaymentAdapter;
import com.nikosoft.clubmanager.Base.BaseFragment;
import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.Data.DBRepository.StudentRepository;
import com.nikosoft.clubmanager.Data.DBRepository.TuitionPaymentRepository;
import com.nikosoft.clubmanager.Data.Model.Club;
import com.nikosoft.clubmanager.Data.Model.Student;
import com.nikosoft.clubmanager.Data.Model.Tuition;
import com.nikosoft.clubmanager.Data.Model.TuitionPayment;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.Utiles.Utility;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.ganfra.materialspinner.MaterialSpinner;
import io.realm.RealmList;
import ir.hamsaa.persiandatepicker.Listener;
import ir.hamsaa.persiandatepicker.PersianDatePickerDialog;
import ir.hamsaa.persiandatepicker.util.PersianCalendar;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;


public class TuitionFragment extends BaseFragment implements TuitionFragContract.View {


    private static final String STUDENT_ID = "studentId";

    private List<Tuition> tuitionList;
    private List<TuitionPayment> tuitionPayments;
    private int studentId = 0;
    private Student student;

    @BindView(R.id.sp_frag_tuition)
    MaterialSpinner sp_tuition;
    @BindView(R.id.sp_frag_club)
    MaterialSpinner sp_club;
    @BindView(R.id.sp_frag_dueDate)
    MaterialSpinner sp_dueDate;
    @BindView(R.id.lay_frag_tuition_ripple)
    MaterialRippleLayout select_date;
    @BindView(R.id.lay_add_tuition_payment)
    MaterialRippleLayout add_tuition_pay;
    @BindView(R.id.txt_frag_tuition_pay_date)
    TextView txt_date;
    @BindView(R.id.recy_frag_tuition)
    RecyclerView recyclerView;
    @BindView(R.id.lay_expan_tuition)
    ExpandableLinearLayout expandableLinearLayout;
    @BindView(R.id.lay_click_expan_tuition)
    LinearLayout btn_expan;
    @BindView(R.id.img_expan_tuition_arrow)
    ImageView img_expan;

    TuitionPaymentAdapter tuitionPaymentAdapter;
    ArrayAdapter tuitionAdapter;
    ArrayAdapter clubAdapter;
    ArrayAdapter dueDateAdapter;
    public int SELECTED_TUITION = -1;
    public int SELECTED_CLUB = -1;
    public int SELECTED_DUEDATE = -1;
    private long selected_date = 0;

    @Inject
    Utility utility;
    @Inject
    TuitionPaymentRepository repository;
    @Inject
    StudentRepository studentRepository;

    TuitionFragContract.Presenter presenter;
    List<ActivityHistoryAccounting> yearAccountings=new ArrayList<>();

    List<Long> dueDatesList=new ArrayList<>();

    View view=null ;
    public TuitionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TuitionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TuitionFragment newInstance(int studentId) {
        TuitionFragment fragment = new TuitionFragment();
        Bundle args = new Bundle();

        args.putInt(STUDENT_ID, studentId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            studentId = getArguments().getInt(STUDENT_ID);
        }

        ClubManager.getApplicationComponent().inject(this);
        AppCompatDelegate.setDefaultNightMode(utility.getThemeNight());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         view= inflater.inflate(getLayout(), container, false);

        /*AsyncLayoutInflater inflaterAsync = new AsyncLayoutInflater(getViewContext());
        inflaterAsync.inflate(getLayout(), container, new AsyncLayoutInflater.OnInflateFinishedListener() {
            @Override
            public void onInflateFinished(@NonNull View view1, int resid, @Nullable ViewGroup parent) {
                parent.addView(view1);

                view=view1;
            }
        });*/
        ButterKnife.bind(this, view);
        presenter.attachView(this);
        setupViews();

        return view;

    }

    @Inject
    public void setPresenter(TuitionFragPresenter presenter) {
        presenter.studentId = studentId;
        this.presenter = presenter;
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_tuition;
    }

    @Override
    public void setupViews() {
        //set recycler adapter
        tuitionPaymentAdapter=new TuitionPaymentAdapter(getViewContext(),yearAccountings);

        //adapter = new TuitionPayment_Adapter(getContext(), tuitionPayments);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(tuitionPaymentAdapter);


        //set expandable layout
        btn_expan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expandableLinearLayout.toggle();
                if(expandableLinearLayout.isExpanded())
                    img_expan.setImageResource(R.drawable.ic_arrow_down);
                else
                    img_expan.setImageResource(R.drawable.ic_arrow_up);
                expandableLinearLayout.initLayout();
            }
        });



        //set tuition spinner adapter

        tuitionAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_list_item_1, getTuitionItems(tuitionList));
        sp_tuition.setAdapter(this.tuitionAdapter);

        sp_tuition.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SELECTED_TUITION = i;


                //set due date spinner adapter
                if(SELECTED_TUITION>-1) {
                    dueDateAdapter = new ArrayAdapter(getViewContext(), android.R.layout.simple_list_item_1, getDueDates(tuitionList.get(SELECTED_TUITION)));
                    sp_dueDate.setAdapter(dueDateAdapter);
                }
                expandableLinearLayout.initLayout();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        //set club spinner adapter

        clubAdapter=new ArrayAdapter(getViewContext(),android.R.layout.simple_list_item_1,getClubItems(student.getClubs()));
        sp_club.setAdapter(clubAdapter);

        sp_club.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SELECTED_CLUB=i;
                expandableLinearLayout.initLayout();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        //set due date spinner adapter
        sp_dueDate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SELECTED_DUEDATE=i;
                expandableLinearLayout.initLayout();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        if (utility.deviceLanguageDirection() == false) {
            sp_tuition.setRtl();
            sp_club.setRtl();
            sp_dueDate.setRtl();
        }



        //set pay date
        selected_date = Long.parseLong(utility.getTodaysDate(false));
        txt_date.setText(utility.farsinumber(utility.getTodaysDate(true)));
        select_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //get today's date
                PersianCalendar persianCalendar = new PersianCalendar();
                persianCalendar.setPersianDate(persianCalendar.getPersianYear(), persianCalendar.getPersianMonth(), persianCalendar.getPersianDay());

                PersianDatePickerDialog picker = new PersianDatePickerDialog(getContext())
                        .setPositiveButtonString(getString(R.string.ok))
                        .setNegativeButton(getString(R.string.cancel))
                        .setTodayButton(getString(R.string.today))
                        .setTodayButtonVisible(true)
                        .setInitDate(persianCalendar)
                        .setMaxYear(PersianDatePickerDialog.THIS_YEAR)
                        .setMinYear(1390)
                        .setListener(new Listener() {
                            @Override
                            public void onDateSelected(PersianCalendar persianCalendar) {

                                String date = utility.getDateFromPersianCalendar(persianCalendar, false);
                                selected_date = Long.parseLong(date);
                                date = utility.farsinumber(utility.getFormattedDate(Long.parseLong(date), 0));
                                txt_date.setText(date);
                            }

                            @Override
                            public void onDismissed() {

                            }
                        });

                picker.show();
            }
        });


        //add yearAccountingList payment
        add_tuition_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SELECTED_TUITION == -1)
                    utility.setError(sp_tuition, getString(R.string.unselected_value_error));
                else if(SELECTED_CLUB==-1)
                    utility.setError(sp_club, getString(R.string.unselected_value_error));
                else if(SELECTED_DUEDATE==-1)
                    utility.setError(sp_dueDate, getString(R.string.unselected_value_error));

                else if(repository.getTuitionPayment(studentId
                        ,tuitionList.get(SELECTED_TUITION).getId(),
                        student.getClubs().get(SELECTED_CLUB).getName(),
                        dueDatesList.get(SELECTED_DUEDATE))!=null)
                {
                    Toast.makeText(getViewContext(), getString(R.string.tuition_already_paid), Toast.LENGTH_SHORT).show();
                }
                else {

                    TuitionPayment tuitionPayment = new TuitionPayment(0,
                            student,
                            tuitionList.get(SELECTED_TUITION),
                            selected_date,
                            student.getClubs().get(SELECTED_CLUB),
                            dueDatesList.get(SELECTED_DUEDATE));

                    repository.save(tuitionPayment);
                    presenter.refreshData();
                    tuitionPaymentAdapter.notifyDataSetChanged();

                }
            }
        });
    }

    /**
     * get months of tuition
     * @param selected_tuition
     * @return array list of due dates
     */
    private ArrayList<String> getDueDates(Tuition selected_tuition) {

        ArrayList<String> dueDate_item = new ArrayList<>();
        if(selected_tuition!=null) {
            PersianDate start_date = utility.getPersianDate(selected_tuition.getStartDate());
            PersianDate end_date = utility.getPersianDate(selected_tuition.getEndDate());

            PersianDateFormat pdformater = new PersianDateFormat("F");


            int i = 0;
            //until end date greater than start date
            while (end_date.compareTo(start_date) == 1) {
                dueDate_item.add(utility.farsinumber(String.valueOf(start_date.getShYear())) + "  " + pdformater.format(start_date));
                dueDatesList.add(utility.getLongFromPersianDate(start_date));
                start_date = start_date.addMonth(1);
            }
            return dueDate_item;
        }
        return dueDate_item;
    }


    /**
     * get student clubs
     * @param clubs
     * @return string array club names
     */
    private String[] getClubItems(RealmList<Club> clubs) {
        String[] clubItems = new String[clubs.size()];
        for (int i=0;i<clubs.size();i++) {
            clubItems[i]=clubs.get(i).getName();
        }
        return clubItems;
    }

    /**
     * get tuition spinner items
     * @param tuitionList list<Tuition>
     * @return string array of tuition items
     */
    private String[] getTuitionItems(List<Tuition> tuitionList) {
        String[] tuitionItems = new String[tuitionList.size()];
        for (int i = 0; i < tuitionList.size(); i++) {
            tuitionItems[i] = new StringBuilder()
                    .append(utility.threeDigit(utility.farsinumber(tuitionList.get(i).getTuitionCost() + ""), ','))
                    .append("\n")
                    .append(utility.farsinumber(utility.getFormattedDate(tuitionList.get(i).getStartDate(), 0)))
                    .append(getString(R.string.to)).append(utility.farsinumber(utility.getFormattedDate(tuitionList.get(i).getEndDate(), 0))).toString();
        }

        return tuitionItems;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
        repository.close();

    }


    @Override
    public Context getViewContext() {
        return getContext();
    }


    @Override
    public void yearAccountingList(List<ActivityHistoryAccounting> yearAccountants) {
        this.yearAccountings=yearAccountants;
        //set recycler adapter
        tuitionPaymentAdapter=new TuitionPaymentAdapter(getViewContext(),yearAccountings);
        recyclerView.setAdapter(tuitionPaymentAdapter);
        tuitionPaymentAdapter.notifyDataSetChanged();
    }

    @Override
    public void tuitionList(List<Tuition> tuitionList) {
        this.tuitionList=tuitionList;
    }

    @Override
    public void student(Student student) {
        this.student=student;
    }
}
