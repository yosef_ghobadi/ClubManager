package com.nikosoft.clubmanager.StudentDetail;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.Data.DBRepository.StudentRepository;
import com.nikosoft.clubmanager.Data.Model.Student;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.StudentDetail.Fragments.Info.InfoFragment;
import com.nikosoft.clubmanager.StudentDetail.Fragments.InsurancePayment.InsuranceFragment;
import com.nikosoft.clubmanager.StudentDetail.Fragments.TuitionPayment.TuitionFragment;
import com.nikosoft.clubmanager.ViewPagerAdapter.ViewPagerAdapter;
import com.nikosoft.clubmanager.Utiles.Utility;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StudentDetailActivity extends AppCompatActivity
        implements StudentDetailContract.View {

    @BindView(R.id.student_detail_collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.student_detail_toolbar)
    Toolbar toolbar;
    @BindView(R.id.tab_student_detail)
    TabLayout tabLayout;
    @BindView(R.id.viewpager_student_detail)
    ViewPager viewPager;
    @BindView(R.id.student_detail_profile_image)
    CircularImageView profile_img;
    @BindView(R.id.txt_student_detail_name)
    TextView txt_name;
    @BindView(R.id.txt_student_detail_joindate)
    TextView txt_joindate;

    StudentDetailContract.Presenter presenter;

    private Student student;
    private int studentId = 0;


    @Inject
    Utility utility;
    @Inject
    StudentRepository studentRepository;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //get coach for show his info
        getStudent();

        ClubManager.getApplicationComponent().inject(this);

        AppCompatDelegate.setDefaultNightMode(utility.getThemeNight());

        setContentView(R.layout.activity_student_detail);

        //addDefaultValues();
        ButterKnife.bind(this);


        presenter.attachView(this);

        setupViews();
    }

    private void getStudent() {
        Bundle bundle = getIntent().getExtras();

        try {
            if (bundle != null) {
                studentId = bundle.getInt("studentId");

            } else {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Inject
    public void setPresenter(StudentDetailPresenter presenter) {
        presenter.studentId = studentId;
        this.presenter = presenter;
    }


    private void setupViews() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.title_activity_student_detail));

        collapsingToolbarLayout.setExpandedTitleTextColor(Objects.requireNonNull(ContextCompat.getColorStateList(getViewContext(), android.R.color.transparent)));


        setupViewPager(viewPager);

        tabLayout.setupWithViewPager(viewPager);

        txt_name.setText(student.getName());
        txt_joindate.setText(getString(R.string.join_date) + " : " + utility.farsinumber(utility.getFormattedDate(student.getJoinDate(), 0)));

        //set image
        if (utility.getImage(student.getImage()) == null) {
            profile_img.setImageDrawable(getViewContext().getResources().getDrawable(R.drawable.ic_account_circle));
        } else
            profile_img.setImageBitmap(utility.getImage(student.getImage()));

        setActivityColor(student.isActivityStatus());
    }

    public void setActivityColor(boolean status) {
        if (status)
            profile_img.setBorderColor(getResources().getColor(R.color.green));
        else
            profile_img.setBorderColor(getResources().getColor(R.color.red_light));

    }

    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(InfoFragment.newInstance(studentId), getString(R.string.info));
        adapter.addFragment(InsuranceFragment.newInstance(studentId), getString(R.string.insurance));
        adapter.addFragment(TuitionFragment.newInstance(studentId), getString(R.string.tuition));
        viewPager.setAdapter(adapter);
    }


    @Override
    public Context getViewContext() {
        return this;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
        studentRepository.close();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_student_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.student_detail_call:
                Intent intent_call = new Intent(Intent.ACTION_DIAL);
                intent_call.setData(Uri.parse("tel:" + student.getPhone()));
                startActivity(intent_call);
                break;

            case R.id.student_detail_sms:
                Intent intent_sms = new Intent(Intent.ACTION_VIEW);
                intent_sms.setData(Uri.parse("sms:" + student.getPhone()));
                startActivity(intent_sms);
                break;

            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }

    @Override
    public void student(Student student) {
        this.student = student;
    }
}