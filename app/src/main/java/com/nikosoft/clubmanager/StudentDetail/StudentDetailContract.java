package com.nikosoft.clubmanager.StudentDetail;

import com.nikosoft.clubmanager.Base.BasePresenter;
import com.nikosoft.clubmanager.Base.BaseView;
import com.nikosoft.clubmanager.Data.Model.Student;

public interface StudentDetailContract {

    public interface View extends BaseView{
        void student(Student student);


    }

    public interface Presenter extends BasePresenter<View>
    {
        void getStudent(int studentId);




    }
}
