package com.nikosoft.clubmanager.StudentDetail;

import com.nikosoft.clubmanager.Data.DataSource;

import javax.inject.Inject;

public class StudentDetailPresenter implements StudentDetailContract.Presenter {

    StudentDetailContract.View view;
    DataSource dataSource;

    public int studentId;

    @Inject
    public StudentDetailPresenter(DataSource dataSource) {
        this.dataSource = dataSource;
    }



    @Override
    public void attachView(StudentDetailContract.View view) {
        this.view=view;
        getStudent(studentId);
    }

    @Override
    public void detachView() {
        this.view=null;
    }

    @Override
    public void getStudent(int studentId) {
        view.student(dataSource.getStudent(studentId));
    }
}
