package com.nikosoft.clubmanager.StudentModify;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.balysv.materialripple.MaterialRippleLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.ClubModify.ClubModifyActivity;
import com.nikosoft.clubmanager.Coach.CoachActivity;
import com.nikosoft.clubmanager.Data.DBRepository.ClubRepository;
import com.nikosoft.clubmanager.Data.DBRepository.CoachRepository;
import com.nikosoft.clubmanager.Data.DBRepository.FieldRepository;
import com.nikosoft.clubmanager.Data.DBRepository.StudentRepository;
import com.nikosoft.clubmanager.Data.Model.Club;
import com.nikosoft.clubmanager.Data.Model.Coach;
import com.nikosoft.clubmanager.Data.Model.Field;
import com.nikosoft.clubmanager.Data.Model.Student;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.Student.StudentActivity;
import com.nikosoft.clubmanager.Utiles.PersianDateModel;
import com.nikosoft.clubmanager.Utiles.Utility;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.ganfra.materialspinner.MaterialSpinner;
import io.realm.RealmList;

public class StudentModifyActivity extends AppCompatActivity implements StudentModifyContract.View {


    @BindView(R.id.student_profile_image)
    CircularImageView profileImage;
    @BindView(R.id.student_choose_image_fab)
    FloatingActionButton fab_picture;
    @BindView(R.id.txt_student_name)
    EditText txt_name;
    @BindView(R.id.txt_student_desc)
    EditText txt_desc;
    @BindView(R.id.txt_student_phone)
    EditText txt_phone;
    @BindView(R.id.birth_date_year_student)
    AppCompatSpinner sp_birthdate_year;
    @BindView(R.id.birth_date_month_student)
    AppCompatSpinner sp_birthdate_month;
    @BindView(R.id.birth_date_day_student)
    AppCompatSpinner sp_birthdate_day;
    @BindView(R.id.join_date_year_student)
    AppCompatSpinner sp_joindate_year;
    @BindView(R.id.join_date_month_student)
    AppCompatSpinner sp_joindate_month;
    @BindView(R.id.join_date_day_student)
    AppCompatSpinner sp_joindate_day;
    @BindView(R.id.student_clubs)
    TextView student_clubs;
    @BindView(R.id.txt_birthday_student_caption)
    TextView birthdate_caption;
    @BindView(R.id.txt_join_date_student_caption)
    TextView joindate_caption;
    @BindView(R.id.txt_clubs_student_caption)
    TextView clubs_caption;
    @BindView(R.id.sp_student_field)
    MaterialSpinner student_field;
    @BindView(R.id.student_coaches)
    TextView student_coaches;
    @BindView(R.id.lay_add_student)
    MaterialRippleLayout add_student;
    @BindView(R.id.student_toolbar)
    Toolbar toolbar;
    @BindView(R.id.student_collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;

    private ArrayAdapter<String> arr_birthdate_year;
    private ArrayAdapter<String> arr_birthdate_month;
    private ArrayAdapter<String> arr_birthdate_day;

    private ArrayAdapter<String> arr_joinDate_year;
    private ArrayAdapter<String> arr_joinDate_month;
    private ArrayAdapter<String> arr_joinDate_day;

    private ArrayAdapter<String> arr_field;
    private String[] field_items;

    private Uri mCropImageUri;

    List<Field> FieldList = new ArrayList<>();
    boolean[] checkedFields = new boolean[0];
    List<Club> ClubList = new ArrayList<>();
    boolean[] checkedClubs = new boolean[0];
    List<Coach> CoachList = new ArrayList<>();
    boolean[] checkedCoaches = new boolean[0];
    private String imageUrl = null;

    //for detecting edit coach or add new
    private boolean EDIT_MODE = false;

    private List<String> birthYearsList = new ArrayList<>();
    private List<String> monthsList = new ArrayList<>();
    private List<String> daysList = new ArrayList<>();
    private List<String> joinYearsList = new ArrayList<>();

    StudentModifyContract.Presenter presenter;
    @Inject
    Utility utility;
    @Inject
    CoachRepository coachRepository;
    @Inject
    FieldRepository fieldRepository;
    @Inject
    ClubRepository clubRepository;
    @Inject
    StudentRepository studentRepository;


    private Student student;
    private int studentId;
    public int SELECTED_FIELD = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ClubManager.getApplicationComponent().inject(this);

        AppCompatDelegate.setDefaultNightMode(utility.getThemeNight());



        setContentView(R.layout.activity_student_modify);
        ButterKnife.bind(this);
        presenter.attachView(this);

        //get the coach id for edit
        Bundle bundle = getIntent().getExtras();

        try {
            if (bundle != null) {
                studentId = bundle.getInt("studentId");
                EDIT_MODE = true;
                student = studentRepository.get(studentId);
                //activityHistory=historyRepository.getByStudent(studentId,true);
            } else {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        //load soinners adapter
        new getAdaptersData().execute(1);
        new getAdaptersData().execute(2);
        new getAdaptersData().execute(3);
        new getAdaptersData().execute(4);

        //set drawable to edittexts and textviews
        setViewsDrawable();


        setupViews();




    }

    /**
     * setup activities views
     */
    private void setupViews() {

        //Toolbar
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.register_student));

        collapsingToolbarLayout.setExpandedTitleTextColor(Objects.requireNonNull(ContextCompat.getColorStateList(getViewContext(), android.R.color.transparent)));

        //choose image from gallery or camera
        fab_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSelectImageClick(view);
            }
        });

        if (EDIT_MODE) {
            //set image
            if (student.getImage() != null) {
                profileImage.setImageBitmap(utility.getImage(student.getImage()));
                imageUrl = student.getImage();
            }

            //set name and family
            txt_name.setText(student.getName());

            //set phone
            txt_phone.setText(student.getPhone());
        }


        //The clubs that the coach operates
        student_clubs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getViewContext(), ClubModifyActivity.class);
                AlertDialog d = showClubMultiChoiceDialog(getViewContext(),
                        getString(R.string.select_clubs), ClubList, student_clubs, checkedClubs, intent).create();
                d.show();


            }
        });

        //set the coach clubs
        if (EDIT_MODE) {

            if (student.getClubs().size() > 0) {
                student_clubs.setText("");
                for (Club item : student.getClubs()) {
                    student_clubs.setText(student_clubs.getText() + item.getName() + ",");
                    for (int i = 0; i < ClubList.size(); i++) {
                        if (item.getId() == ClubList.get(i).getId()) {
                            checkedClubs[i] = true;
                        }
                    }
                }
                //برای حذف ویرگول اخر
                student_clubs.setText(student_clubs.getText().subSequence(0, student_clubs.getText().length() - 1));

            }
        }


        //set field spinner
        arr_field = new ArrayAdapter<>(getViewContext(), android.R.layout.simple_list_item_1);

        if (EDIT_MODE)
            setFields();


        if (!utility.deviceLanguageDirection())
            student_field.setRtl();


        student_field.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SELECTED_FIELD = i;

                //set selected fields coaches to show coach dialog
                setCoaches(i);

                setCoachesEditMode();
                setSelectedCoaches(checkedCoaches,student_coaches,CoachList);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        //coaches that works with the coach
        student_coaches.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getViewContext(), CoachActivity.class);

                AlertDialog d = showCoachMultiChoiceDialog(getViewContext(),
                        getString(R.string.select_coaches), CoachList, student_coaches, checkedCoaches, intent).create();
                d.show();
            }
        });


        //set description
        if (EDIT_MODE)
            txt_desc.setText(student.getDescription());


        //Save coach
        add_student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //empty name
                if (utility.isEmpty(txt_name)) {
                    utility.setError(txt_name, getString(R.string.emtpy_value_error));
                    return;
                }
                //exsit name
                else {
                    if (EDIT_MODE == false) {
                        for (Student item : presenter.getStudents()) {
                            if (item.getName().equals(txt_name.getText().toString())) {
                                utility.setError(txt_name, getString(R.string.exist_value_error));
                                return;
                            }
                        }
                    }
                }
                //empty phone
                if (utility.isEmpty(txt_phone)) {
                    utility.setError(txt_phone, getString(R.string.emtpy_value_error));
                    return;
                }
                //check digit count
                else if (txt_phone.getText().length() < 11) {
                    utility.setError(txt_phone, getString(R.string.phone_digit_length_error));
                    return;
                }
                //exsit phone
                else {
                    if (EDIT_MODE == false) {
                        for (Student item : presenter.getStudents()) {
                            if (item.getPhone().equals(txt_phone.getText().toString())) {
                                utility.setError(txt_phone, getString(R.string.exist_value_error));
                                return;
                            }
                        }
                    }
                }
                if (student_clubs.getText().equals(getString(R.string.choose))) {
                    utility.setError(student_clubs, getString(R.string.unselected_value_error));
                    return;
                } else if (SELECTED_FIELD == -1) {
                    utility.setError(student_field, getString(R.string.unselected_value_error));
                    return;
                } else if (student_coaches.getText().equals(getString(R.string.choose))) {
                    utility.setError(student_coaches, getString(R.string.unselected_value_error));
                    return;
                }

                //save coach
                else {

                    studentId=studentRepository.save(new Student(
                            studentId,
                            txt_name.getText().toString(),
                            Long.parseLong(sp_birthdate_year.getSelectedItem().toString() + sp_birthdate_month.getSelectedItem().toString() + sp_birthdate_day.getSelectedItem().toString()),
                            Long.parseLong(sp_joindate_year.getSelectedItem().toString() + sp_joindate_month.getSelectedItem().toString() + sp_joindate_day.getSelectedItem().toString()),
                            utility.removeSpaces(txt_phone.getText().toString()),
                            imageUrl,
                            txt_desc.getText().toString(),
                            new genericsModel<Club>().getSelectedItems(checkedClubs, ClubList),
                            new genericsModel<Coach>().getSelectedItems(checkedCoaches, CoachList),
                            FieldList.get(SELECTED_FIELD),
                            true
                    ));

                    /*//save student activity history
                    if(EDIT_MODE)
                    {
                        historyRepository.update(new StudentActivityHistory(
                                (activityHistory==null)?0:activityHistory.getId(),
                                Long.parseLong(sp_joindate_year.getSelectedItem().toString() + sp_joindate_month.getSelectedItem().toString() + sp_joindate_day.getSelectedItem().toString()),
                                0,
                                studentId
                        ));
                    }
                    else{
                        historyRepository.save(new StudentActivityHistory(
                                0,
                                Long.parseLong(sp_joindate_year.getSelectedItem().toString() + sp_joindate_month.getSelectedItem().toString() + sp_joindate_day.getSelectedItem().toString()),
                                0,
                                studentId
                                ),true);
                    }*/

                    Intent intent = new Intent(getViewContext(), StudentActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    @Inject
    public void setPresenter(StudentModifyPresenter presenter) {
        this.presenter = presenter;
    }

    /**
     * set edittext and textview drawable
     */
    private void setViewsDrawable() {
        utility.setViewDrawable(txt_name, getViewContext(), R.drawable.ic_account_box, 4, 10);
        utility.setViewDrawable(txt_phone, getViewContext(), R.drawable.ic_phone, 4, 10);
        utility.setViewDrawable(txt_desc, getViewContext(), R.drawable.ic_description, 4, 10);
        utility.setViewDrawable(birthdate_caption, getViewContext(), R.drawable.ic_birthday, 4, 10);
        utility.setViewDrawable(joindate_caption, getViewContext(), R.drawable.ic_join, 4, 10);
        utility.setViewDrawable(clubs_caption, getViewContext(), R.drawable.ic_gym, 4, 10);

    }

    /**
     * Start pick image activity with chooser.
     *
     * @param view
     */
    public void onSelectImageClick(View view) {
        CropImage.startPickImageActivity(this);
    }


    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), result.getUri());
                    if (EDIT_MODE)
                        utility.deleteFile(imageUrl);
                    imageUrl = utility.saveImage(bitmap, getViewContext());
                    profileImage.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }


                //Toast.makeText(this, "Cropping successful, Sample: " + result.getSampleSize(), Toast.LENGTH_LONG).show();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                //Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // required permissions granted, start crop image activity
            startCropImageActivity(mCropImageUri);
        } else {
            Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .start(this);
    }


    private void setCoaches(int id) {
        CoachList = new ArrayList<>();
        if (FieldList.size() > 0 & id>-1) {
            CoachList = FieldList.get(id).getCoaches();
            checkedCoaches = new boolean[CoachList.size()];
        }
    }


    private void setCoachesEditMode() {

        //set the coach coaches
        if(student!=null){
        if (student.getCoaches().size() > 0) {
            //student_coaches.setText("");
            for (Coach item : student.getCoaches()) {
                //student_coaches.setText(student_coaches.getText() + item.getName() + ",");
                for (int i = 0; i < CoachList.size(); i++) {
                    if (item.getId() == CoachList.get(i).getId()) {
                        checkedCoaches[i] = true;
                    }
                }
            }
        }
            //برای حذف ویرگول اخر
            //student_coaches.setText(student_coaches.getText().subSequence(0, student_coaches.getText().length() - 1));
        } //else
            //student_coaches.setText(getString(R.string.choose));
    }


    //set selected coaches by user
    private void setSelectedCoaches(boolean[] checkedItems, TextView textView, List<Coach> itemsName) {
        textView.setText("");

        //مشخص کردن گزینه های انتخاب شده از روی ارایه check
        for (int j = 0; j < checkedItems.length; j++) {
            if (checkedItems[j]) {
                if(itemsName.size()>0)
                textView.setText(textView.getText() + itemsName.get(j).getName() + " ,");
            }
        }

        //برای حذف وبرگول اضافی آخر
        if (textView.getText().length() == 0)
            textView.setText(R.string.choose);
        else
            textView.setText(textView.getText().subSequence(0, textView.getText().length() - 1));
    }

        //set selected coaches by user
    private void setSelectedCoaches(boolean[] checkedItems,boolean[] checkedItems_temp, TextView textView, List<Coach> itemsName) {
        textView.setText("");

        //مشخص کردن گزینه های انتخاب شده از روی ارایه check
        for (int j = 0; j < checkedItems_temp.length; j++) {
            checkedItems[j] = checkedItems_temp[j];
            if (checkedItems_temp[j]) {
                textView.setText(textView.getText() + itemsName.get(j).getName() + " ,");
            }
        }

        //برای حذف وبرگول اضافی آخر
        if (textView.getText().length() == 0)
            textView.setText(R.string.choose);
        else
            textView.setText(textView.getText().subSequence(0, textView.getText().length() - 1));
    }




    private void setFields() {
        FieldList = getClubsFields(checkedClubs, ClubList);
        arr_field = new ArrayAdapter<>(getViewContext(), android.R.layout.simple_spinner_dropdown_item, field_items);
        student_field.setAdapter(arr_field);
        if (EDIT_MODE) {
            if(student.getField()!=null) {
                student_field.setSelection(arr_field.getPosition(student.getField().getTitle()) + 1);
                SELECTED_FIELD = arr_field.getPosition(student.getField().getTitle());
                //setCoaches();
            }
        }
    }


    /**
     * show dialog for choose clubs
     *
     * @param context
     * @param title        dialog title
     * @param itemsName    clubs names
     * @param textView     textview that shows clubs name after selection
     * @param checkedItems save checked clubs
     * @param intent       start clubs activity for insert
     * @return
     */
    private AlertDialog.Builder showClubMultiChoiceDialog(Context context, String title,
                                                          List<Club> itemsName,
                                                          TextView textView,
                                                          boolean[] checkedItems,
                                                          Intent intent) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setCancelable(false);

        //ارایه ای برای نمایش نام ایتم ها
        int itemsSize = 0;

        if (itemsName != null)
            itemsSize = itemsName.size();

        String[] filds = new String[itemsSize];
        boolean[] checkedItems_temp =new boolean[itemsSize];

        if (itemsName != null) {
            for (int i = 0; i < itemsSize; i++) {
                filds[i] = itemsName.get(i).getName();
                checkedItems_temp[i] = checkedItems[i];
            }
        }

        builder.setMultiChoiceItems(filds, checkedItems_temp, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i, boolean b) {
                checkedItems_temp[i] = b;
            }
        });

        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                textView.setText("");

                //مشخص کردن گزینه های انتخاب شده از روی ارایه check
                for (int j = 0; j < checkedItems_temp.length; j++) {
                    checkedItems[j] = checkedItems_temp[j];
                    if (checkedItems_temp[j]) {

                        textView.setText(textView.getText() + itemsName.get(j).getName() + " ,");
                    }
                }

                //برای حذف وبرگول اضافی آخر
                if (textView.getText().length() == 0)
                    textView.setText(R.string.choose);
                else
                    textView.setText(textView.getText().subSequence(0, textView.getText().length() - 1));

                //setting spinner field adapter and reset previous selected field
                setFields();


            }
        });
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                for (boolean item:checkedItems) {
                    item=false;
                }
            }
        });
        builder.setNeutralButton(R.string.add, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivity(intent);
            }
        });
        builder.setCancelable(true);
        return builder;
    }


    /**
     * show dialog for choose coaches
     *
     * @param context
     * @param title        dialog title
     * @param itemsName    fields name
     * @param textView     textview that shows fields name after selection
     * @param checkedItems save checked fields
     * @param intent       start coaches activity for insert fields
     * @return
     */
    private AlertDialog.Builder showCoachMultiChoiceDialog(Context context, String title,
                                                           List<Coach> itemsName,
                                                           TextView textView,
                                                           boolean[] checkedItems,
                                                           Intent intent) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setCancelable(false);

        //ارایه ای برای نمایش نام ایتم ها
        int itemsSize = 0;

        if (itemsName != null)
            itemsSize = itemsName.size();

        String[] filds = new String[itemsSize];
        boolean[] checkedItems_temp =new boolean[itemsSize];

        if (itemsName != null) {
            for (int i = 0; i < itemsSize; i++) {
                filds[i] = itemsName.get(i).getName();
                checkedItems_temp[i] = checkedItems[i];
            }
        }

        builder.setMultiChoiceItems(filds, checkedItems_temp, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i, boolean b) {
                checkedItems_temp[i] = b;
            }
        });

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                setSelectedCoaches(checkedItems,checkedItems_temp,textView,itemsName);

            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                for (boolean item:checkedItems) {
                    item=false;
                }
            }
        });
        builder.setNeutralButton(R.string.add, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivity(intent);
            }
        });
        builder.setCancelable(true);
        return builder;
    }








    /**
     * back arrow action
     *
     * @return
     */
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    /**
     * get selected clubs fields
     *
     * @param checkedItems
     * @param clubs
     * @return
     */
    private List<Field> getClubsFields(boolean[] checkedItems, List<Club> clubs) {
        List<Club> clubList = new genericsModel<Club>().getSelectedItems(checkedItems, clubs);
        List<Field> fieldList = new ArrayList<>();
        for (Club item : clubList) {
            for(int i=0;i<item.getFields().size();i++) {
                if (!fieldList.contains(item.getFields().get(i)))
                    fieldList.add(item.getFields().get(i));
            }
        }

        //set fields titles for show in spinner
        field_items = new String[fieldList.size()];
        for (int i = 0; i < fieldList.size(); i++) {
            field_items[i] = fieldList.get(i).getTitle();
        }
        return fieldList;
    }


    @Override
    public void clubList(List<Club> clubs) {
        ClubList.addAll(clubs);
        checkedClubs = new boolean[clubs.size()];
    }



    @Override
    public Context getViewContext() {
        return this;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
        studentRepository.close();
        clubRepository.close();
        coachRepository.close();
        fieldRepository.close();
    }


    /**
     * laod all adapter in background thread
     */
    private class getAdaptersData extends AsyncTask<Integer, Void, Integer> {


        @Override
        protected Integer doInBackground(Integer... integers) {
            switch (integers[0]) {
                case 1:
                    daysList = utility.getDays();
                    break;
                case 2:
                    monthsList = utility.getMonths();
                    break;
                case 3:
                    joinYearsList = utility.getJoinYears();
                    break;
                case 4:
                    birthYearsList = utility.getBirthYears();
                    break;

            }

            return integers[0];
        }


        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);

            PersianDateModel birthDate = new PersianDateModel();
            PersianDateModel joinDate = new PersianDateModel();
            //set birth date
            if (EDIT_MODE) {
                birthDate = utility.getPersianDateModel(student.getBirthDate());
                joinDate = utility.getPersianDateModel(student.getJoinDate());
            }

            switch (integer) {
                case 1:
                    arr_birthdate_day = new ArrayAdapter<String>(getViewContext(), android.R.layout.simple_spinner_dropdown_item, daysList);
                    sp_birthdate_day.setAdapter(arr_birthdate_day);


                    arr_joinDate_day = new ArrayAdapter<String>(getViewContext(), android.R.layout.simple_spinner_dropdown_item, daysList);
                    sp_joindate_day.setAdapter(arr_joinDate_day);

                    //setting for values for edit if exist
                    if (EDIT_MODE) {
                        sp_birthdate_day.setSelection(arr_birthdate_day.getPosition(birthDate.getDay()));
                        sp_joindate_day.setSelection(arr_joinDate_day.getPosition(joinDate.getDay()));
                    }

                    break;
                case 2:

                    arr_birthdate_month = new ArrayAdapter<String>(getViewContext(), android.R.layout.simple_spinner_dropdown_item, monthsList);
                    sp_birthdate_month.setAdapter(arr_birthdate_month);

                    arr_joinDate_month = new ArrayAdapter<String>(getViewContext(), android.R.layout.simple_spinner_dropdown_item, monthsList);
                    sp_joindate_month.setAdapter(arr_joinDate_month);

                    if (EDIT_MODE) {
                        sp_birthdate_month.setSelection(arr_birthdate_month.getPosition(birthDate.getMonth()));
                        sp_joindate_month.setSelection(arr_joinDate_month.getPosition(joinDate.getMonth()));
                    }

                    break;
                case 3:

                    arr_joinDate_year = new ArrayAdapter<String>(getViewContext(), android.R.layout.simple_spinner_dropdown_item, joinYearsList);
                    sp_joindate_year.setAdapter(arr_joinDate_year);

                    if (EDIT_MODE) {
                        sp_joindate_year.setSelection(arr_joinDate_year.getPosition(joinDate.getYear()));
                    }

                    break;
                case 4:

                    arr_birthdate_year = new ArrayAdapter<String>(getViewContext(), android.R.layout.simple_spinner_dropdown_item, birthYearsList);
                    sp_birthdate_year.setAdapter(arr_birthdate_year);

                    if (EDIT_MODE)
                        sp_birthdate_year.setSelection(arr_birthdate_year.getPosition(birthDate.getYear()));
                    else
                        sp_birthdate_year.setSelection(sp_birthdate_year.getCount() / 2);

                    break;

            }
        }
    }


    /**
     * generic class for avoid duplicate methods
     *
     * @param <T>
     */
    public class genericsModel<T> {

        public RealmList<T> getSelectedItems(boolean[] checkedItems, List<T> objects) {
            RealmList<T> selectedObjects = new RealmList<>();

            for (int i = 0; i < checkedItems.length; i++) {
                if (checkedItems[i])
                    selectedObjects.add(objects.get(i));
            }

            return selectedObjects;
        }
    }


}

