package com.nikosoft.clubmanager.StudentModify;

import com.nikosoft.clubmanager.Base.BasePresenter;
import com.nikosoft.clubmanager.Base.BaseView;
import com.nikosoft.clubmanager.Data.Model.Club;
import com.nikosoft.clubmanager.Data.Model.Student;

import java.util.List;

public interface StudentModifyContract {

    public interface View extends BaseView{
        void clubList(List<Club> clubs);

    }

    public interface Presenter extends BasePresenter<View>
    {
        void getClubs();

        List<Student> getStudents();
    }
}
