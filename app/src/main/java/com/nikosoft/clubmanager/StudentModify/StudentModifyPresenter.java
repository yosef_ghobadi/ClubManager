package com.nikosoft.clubmanager.StudentModify;

import com.nikosoft.clubmanager.Data.DataSource;
import com.nikosoft.clubmanager.Data.Model.Student;

import java.util.List;

import javax.inject.Inject;

public class StudentModifyPresenter implements StudentModifyContract.Presenter {

    StudentModifyContract.View view;
    DataSource dataSource;

    @Inject
    public StudentModifyPresenter(DataSource dataSource) {
        this.dataSource=dataSource;
    }

    @Override
    public void getClubs() {
        view.clubList(dataSource.getClubs());
    }



    @Override
    public List<Student> getStudents() {
        return dataSource.getStudents();
    }

    @Override
    public void attachView(StudentModifyContract.View view) {
        this.view=view;
        getClubs();

    }

    @Override
    public void detachView() {
        this.view=null;
    }
}
