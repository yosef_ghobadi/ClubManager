package com.nikosoft.clubmanager.Tuition;

import com.nikosoft.clubmanager.Data.Model.Tuition;

public interface OnTuitionEditMenuClickListener {
    void onEditMenuClick(Tuition tuition);

}
