package com.nikosoft.clubmanager.Tuition;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.nikosoft.clubmanager.Adapters.Tuition_Adapter;
import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.Data.DBRepository.TuitionRepository;
import com.nikosoft.clubmanager.Data.Model.Tuition;
import com.nikosoft.clubmanager.R;
import com.nikosoft.clubmanager.Utiles.Utility;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.hamsaa.persiandatepicker.Listener;
import ir.hamsaa.persiandatepicker.PersianDatePickerDialog;
import ir.hamsaa.persiandatepicker.util.PersianCalendar;

public class TuitionActivity extends AppCompatActivity implements TuitionContract.View, OnTuitionEditMenuClickListener {


    @BindView(R.id.txt_tuition)
    EditText txt_tuition;
    @BindView(R.id.lay_add_tuition)
    MaterialRippleLayout add_tuition;
    @BindView(R.id.recy_tuitions)
    RecyclerView recycler_tuitions;
    @BindView(R.id.txt_end_date)
    TextView txt_endDate;
    @BindView(R.id.txt_start_date)
    TextView txt_startDate;

    TuitionContract.Presenter presenter;
    @Inject
    Utility utility;
    @Inject
    TuitionRepository tuitionRepository;


    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;
    List<Tuition> tuitionList;
    public static int tuitionID = 0;
    private long startDate=0;
    private long endDate=0;
    private boolean EDIT_MODE=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //dependency injection by dagger2
        ClubManager.getApplicationComponent().inject(this);
        //set app theme
        AppCompatDelegate.setDefaultNightMode(utility.getThemeNight());

        setContentView(R.layout.activity_tuition);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        presenter.attachView(this);

        setupViews();


    }

    /**
     * setup activitys views
     */
    private void setupViews() {

        //tuition cost
        txt_tuition.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                utility.handleNumberInput(txt_tuition, this);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        //start date
        txt_startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //get today's date
                PersianCalendar persianCalendar = new PersianCalendar();
                persianCalendar.setPersianDate(persianCalendar.getPersianYear(), 1, 1);

                PersianDatePickerDialog picker = new PersianDatePickerDialog(getViewContext())
                        .setPositiveButtonString(getString(R.string.ok))
                        .setNegativeButton(getString(R.string.cancel))
                        .setTodayButton(getString(R.string.today))
                        .setTodayButtonVisible(true)
                        .setInitDate(persianCalendar)
                        .setMaxYear(PersianDatePickerDialog.THIS_YEAR)
                        .setMinYear(1390)
                        .setListener(new Listener() {
                            @Override
                            public void onDateSelected(PersianCalendar persianCalendar) {

                                String date = utility.getDateFromPersianCalendar(persianCalendar, false);
                                startDate = Long.parseLong(date);
                                date = utility.farsinumber(utility.getFormattedDate(Long.parseLong(date), 0));
                                txt_startDate.setText(date);
                            }

                            @Override
                            public void onDismissed() {

                            }
                        });

                picker.show();
            }
        });


        //end date
        txt_endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //get today's date
                PersianCalendar persianCalendar = new PersianCalendar();
                persianCalendar.setPersianDate(persianCalendar.getPersianYear(), 12, 29);

                PersianDatePickerDialog picker = new PersianDatePickerDialog(getViewContext())
                        .setPositiveButtonString(getString(R.string.ok))
                        .setNegativeButton(getString(R.string.cancel))
                        .setTodayButton(getString(R.string.today))
                        .setTodayButtonVisible(true)
                        .setInitDate(persianCalendar)
                        .setMaxYear(PersianDatePickerDialog.THIS_YEAR+1)
                        .setMinYear(1390)
                        .setListener(new Listener() {
                            @Override
                            public void onDateSelected(PersianCalendar persianCalendar) {

                                String date = utility.getDateFromPersianCalendar(persianCalendar, false);
                                endDate = Long.parseLong(date);
                                date = utility.farsinumber(utility.getFormattedDate(Long.parseLong(date), 0));
                                txt_endDate.setText(date);
                            }

                            @Override
                            public void onDismissed() {

                            }
                        });

                picker.show();
            }
        });


        //tuitions recycler
        layoutManager = new LinearLayoutManager(getViewContext());

        recycler_tuitions.setLayoutManager(layoutManager);

        adapter = new Tuition_Adapter(getViewContext(), tuitionList, this::onEditMenuClick);

        recycler_tuitions.setAdapter(adapter);


        //add or update field
        add_tuition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //check empty title
                if (utility.isEmpty(txt_tuition)) {
                    utility.setError(txt_tuition, getString(R.string.emtpy_value_error));
                    utility.setError(txt_startDate, null);
                    utility.setError(txt_endDate, null);
                    return;
                }

                //check empty start date
                if (startDate==0) {
                    utility.setError(txt_startDate, getString(R.string.emtpy_value_error));
                    utility.setError(txt_tuition, null);
                    utility.setError(txt_endDate, null);
                    return;
                }

                //check empty end date
                if (endDate==0) {
                    utility.setError(txt_endDate, getString(R.string.emtpy_value_error));
                    utility.setError(txt_startDate, null);
                    utility.setError(txt_tuition, null);
                    return;
                }

                //check start date and end date
                if (endDate<startDate) {
                    utility.setError(txt_endDate, getString(R.string.end_date_less_start_date_error));
                    utility.setError(txt_startDate, null);
                    utility.setError(txt_tuition, null);
                    return;
                }

                //check exist value
                if(EDIT_MODE==false) {
                    for (Tuition item : tuitionList) {
                        if (utility.isConflict(item.getStartDate(), item.getEndDate(), startDate, endDate)) {
                            Toast.makeText(TuitionActivity.this, R.string.conflict_tuition_date, Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                }


                tuitionRepository.save(new Tuition(tuitionID, Integer.parseInt(utility.removeSpaces(txt_tuition.getText().toString())),startDate,endDate));
                adapter.notifyDataSetChanged();
                txt_tuition.setText("");
                txt_startDate.setText("");
                txt_endDate.setText("");
                tuitionID = 0;
                EDIT_MODE=false;

            }
        });

    }


    @Inject
    public void setPresenter(TuitionPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void tuitionList(List<Tuition> tuitions) {
        this.tuitionList = tuitions;
    }

    @Override
    public Context getViewContext() {
        return this;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }


    @Override
    public void onEditMenuClick(Tuition tuition) {
        txt_tuition.setText(String.valueOf(tuition.getTuitionCost()));
        txt_startDate.setText(utility.farsinumber(utility.getFormattedDate(tuition.getStartDate(), 0)));
        txt_endDate.setText(utility.farsinumber(utility.getFormattedDate(tuition.getEndDate(), 0)));
        startDate=tuition.getStartDate();
        endDate=tuition.getEndDate();
        tuitionID = tuition.getId();
        EDIT_MODE=true;
    }


}
