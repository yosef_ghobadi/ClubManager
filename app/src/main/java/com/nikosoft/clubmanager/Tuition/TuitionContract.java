package com.nikosoft.clubmanager.Tuition;

import com.nikosoft.clubmanager.Base.BasePresenter;
import com.nikosoft.clubmanager.Base.BaseView;
import com.nikosoft.clubmanager.Data.Model.Tuition;

import java.util.List;

public interface TuitionContract {

    public interface View extends BaseView {
        void tuitionList(List<Tuition> tuitions);
    }

    public interface Presenter extends BasePresenter<View>
    {
        void getTuitionList();
    }
}
