package com.nikosoft.clubmanager.Tuition;

import com.nikosoft.clubmanager.Data.DataSource;

import javax.inject.Inject;

public class TuitionPresenter implements TuitionContract.Presenter {

    private TuitionContract.View view;
    private DataSource dataSource;


    @Inject
    public TuitionPresenter( DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void getTuitionList() {
        view.tuitionList(dataSource.getTuitions());

    }

    @Override
    public void attachView(TuitionContract.View view) {
        this.view = view;
        getTuitionList();
    }

    @Override
    public void detachView() {
        this.view = null;
    }
}
