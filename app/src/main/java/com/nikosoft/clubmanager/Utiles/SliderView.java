package com.nikosoft.clubmanager.Utiles;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.nikosoft.clubmanager.R;

/**
 * Created by Yosef on 26/01/2019.
 */

public class SliderView extends BaseSliderView {
    public SliderView(Context context) {
        super(context);
    }

    @Override
    public View getView() {
        View v = LayoutInflater.from(getContext()).inflate(R.layout.slider_view,null);
        ImageView target = v.findViewById(R.id.img_slide);
        bindEventAndShow(v, target);
        return v;
    }
}
