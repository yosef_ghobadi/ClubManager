package com.nikosoft.clubmanager.Utiles;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.text.TextUtilsCompat;
import androidx.core.view.ViewCompat;

import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import dagger.Module;
import fr.ganfra.materialspinner.MaterialSpinner;
import ir.hamsaa.persiandatepicker.util.PersianCalendar;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;

/**
 * Created by Yosef on 11/02/2019.
 */
@Module
public class Utility {

    public String Farsi = "فارسی";
    public String dayNight = "DayOrNight";
    public String DEFAULT_MESSAGE="default_message";
    public String PHONE_NUMBER="phone";
    public String FIRST_RUN="firstRun";

    //yearly subscription id
    public String YEARLY_SUB="yearly_subscription";
    //monthly subscription id
    public String MONTHLY_SUB="monthly_subscription";
    //limit for adding students for free version
    public  final int LIMIT_COUNT=15;
    //for detecting app validation
    public String IS_VALID_APP="valid_app";
    //for connecting to internet after some days
    public String OFFLINE_LIMIT="offline_limit";
    public int OFFLINE_LIMIT_COUNT=10;
    //save new version code
    public String NEW_VERSION="new_version";
    @Inject
    public Context context;



    @Inject
    public Utility() {
        ClubManager.getApplicationComponent().inject(this);
    }



    /**
     * get bazar store app key
     * @param context context
     * @return return RSA key
     */
    public String getRSAKey(Context context)
    {
        return context.getString(R.string.RSA_KEY);
    }

    /**
     * detect device language
     *
     * @return device language
     */
    
    public String deviceLanguage() {
        return Locale.getDefault().getDisplayLanguage();
    }

    /**
     * get days of months
     *
     * @return days of months list
     */
    
    public List<String> getDays() {
        List<String> days = new ArrayList<String>();

        if (deviceLanguage().equals(Farsi)) {

            for (int i = 1; i <= 31; i++) {
                if (i < 10) {
                    days.add(farsinumber("0" + i));
                } else {
                    days.add(farsinumber(i + ""));
                }
            }

        } else {
            for (int i = 1; i <= 31; i++) {
                if (i < 10)
                    days.add("0" + i);
                else
                    days.add(i + "");
            }

        }


        return days;
    }

    /**
     * returns percentages with step 5
     *
     * @return
     */
    
    public List<String> getPercentages() {
        List<String> percentList = new ArrayList<>();
        boolean farsi = deviceLanguage().equals(Farsi);
        Log.i("IS_FARSI", farsi + "");
        for (int i = 0; i <= 100; i += 5) {
            if (farsi)
                percentList.add(farsinumber(i + ""));
            else
                percentList.add(i + "");
        }
        return percentList;
    }

    
    public List<String> getMonths() {
        List<String> months = new ArrayList<>();
        if (deviceLanguage().equals(Farsi)) {
            for (int i = 1; i <= 12; i++) {
                if (i < 10)
                    months.add(farsinumber("0" + i));
                else
                    months.add(farsinumber(i + ""));
            }
            return months;
        } else {

            for (int i = 1; i <= 12; i++) {
                if (i < 10)
                    months.add("0" + i);
                else
                    months.add(i + "");
            }
            return months;
        }
    }

    
    public List<String> getBirthYears() {
        List<String> years = new ArrayList<>();
        if (deviceLanguage().equals(Farsi)) {
            for (int i = 1320; i <= 1390; i++) {
                years.add(farsinumber(i + ""));
            }

            return years;
        } else {
            for (int i = 1930; i <= 2015; i++) {
                years.add(i + "");
            }
            return years;
        }
    }

    
    public List<String> getJoinYears() {
        List<String> years = new ArrayList<>();
        PersianDate persianDate = new PersianDate();


        if (deviceLanguage().equals(Farsi)) {
            for (int i = persianDate.getShYear(); i >= persianDate.getShYear() - 20; i--) {
                years.add(farsinumber(i + ""));
            }

            return years;
        } else {
            for (int i = persianDate.getGrgYear(); i >= persianDate.getGrgYear() - 20; i--) {
                years.add(i + "");
            }
            return years;
        }

    }

    
    public Boolean isConnectedInternet(Context context) {

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return (activeNetworkInfo != null && activeNetworkInfo.isConnected());

    }

    
    public String farsinumber(String text) {
        if (deviceLanguage().equals(Farsi)) {
            String out = text.replaceAll("1", "۱")
                    .replaceAll("2", "۲")
                    .replaceAll("3", "۳")
                    .replaceAll("4", "۴")
                    .replaceAll("5", "۵")
                    .replaceAll("6", "۶")
                    .replaceAll("7", "۷")
                    .replaceAll("8", "۸")
                    .replaceAll("9", "۹")
                    .replaceAll("0", "۰");
            return out;
        }
        return text;
    }

    
    public String threeDigit(String input, char separator) {


        if (input.length() > 3) {
            int seprate = 0;
            String output = "";
            input = input.replaceAll(" ", "");
            char[] array = input.toCharArray();
            for (int i = array.length - 1; i >= 0; i--) {
                output = array[i] + output;
                seprate++;
                if (seprate == 3) {
                    if (i != 0)
                        output = separator + output;
                    seprate = 0;
                }
            }
            return output;
        }
        return input;
    }

    /**
     * @return check sd card availabe and have free size
     */
    
    public boolean isSDCardAvailableAndHaveFreeSpace() {
        Boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
        Boolean isSDSupportedDevice = Environment.isExternalStorageRemovable();

        if (isSDSupportedDevice && isSDPresent) {
            File extdir = Environment.getExternalStorageDirectory();
            File stats = new File(extdir.getAbsolutePath());
            long availableMegaBytes = stats.getFreeSpace() / 1024 / 1024;
            Log.i("SDCARD: ", availableMegaBytes + "");
            if (availableMegaBytes > 10)
                return true;
            else
                return false;
        } else {
            return false;
        }
    }

    /**
     * saves image on sd card if exist , if not exist saves on internal storage
     *
     * @param bitmap  image file
     * @param context activity context
     * @return saved image path
     */
    
    public String saveImage(Bitmap bitmap, Context context) {
        String chidDir = "images";
        String imageName = System.currentTimeMillis() + "";


        File file = null;
        //if sd card exist create images dir in sd card
        if (isSDCardAvailableAndHaveFreeSpace()) {
            file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), chidDir);
            if (!file.exists())
                file.mkdir();
        }
        //create images dir in internal memory
        else {
            file = new File(context.getFilesDir(), chidDir);
            if (!file.exists())
                file.mkdir();
        }
        File saveImage = new File(file, imageName + ".jpg");
        //save image
        try {
            OutputStream outputStream = new FileOutputStream(saveImage);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.flush();
            outputStream.close();
            return saveImage.getAbsolutePath();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * get long date and separate by / and return shamsi date
     *
     * @param date shamsi date
     * @return string
     */
    
    public String getNormalPersianDate(long date) {
        return (date + "").substring(0, 4) + "/" + (date + "").substring(4, 6) + "/" + (date + "").substring(6, 8);
    }

    /**
     * return PersianDate from long shamsi date
     *
     * @param date long shamsi date
     * @return return PersianDateModel
     */
    
    public PersianDateModel getPersianDateModel(long date) {
        PersianDateModel persianDate = new PersianDateModel();
        if(String.valueOf(date).length()==8) {
            persianDate.setYear(farsinumber((date + "").substring(0, 4)));
            persianDate.setMonth(farsinumber((date + "").substring(4, 6)));
            persianDate.setDay(farsinumber((date + "").substring(6, 8)));
            return persianDate;
        }
        return null;
    }

    public PersianDate getPersianDate(long date) {
        PersianDate persianDate = new PersianDate();
        if(String.valueOf(date).length()==8) {
            persianDate.setShYear(Integer.parseInt((date + "").substring(0, 4)));
            persianDate.setShMonth(Integer.parseInt((date + "").substring(4, 6)));
            persianDate.setShDay(Integer.parseInt((date + "").substring(6, 8)));
            return persianDate;
        }
        return null;
    }



    /**
     * get image path and return bitmp
     *
     * @param imagePath image path
     * @return bitmap if image not exist return null
     */
    
    public Bitmap getImage(String imagePath) {
        if (imagePath != null) {
            try {
                File image = new File(imagePath);
                if (image.exists()) {
                    Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath());
                    return bitmap;
                }
                return null;
            } catch (Exception e) {
            }
        }
        return null;
    }

    
    public String removeSpaces(String s) {
        return s.replaceAll(" ", "");
    }

    /**
     * set drawable for textview and edittext
     *
     * @param editText edittext for set drawable
     * @param context  context
     * @param drawable drawable to set to edittext
     * @param position position of drawable 0=left 1=top 2=right 3=bottom 4=auto detect by languege
     * @param padding  padding of drawable from text
     */
    
    public boolean setViewDrawable(EditText editText, Context context, int drawable, int position, int padding) {
        switch (position) {
            case 0:
                editText.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(context, drawable), null, null, null);
                break;
            case 1:
                editText.setCompoundDrawablesWithIntrinsicBounds(null, AppCompatResources.getDrawable(context, drawable), null, null);
                break;
            case 2:
                editText.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(context, drawable), null);
                break;
            case 3:
                editText.setCompoundDrawablesWithIntrinsicBounds(null, null, null, AppCompatResources.getDrawable(context, drawable));
                break;
            case 4:
                if (deviceLanguage().equals(Farsi))
                    editText.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(context, drawable), null);
                else
                    editText.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(context, drawable), null, null, null);
                break;
        }

        editText.setCompoundDrawablePadding(padding);
        return true;
    }

    /**
     * set drawable for textview and edittext
     *
     * @param textView textView for set drawable
     * @param context  context
     * @param drawable drawable to set to edittext
     * @param position position of drawable 0=left 1=top 2=right 3=bottom 4=auto detect by languege
     * @param padding  padding of drawable from text
     */
    public boolean setViewDrawable(TextView textView, Context context, int drawable, int position, int padding) {
        switch (position) {
            case 0:
                textView.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(context, drawable), null, null, null);
                break;
            case 1:
                textView.setCompoundDrawablesWithIntrinsicBounds(null, AppCompatResources.getDrawable(context, drawable), null, null);
                break;
            case 2:
                textView.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(context, drawable), null);
                break;
            case 3:
                textView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, AppCompatResources.getDrawable(context, drawable));
                break;
            case 4:
                if (deviceLanguage().equals(Farsi))
                    textView.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(context, drawable), null);
                else
                    textView.setCompoundDrawablesWithIntrinsicBounds(AppCompatResources.getDrawable(context, drawable), null, null, null);
                break;

        }
        textView.setCompoundDrawablePadding(padding);
        return true;
    }


    /**
     * check edittext value
     *
     * @return
     */
    
    public boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0)
            return false;

        return true;
    }

    /**
     * set error message to view
     *
     * @param editText
     * @param message
     */
    public void setError(EditText editText, String message) {
        editText.setError(message);
        editText.requestFocus();
    }

    public void setError(MaterialSpinner spinner, String message) {
        spinner.setError(message);
        spinner.requestFocus();
    }

    public void setError(TextView textView, String message) {
        textView.setError(message);
        textView.requestFocus();
    }




    /**
     * detect device language is rtl or ltr
     *
     * @return true for ltr
     */
    public boolean deviceLanguageDirection() {
        boolean ltr = TextUtilsCompat.getLayoutDirectionFromLocale(Locale.getDefault()) == ViewCompat.LAYOUT_DIRECTION_LTR;
        return ltr;
    }

    /**
     * handle edittext number input,farsi or en digits and separate three digit
     *
     * @param editText
     * @param watcher
     */
    public void handleNumberInput(EditText editText, TextWatcher watcher) {
        String ss = farsinumber(threeDigit(editText.getText().toString(), ' '));
        // remove listener to avoid cyclic calling
        editText.removeTextChangedListener(watcher);
        editText.setText(ss);
        editText.setSelection(ss.length());
        // reattach listener
        editText.addTextChangedListener(watcher);
    }

    /**
     * set app theme day or night
     *
     * @param night false for day - true for night
     * @param context
     */
    public void setThemeNight(boolean night, Context context) {
        if (night)
            savePref(dayNight, "2", context);
        else
            savePref(dayNight, "1", context);

    }

    /**
     * get theme night or not
     *
     * @param context
     * @return
     */
    public int getThemeNight(Context context) {
        int mode = Integer.parseInt(retrievePref(dayNight,"1", context));

        return mode;
    }

    /**
     * get theme night or not
     *
     * @return
     */
    public int getThemeNight() {
        int mode = Integer.parseInt(retrievePref(dayNight,"1", context));

        return mode;
    }

    /**
     * save value to shared preferences
     *
     * @param name    key name
     * @param value   value for save
     * @param context
     */
    public void savePref(String name, String value, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(name, value);
        editor.apply();
    }

    /**
     * get saved value from shared preferences
     *
     * @param name    key name
     * @param defaultValue 
     * @param context
     * @return
     */
    public String retrievePref(String name,String defaultValue, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        return preferences.getString(name, defaultValue);
    }

    /**
     * delete file from storage
     * @param image image or file path
     */
    public void deleteFile(String image) {
        if (image != "" & image != null) {
            File file = new File(image);
            if (file.exists()) {
                file.delete();
                callBroadCast();
            }
        }
    }

    /**
     * refresh gallery after delete image
     */
    public void callBroadCast() {
        if (Build.VERSION.SDK_INT >= 14) {
            Log.e("-->", " >= 14");
            MediaScannerConnection.scanFile(context, new String[]{Environment.getExternalStorageDirectory().toString()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                /*
                 *   (non-Javadoc)
                 * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                 */
                public void onScanCompleted(String path, Uri uri) {
                    Log.e("ExternalStorage", "Scanned " + path + ":");
                    Log.e("ExternalStorage", "-> uri=" + uri);
                }
            });
        } else {
            Log.e("-->", " < 14");
            context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
                    Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        }
    }

    /**
     * format persian date
     * @param persianDate input persian date
     * @param format 0 example: 29 tir 1398
     * @return
     */
    public String getFormattedDate(long persianDate, int format)
    {
        PersianDateModel dateModel= getPersianDateModel(persianDate);
        if(dateModel!=null) {
            PersianDate persianDate1 = new PersianDate();
            persianDate1.setShYear(Integer.parseInt(dateModel.getYear()));
            persianDate1.setShMonth(Integer.parseInt(dateModel.getMonth()));
            persianDate1.setShDay(Integer.parseInt(dateModel.getDay()));

            PersianDateFormat dateFormat = null;
            if (format == 0) {
                dateFormat = new PersianDateFormat("j F Y");
            }
            return dateFormat.format(persianDate1);
        }
        return "";
    }


    /**
     * get persian todays date
     * @param formatted true for format date
     * @return todays persian date
     */
    public String getTodaysDate(boolean formatted)
    {
        PersianCalendar persianCalendar=new PersianCalendar();
        String date= new StringBuilder()
                .append(String.format("%02d",persianCalendar.getPersianYear()))
                .append(String.format("%02d",persianCalendar.getPersianMonth()))
                .append(String.format("%02d",persianCalendar.getPersianDay()))
                .toString();
        if(formatted)
          return   getFormattedDate(Long.parseLong(date),0);
        else
            return date;
    }


    /**
     * get first this year for example 1398/01/01
     * @return 13980101
     */
    public long getFirstThisYear()
    {
        PersianCalendar persianCalendar=new PersianCalendar();
        String date= new StringBuilder()
                .append(String.format("%02d",persianCalendar.getPersianYear()))
                .append(String.format("%02d",1))
                .append(String.format("%02d",1))
                .toString();
        return Long.parseLong(date);
    }

public long getLongFromPersianDate(PersianDate persianDate)
    {
        String date= new StringBuilder()
                .append(persianDate.getShYear())
                .append(String.format("%02d",persianDate.getShMonth()))
                .append(String.format("%02d",persianDate.getShDay()))
                .toString();
        return Long.parseLong(date);
    }


    /**
     * get end this year example 1398/12/29
     * @return 13981229
     */
    public long getEndThisYear()
    {
        PersianCalendar persianCalendar=new PersianCalendar();
        String lastDayOfLastMonth="29";
        if(persianCalendar.isPersianLeapYear())
            lastDayOfLastMonth="30";

        String date= new StringBuilder()
                .append(String.format("%02d",persianCalendar.getPersianYear()))
                .append("12")
                .append(lastDayOfLastMonth)
                .toString();
        return Long.parseLong(date);
    }


    /**
     * get string date from persian calendar
     * @param persianCalendar persian calendar
     * @param formatted true for format date
     * @return string persian date
     */
    public String getDateFromPersianCalendar(PersianCalendar persianCalendar,boolean formatted)
    {

        String date= new StringBuilder()
                .append(String.format("%02d",persianCalendar.getPersianYear()))
                .append(String.format("%02d",persianCalendar.getPersianMonth()))
                .append(String.format("%02d",persianCalendar.getPersianDay()))
                .toString();
        if(formatted)
          return   getFormattedDate(Long.parseLong(date),0);
        else
            return date;
    }


    /**
     * get two date conflict
     * @param startDate1
     * @param endDate1
     * @param startDate2
     * @param endDate2
     * @return if conflict return true otherwise false
     */
    public boolean isConflict(long startDate1,long endDate1,long startDate2,long endDate2){

        if(startDate1>endDate1)
            return true;
        if(startDate2>endDate2)
            return true;

        if(endDate1>startDate2 & startDate1<endDate2)
            return true;
        if(endDate2>startDate1 & startDate2<endDate1)
            return true;
        return false;

    }

    /**
     * compare two persian date by year and month
     * @param date1 date1
     * @param date2 date2
     * @return return 0 if equal, 1 if date1 greater , -1 if date2 greater
     */
    public int compareYearAndMonth(PersianDate date1,PersianDate date2)
    {


        String year1,year2;

        year1= String.valueOf(date1.getShYear())+String.format("%02d",date1.getShMonth());

        year2= String.valueOf(date2.getShYear())+String.format("%02d",date2.getShMonth());

        if(Integer.parseInt(year1)==Integer.parseInt(year2))
            return 0;
        else if(Integer.parseInt(year1)>Integer.parseInt(year2))
            return 1;
        else
        return -1;
    }

    /**
     * add year,month and day to persian date
     * @param persianDate date
     * @param year int added year
     * @param month int added month
     * @param day int added day
     * @return persian date
     */
    public PersianDate addPersianDate(PersianDate persianDate,int year,int month,int day)
    {
        int date_year=persianDate.getShYear()+year;
        int date_month=persianDate.getShMonth()+month;
        int date_day=persianDate.getShDay()+day;

        PersianDate date=new PersianDate();
        date.setShYear(date_year);
        date.setShMonth(date_month);
        date.setShDay(date_day);

        return date;
    }


    public String getPersianDateFromMiladiDate(Date date,int month) {

        PersianDate persianDate=new PersianDate(date);
        persianDate.addMonth(month);
        PersianDateFormat dateFormat = new PersianDateFormat("j F Y");

        return dateFormat.format(persianDate);


    }

    public void closeApp() {
        //close app
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
    }

    public int getAppVersionNumber()
    {
        int version = 0;
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }


    public void setAppValidity(boolean validity,Context context) {
        if (validity)
        {
            savePref(OFFLINE_LIMIT, String.valueOf(OFFLINE_LIMIT_COUNT), context);
            savePref(IS_VALID_APP, "1", context);
        }
        else
        {
            savePref(IS_VALID_APP, "0", context);
        }

    }


}
