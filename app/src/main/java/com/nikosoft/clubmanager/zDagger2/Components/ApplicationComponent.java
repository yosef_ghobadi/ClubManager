package com.nikosoft.clubmanager.zDagger2.Components;

import com.nikosoft.clubmanager.Adapters.Coaches_Adapter;
import com.nikosoft.clubmanager.Adapters.SMS_Adapter;
import com.nikosoft.clubmanager.Adapters.Sessions_Adapter;
import com.nikosoft.clubmanager.Adapters.Students_Adapter;
import com.nikosoft.clubmanager.Adapters.TuitionPaymentAdapter;
import com.nikosoft.clubmanager.Club.ClubActivity;
import com.nikosoft.clubmanager.ClubManager;
import com.nikosoft.clubmanager.ClubModify.ClubModifyActivity;
import com.nikosoft.clubmanager.Club_Fragment_Dialog.Fragment_dialog_modal;
import com.nikosoft.clubmanager.Coach.CoachActivity;
import com.nikosoft.clubmanager.CoachDetail.CoachDetailActivity;
import com.nikosoft.clubmanager.CoachModify.CoachModifyActivity;
import com.nikosoft.clubmanager.Data.DBRepository.CoachRepository;
import com.nikosoft.clubmanager.Data.Repository;
import com.nikosoft.clubmanager.Field.FieldActivity;
import com.nikosoft.clubmanager.Home.HomeActivity;
import com.nikosoft.clubmanager.Insurance.InsuranceActivity;
import com.nikosoft.clubmanager.MyAccount.MyAccountActivity;
import com.nikosoft.clubmanager.MyAccount.MyAccountPresenter;
import com.nikosoft.clubmanager.SMS.SMSActivity;
import com.nikosoft.clubmanager.Services.SendSMSService;
import com.nikosoft.clubmanager.Session.SessionActivity;
import com.nikosoft.clubmanager.SessionModify.SessionModifyActivity;
import com.nikosoft.clubmanager.Student.StudentActivity;
import com.nikosoft.clubmanager.StudentDetail.Fragments.Info.InfoFragment;
import com.nikosoft.clubmanager.StudentDetail.Fragments.InsurancePayment.InsuranceFragment;
import com.nikosoft.clubmanager.StudentDetail.Fragments.TuitionPayment.TuitionFragPresenter;
import com.nikosoft.clubmanager.StudentDetail.Fragments.TuitionPayment.TuitionFragment;
import com.nikosoft.clubmanager.StudentDetail.StudentDetailActivity;
import com.nikosoft.clubmanager.StudentModify.StudentModifyActivity;
import com.nikosoft.clubmanager.Tuition.TuitionActivity;
import com.nikosoft.clubmanager.Utiles.Utility;
import com.nikosoft.clubmanager.zDagger2.Modules.ApplicationModule;
import com.nikosoft.clubmanager.zDagger2.Modules.DataSourceModule;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {ApplicationModule.class, DataSourceModule.class})
@Singleton
public interface ApplicationComponent {
    void inject(Students_Adapter students_adapter);

    void inject(ClubManager clubManager);

    void inject(Coaches_Adapter coaches_adapter);

    void inject(Sessions_Adapter sessions_adapter);

    void inject(Fragment_dialog_modal fragment_dialog_modal);

    void inject(TuitionFragment tuitionFragment);

    void inject(InsuranceFragment insuranceFragment);

    void inject(InfoFragment infoFragment);


    void inject(ClubActivity clubActivity);

    void inject(ClubModifyActivity clubModifyActivity);

    void inject(CoachActivity coachActivity);

    void inject(StudentActivity studentActivity);

    void inject(CoachModifyActivity coachModifyActivity);

    void inject(StudentModifyActivity studentModifyActivity);

    void inject(HomeActivity homeActivity);

    void inject(FieldActivity fieldActivity);

    void inject(TuitionActivity tuitionActivity);

    void inject(InsuranceActivity insuranceActivity);

    void inject(SessionActivity sessionActivity);

    void inject(SessionModifyActivity sessionModifyActivity);

    void inject(StudentDetailActivity studentDetailActivity);

    void inject(CoachDetailActivity coachDetailActivity);

    void inject(SMSActivity smsActivity);

    void inject(Repository repository);

    void inject(Utility utility);

    void inject(TuitionFragPresenter tuitionFragPresenter);

    void inject(TuitionPaymentAdapter tuitionPaymentAdapter);

    void inject(SMS_Adapter sms_adapter);
    void inject(SendSMSService sendSMSService);


    CoachRepository provideCoachRepository();


    void inject(MyAccountPresenter myAccountPresenter);

    void inject(MyAccountActivity myAccountActivity);
}
