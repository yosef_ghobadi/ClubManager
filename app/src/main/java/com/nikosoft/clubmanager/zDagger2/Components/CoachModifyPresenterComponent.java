package com.nikosoft.clubmanager.zDagger2.Components;

import com.nikosoft.clubmanager.CoachModify.CoachModifyPresenter;
import com.nikosoft.clubmanager.zDagger2.CoachModifyPresenterScope;

import dagger.Component;

@CoachModifyPresenterScope
@Component(dependencies =ApplicationComponent.class )
public interface CoachModifyPresenterComponent {
    void inject(CoachModifyPresenter coachModifyPresenter);
}
