package com.nikosoft.clubmanager.zDagger2.Modules;

import com.nikosoft.clubmanager.Data.DataSource;
import com.nikosoft.clubmanager.Data.Repository;

import dagger.Module;
import dagger.Provides;

@Module
public class DataSourceModule {

    @Provides
    public DataSource provideDataSource()
    {
        return new Repository();
    }
}
