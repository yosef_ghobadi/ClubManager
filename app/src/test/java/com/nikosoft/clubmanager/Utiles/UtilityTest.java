package com.nikosoft.clubmanager.Utiles;

import com.nikosoft.clubmanager.ClubManager;

import org.junit.Test;

import static org.junit.Assert.*;


public class UtilityTest {
    ClubManager clubManager = new ClubManager();
    Utility utility=new Utility();


    @Test
    public void farsinumber() {
        assertEquals("۰۱۲۳۴۵۶۷۸۹", utility.farsinumber("0123456789"));
    }

    @Test
    public void threeDigit() {
        assertEquals("100 000 000 000", utility.threeDigit("100000000000", ' '));
    }



    @Test
    public void getNormalPersianDate() {
        long value = 13970523;
        String date = utility.getNormalPersianDate(value);
        assertEquals(date, "1397/05/23");
    }


}

